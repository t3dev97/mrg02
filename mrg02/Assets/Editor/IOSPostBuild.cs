﻿#if UNITY_IOS //|| UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

public class IOSPostBuild
{
    [PostProcessBuildAttribute(1)]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            string IOS_ADmobID = "ca-app-pub-6533426307108667~6400834199";

            //iOS9 App Transport Security Settings ironsrc
            rootDict.CreateDict("NSAppTransportSecurity");
            rootDict["NSAppTransportSecurity"].AsDict().SetBoolean("NSAllowsArbitraryLoads", true);

            //admob background location useage key (new in iOS 8)
            rootDict.SetString("GADApplicationIdentifier", IOS_ADmobID);
            
            //rootDict.SetString("NSLocationAlwaysAndWhenInUseUsageDescription", " ");
            //rootDict.SetString("NSLocationAlwaysUsageDescription", " ");
            //rootDict.SetString("NSLocationWhenInUseUsageDescription", " ");
            // Write to file 1
            File.WriteAllText(plistPath, plist.WriteToString());

            //string projectPath = PBXProject.GetPBXProjectPath(pathToBuiltProject);
            //PBXProject project = new PBXProject();
            //project.ReadFromFile(projectPath);
            //string targetGUID = project.GetUnityFrameworkTargetGuid();

            //AddFrameworks(project, targetGUID);

            // Write.
            //File.WriteAllText(projectPath, project.WriteToString());

        }
    }

    static void AddFrameworks(PBXProject project, string targetGUID)
    {

        // Frameworks (eppz! Photos, Google Analytics).
        //project.AddFrameworkToProject(targetGUID, "iAd.framework", false);
        project.AddFrameworkToProject(targetGUID, "AdSupport.framework", false);

        project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");

    }
}
#endif