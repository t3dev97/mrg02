using System.Linq;
using UnityEditor;
using UnityEngine;

public class AssetbundleEditor {

	[MenuItem("Assetbundle/Set AssetBundle from Filename", false, 0)]
	static public void SetAssetBundleFromFilename()
	{
		var assets = Selection.objects.Where(o => !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(o))).ToArray();
		foreach (var a in assets)
        {
			string assetPath = AssetDatabase.GetAssetPath(a);
			AssetImporter.GetAtPath(assetPath).SetAssetBundleNameAndVariant(a.name,"");
        }
	}
}

