﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollRectEx : ScrollRect
{
    private bool routeToParent = true;
    private Transform parent;
    private PointerEventData cachePointerEventData;
    public bool RouteToParent { get => routeToParent; set => routeToParent = value; }
    public Transform Parent { get => parent; set => parent = value; }
    public PointerEventData CachePointerEventData { get => cachePointerEventData; set => cachePointerEventData = value; }

    /// <summary>
    /// Do action for all parents
    /// </summary>
    private void DoForParents<T>(Action<T> action) where T : IEventSystemHandler
    {
        //Transform parent = transform.parent;
        if (parent != null)
        {
            foreach (var component in parent.GetComponents<Component>())
            {
                if (component is T)
                {
                    action((T)(IEventSystemHandler)component);
                }
            }
        }
        //while (parent != null)
        //{
        //    foreach (var component in parent.GetComponents<Component>())
        //    {
        //        if (component is T)
        //            action((T)(IEventSystemHandler)component);
        //    }
        //    parent = parent.parent;
        //    print("parent" + (parent.name));
        //}
    }

    /// <summary>
    /// Always route initialize potential drag event to parents
    /// </summary>
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        DoForParents<IInitializePotentialDragHandler>((parent) => { parent.OnInitializePotentialDrag(eventData); });
        base.OnInitializePotentialDrag(eventData);
    }

    /// <summary>
    /// Drag event
    /// </summary>
    public override void OnDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {        
        if (routeToParent)
        {
            DoForParents<IDragHandler>((parent) => { parent.OnDrag(eventData); /*print("IDragHandler");*/ });
        }
        else
        {
            base.OnDrag(eventData);
        }
    }

    /// <summary>
    /// Begin drag event
    /// </summary>
    public override void OnBeginDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        //if (!horizontal && Math.Abs(eventData.delta.x) > Math.Abs(eventData.delta.y))
        //    routeToParent = true;
        //else if (!vertical && Math.Abs(eventData.delta.x) < Math.Abs(eventData.delta.y))
        //    routeToParent = true;
        //else
        //    routeToParent = false;
        //routeToParent = true;


        //if (verticalNormalizedPosition >= 1)
        //    routeToParent = true;
        if (routeToParent)
            DoForParents<IBeginDragHandler>((parent) => { parent.OnBeginDrag(eventData); });
        else
            base.OnBeginDrag(eventData);

        cachePointerEventData = eventData;
    }

    /// <summary>
    /// End drag event
    /// </summary>
    public override void OnEndDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (routeToParent)
            DoForParents<IEndDragHandler>((parent) => { parent.OnEndDrag(eventData); });
        else
            base.OnEndDrag(eventData);
        //routeToParent = false;
    }
}
