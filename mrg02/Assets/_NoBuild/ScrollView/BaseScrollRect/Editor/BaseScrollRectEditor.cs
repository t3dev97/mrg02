﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor.AnimatedValues;
using UnityEditor;

namespace ATD.Unity.UI.Editors
{
    [CustomEditor(typeof(BaseScrollRect), true)]
    [CanEditMultipleObjects]
    public class BaseScrollRectEditor : Editor
    {
        SerializedProperty content;
        SerializedProperty horizontallyScrollable;
        SerializedProperty verticallyScrollable;
        SerializedProperty scrollingMovementType;
        SerializedProperty elasticity;
        SerializedProperty inertia;
        SerializedProperty decelerationRate;
        SerializedProperty scrollSensitivity;
        SerializedProperty viewportRectTransform;
        SerializedProperty horizontalScrollbar;
        SerializedProperty verticalScrollbar;
        SerializedProperty horizontalScrollbarVisibility;
        SerializedProperty verticalScrollbarVisibility;
        SerializedProperty horizontalScrollbarSpacing;
        SerializedProperty verticalScrollbarSpacing;
        SerializedProperty forwardUnusedEventsToContainer;
        SerializedProperty onValueChanged;
        AnimBool showElasticity;
        AnimBool showDecelerationRate;
        bool viewportIsNotChild, hScrollbarIsNotChild, vScrollbarIsNotChild;
        static string s_HError = "For this visibility mode, the Viewport property and the Horizontal Scrollbar property both needs to be set to a Rect Transform that is a child to the Scroll Rect.";
        static string s_VError = "For this visibility mode, the Viewport property and the Vertical Scrollbar property both needs to be set to a Rect Transform that is a child to the Scroll Rect.";

        protected virtual void OnEnable()
        {
            content = serializedObject.FindProperty("content");
            horizontallyScrollable = serializedObject.FindProperty("horizontallyScrollable");
            verticallyScrollable = serializedObject.FindProperty("verticallyScrollable");
            scrollingMovementType = serializedObject.FindProperty("scrollingMovementType");
            elasticity = serializedObject.FindProperty("elasticity");
            inertia = serializedObject.FindProperty("inertia");
            decelerationRate = serializedObject.FindProperty("decelerationRate");
            scrollSensitivity = serializedObject.FindProperty("scrollSensitivity");
            viewportRectTransform = serializedObject.FindProperty("viewportRectTransform");
            horizontalScrollbar = serializedObject.FindProperty("horizontalScrollbar");
            verticalScrollbar = serializedObject.FindProperty("verticalScrollbar");
            horizontalScrollbarVisibility = serializedObject.FindProperty("horizontalScrollbarVisibility");
            verticalScrollbarVisibility = serializedObject.FindProperty("verticalScrollbarVisibility");
            horizontalScrollbarSpacing = serializedObject.FindProperty("horizontalScrollbarSpacing");
            verticalScrollbarSpacing = serializedObject.FindProperty("verticalScrollbarSpacing");
            forwardUnusedEventsToContainer = serializedObject.FindProperty("forwardUnusedEventsToContainer");
            onValueChanged = serializedObject.FindProperty("onValueChanged");

            showElasticity = new AnimBool(Repaint);
            showDecelerationRate = new AnimBool(Repaint);
            SetAnimBools(true);
        }

        protected virtual void OnDisable()
        {
            showElasticity.valueChanged.RemoveListener(Repaint);
            showDecelerationRate.valueChanged.RemoveListener(Repaint);
        }

        void SetAnimBools(bool instant)
        {
            SetAnimBool(showElasticity, !scrollingMovementType.hasMultipleDifferentValues && scrollingMovementType.enumValueIndex == (int)ScrollRect.MovementType.Elastic, instant);
            SetAnimBool(showDecelerationRate, !inertia.hasMultipleDifferentValues && inertia.boolValue == true, instant);
        }

        void SetAnimBool(AnimBool a, bool value, bool instant)
        {
            if (instant)
                a.value = value;
            else
                a.target = value;
        }

        void CalculateCachedValues()
        {
            viewportIsNotChild = false;
            hScrollbarIsNotChild = false;
            vScrollbarIsNotChild = false;
            if (targets.Length == 1)
            {
                Transform transform = ((BaseScrollRect)target).transform;
                if (viewportRectTransform.objectReferenceValue == null || ((RectTransform)viewportRectTransform.objectReferenceValue).transform.parent != transform)
                    viewportIsNotChild = true;
                if (horizontalScrollbar.objectReferenceValue == null || ((Scrollbar)horizontalScrollbar.objectReferenceValue).transform.parent != transform)
                    hScrollbarIsNotChild = true;
                if (verticalScrollbar.objectReferenceValue == null || ((Scrollbar)verticalScrollbar.objectReferenceValue).transform.parent != transform)
                    vScrollbarIsNotChild = true;
            }
        }

        public override void OnInspectorGUI()
        {
            SetAnimBools(false);

            serializedObject.Update();
            // Once we have a reliable way to know if the object changed, only re-cache in that case.
            CalculateCachedValues();

            EditorGUILayout.PropertyField(content);

            EditorGUILayout.PropertyField(horizontallyScrollable);
            EditorGUILayout.PropertyField(verticallyScrollable);

            EditorGUILayout.PropertyField(scrollingMovementType);
            if (EditorGUILayout.BeginFadeGroup(showElasticity.faded))
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(elasticity);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndFadeGroup();

            EditorGUILayout.PropertyField(inertia);
            if (EditorGUILayout.BeginFadeGroup(showDecelerationRate.faded))
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(decelerationRate);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndFadeGroup();

            EditorGUILayout.PropertyField(scrollSensitivity);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(viewportRectTransform);

            EditorGUILayout.PropertyField(horizontalScrollbar);
            if (horizontalScrollbar.objectReferenceValue && !horizontalScrollbar.hasMultipleDifferentValues)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(horizontalScrollbarVisibility, new GUIContent("Visibility"));

                if ((ScrollRect.ScrollbarVisibility)horizontalScrollbarVisibility.enumValueIndex == ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport
                    && !horizontalScrollbarVisibility.hasMultipleDifferentValues)
                {
                    if (viewportIsNotChild || hScrollbarIsNotChild)
                        EditorGUILayout.HelpBox(s_HError, MessageType.Error);
                    EditorGUILayout.PropertyField(horizontalScrollbarSpacing, new GUIContent("Spacing"));
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.PropertyField(verticalScrollbar);
            if (verticalScrollbar.objectReferenceValue && !verticalScrollbar.hasMultipleDifferentValues)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(verticalScrollbarVisibility, new GUIContent("Visibility"));

                if ((ScrollRect.ScrollbarVisibility)verticalScrollbarVisibility.enumValueIndex == ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport
                    && !verticalScrollbarVisibility.hasMultipleDifferentValues)
                {
                    if (viewportIsNotChild || vScrollbarIsNotChild)
                        EditorGUILayout.HelpBox(s_VError, MessageType.Error);
                    EditorGUILayout.PropertyField(verticalScrollbarSpacing, new GUIContent("Spacing"));
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(forwardUnusedEventsToContainer);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(onValueChanged);

            serializedObject.ApplyModifiedProperties();
        }
    }
}