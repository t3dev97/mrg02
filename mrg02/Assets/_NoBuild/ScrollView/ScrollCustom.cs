﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollCustom : MonoBehaviour
{
    [SerializeField]
    private float maxY;
    [SerializeField]
    private ScrollRect scrollExMain;
    [SerializeField]
    private ScrollRectEx scrollExLevel;
    [SerializeField]
    private Transform trsContentMain;
    [SerializeField]
    private Transform trsContentLevel;
    [SerializeField]
    private Transform topBG;
    private float oldPosYContentLevel;
    private void Awake()
    {
        scrollExLevel.Parent = scrollExMain.transform;
        scrollExMain.onValueChanged.AddListener(OnValuesSrollExMain);
        scrollExLevel.onValueChanged.AddListener(OnValuesSrollExLevel);
    }
    private void OnValuesSrollExMain(Vector2 value)
    {
        if (trsContentMain.localPosition.y >= maxY)
        {
            scrollExLevel.RouteToParent = false;
            //GameController.Instance.HideImgBGTopMainMenu();
            GameController.Instance.ShowBtnGotoMain();
            HideTopBG();
            Vector3 cachePos = trsContentMain.localPosition;
            cachePos.y = maxY;
            trsContentMain.localPosition = cachePos;
            if (scrollExLevel.CachePointerEventData != null)
                scrollExLevel.OnBeginDrag(scrollExLevel.CachePointerEventData);
        }
    }
    private float lastValue;
    private void OnValuesSrollExLevel(Vector2 value)
    {
        //print("OnValuesSrollExLevel " + value);
        if (lastValue > value.y)
        {
            //UnityEngine.Debug.Log("Scrolling UP: " + value);
            if (trsContentMain.localPosition.y >= maxY)
            {
                scrollExLevel.RouteToParent = false;
                Vector3 cachePos = trsContentMain.localPosition;
                cachePos.y = maxY;
                trsContentMain.localPosition = cachePos;
            }
            else
            {
                if (GameController.Instance.CheckShowBtnGotoMain() == false)
                {
                    //GameController.Instance.ShowImgBGTopMainMenu();
                    scrollExLevel.RouteToParent = true;
                }
            }
        }
        else
        {
            //UnityEngine.Debug.Log("Scrolling DOWN: " + value);
            if (value.y > 1.0f)
            {
                scrollExLevel.RouteToParent = true;
                GameController.Instance.HideBtnGotoMain();
                ShowTopBG();
            }
        }
        lastValue = value.y;
    }
    private void ShowTopBG()
    {
        topBG.gameObject.SetActive(true);
    }
    private void HideTopBG()
    {
        topBG.gameObject.SetActive(false);
    }
}
