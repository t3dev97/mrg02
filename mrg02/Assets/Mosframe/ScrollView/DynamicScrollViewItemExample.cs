﻿/**
 * DynamicScrollViewItemExample.cs
 * 
 * @author mosframe / https://github.com/mosframe
 * 
 */
namespace Mosframe
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class DynamicScrollViewItemExample : UIBehaviour, IDynamicScrollViewItem
    {
        private readonly Color[] colors = new Color[] {
            Color.cyan,
            Color.green,
        };

        public Text title;
        public Image background;
        [SerializeField]
        private Transform trsLevelsNormal;
        [SerializeField]
        private LevelItem levelItem;
        [SerializeField]
        private LevelItem levelSpecialItem;
        private List<LevelItem> levelItems;
        private int numberItem = 9;
        public void onUpdateItem(int index)
        {
            if (levelItem == null) return;
            levelItem.gameObject.SetActive(false);
            levelItems = new List<LevelItem>();
            this.title.text = string.Format("WORLD {0:d2}", (index + 1));
            if (trsLevelsNormal.childCount > 1)
            {
                for (int i = 0; i < numberItem; i++)
                {
                    //trsLevelsNormal.GetChild(i).GetComponent<LevelItem>().ShowLevelLock();
                }
            }
            else
            {
                for (int i = 0; i < numberItem; i++)
                {
                    LevelItem temp = Instantiate(levelItem, trsLevelsNormal);
                    temp.name = i.ToString();
                    int idLevel = i + index * 10 + 1;
                    temp.UpdateLevel(idLevel);
                    temp.gameObject.SetActive(true);
                    MCache.Instance.LevelsUI.Add(idLevel, temp);
                    if (MCache.Instance.UserData.IdLevelsUnlocked.Contains(idLevel))
                    {
                        temp.ShowLevelUnLock();
                        if (MCache.Instance.UserData.IdLevelsComplete.Contains(idLevel))
                        {
                            temp.ShowLevelComplete();
                        }
                    }
                    else
                    {
                        temp.ShowLevelLock();
                    }
                    levelItems.Add(temp);
                }
                // Special Item 
                int id = numberItem + index * 10 + 1;
                levelSpecialItem.gameObject.SetActive(true);
                levelSpecialItem.UpdateLevel(id);
                MCache.Instance.LevelsUI.Add(id, levelSpecialItem);
                if (MCache.Instance.UserData.IdLevelsUnlocked.Contains(id))
                {
                    levelSpecialItem.ShowLevelUnLock();
                    if (MCache.Instance.UserData.IdLevelsComplete.Contains(id))
                    {
                        levelSpecialItem.ShowLevelComplete();
                    }
                }
                else
                {
                    levelSpecialItem.ShowLevelLock();
                }
                levelItems.Add(levelSpecialItem);
            }
            return;

            //this.background.color   = this.colors[Mathf.Abs(index) % this.colors.Length];
        }
        public void ReUpdate()
        {
            for (int index = 0; index < levelItems.Count; index++)
            {
                if (MCache.Instance.UserData.IdLevelsUnlocked.Contains(levelItems[index].IdLevel))
                {
                    levelItems[index].ShowLevelUnLock();
                }
            }
        }
    }
}