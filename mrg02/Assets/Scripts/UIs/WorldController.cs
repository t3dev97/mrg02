﻿using UnityEngine;
using UnityEngine.UI;

public class WorldController : MonoBehaviour
{
    [SerializeField] private Text txtTitle;
    [SerializeField] private Transform trsTop;
    [SerializeField] private Transform trsLevelGroup;
    [SerializeField] private Transform trsBottom;
    private void Awake()
    {
        if (Camera.main.aspect < 0.5625)
            transform.GetComponent<RectTransform>().sizeDelta = new Vector2(1080, 1255);
    }
    public void UpdateTitle(string str)
    {
        txtTitle.text = str;
    }
    public void UpdateWorld(int idWorld, int numberLevel = 10, bool isAdsToUnLock = false)
    {
        LevelItem demo = MCache.Instance.LevelsUItem[ConfigVariables.LEVEL_ITEM];
        LevelItem demoSpacial = MCache.Instance.LevelsUItem[ConfigVariables.LEVEL_ITEM_SPACIAL];
        if (isAdsToUnLock)
        {
            ShowTop();
        }
        else
        {
            HideTop();
            for (int index = 0; index < numberLevel; index++)
            {
                LevelItem level = (index < numberLevel - 1) ? Instantiate(demo, trsLevelGroup) : Instantiate(demoSpacial, trsBottom);
                level.IdLevel = index + idWorld * 10 + 1;
                level.UpdateLevel();
                MCache.Instance.LevelsUI.Add(level.IdLevel, level);
            }
        }
    }
    private void ShowTop()
    {
        trsTop.gameObject.SetActive(true);
    }
    private void HideTop()
    {
        trsTop.gameObject.SetActive(false);
    }
}
