﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupVipGoldController : UIPopupBase
{
    [SerializeField]
    private Button btnStart;
    [SerializeField]
    private Button btnBack;
    private bool isShow = false;
    public override void Show()
    {
        base.Show();
        isShow = true;
        AdsManager.Instance.HideBanner();
    }
    public override void Hide()
    {
        base.Hide();
        if (isShow)
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                AdsManager.Instance.ShowBanner("Screen Banner", "none", "none");
            }
    }
    public override void OnCustomStart()
    {
        btnStart.onClick.AddListener(OnClickBtnStart);
        btnBack.onClick.AddListener(OnClickBtnBack);
    }
    private void OnClickBtnStart()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        IAPManager.Instance.BuyVipGold((b) =>
        {
            if (b)
            {
                GameController.Instance.ShowPopupNotification("BUY VIP GOLE TO COMPLETE");
            }
            else
            {
                GameController.Instance.ShowPopupNotification("BUY VIP GOLE TO FAIL");
            }
        });
    }
    private void OnClickBtnBack()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Hide();
    }

    protected override void EventBack()
    {
        throw new System.NotImplementedException();
    }
}
