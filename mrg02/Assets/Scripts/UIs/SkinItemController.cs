﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
public class SkinItemController : MonoBehaviour
{
    public static SkinItemController oldItemSelect;
    [SerializeField]
    private Button btnSelect;
    [SerializeField]
    private Transform imgBackgroundSelect;
    [SerializeField]
    private Transform imgLockBackground;
    [SerializeField]
    private Text txtPrice;
    [SerializeField]
    private Image imgIcon;
    [SerializeField]
    private Image imgIconGlow;
    [SerializeField]
    private Image imgAds;
    private ItemShopRow itemShop;
    public Transform ImgBackgroundSelect { get => imgBackgroundSelect; set => imgBackgroundSelect = value; }
    public Text TxtPrice { get => txtPrice; set => txtPrice = value; }

    public void OnCustomStart(int id)
    {
        itemShop = MCache.Instance.ItemsShopData[id];
        Sprite sprite = MCache.Instance.ImgItemsShop[itemShop.NAME];
        imgIcon.sprite = sprite;
        imgIcon.SetNativeSize();
        imgIconGlow.gameObject.SetActive(false);
        if (id == MCache.Instance.UserData.IdSkinSelected)
        {
            itemShop.ADS = false;
            imgAds.gameObject.SetActive(false);
            OnClickBtnSelect();
        }
        else
        {
            int checkIdSkins = MCache.Instance.UserData.IdSkins.Find(x => x == id);
            if (checkIdSkins > 0) // Skin Unlocked
            {
                itemShop.ADS = false;
                imgAds.gameObject.SetActive(false);
                txtPrice.text = ConfigVariables.TXT_EQUIP;
            }
            else
            {
                txtPrice.gameObject.SetActive(!itemShop.ADS);
                txtPrice.text = itemShop.PIRCE.ToString();
                imgAds.gameObject.SetActive(itemShop.ADS);
            }
            imgLockBackground.gameObject.SetActive(false);
        }
        btnSelect.onClick.AddListener(OnClickBtnSelect);

        Bounce();
    }
    private void OnClickBtnSelect()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (itemShop.ADS == false)
        {
            if (MCache.Instance.UserData.IdSkins.Find(x => x == itemShop.ID) > 0)
            {
                ChooseSkin();
            }
            else
            {
                if (MCache.Instance.UserData.StarNumber > itemShop.PIRCE) // Unlock
                {
                    UnlockSkin();
                }
                else
                {
                    GameController.Instance.ShowPopupNotEnoughStar();
                }
            }
        }
        else
        {
            GameController.Instance.ShowPopupConfirmOpenAds(() =>
            {
                AdsManager.Instance.ShowRewardedVideo(UnlockSkin, Error, UnlockSkin, "none", "none", "none");
            });

            void Error()
            {

            }
        }
    }
    public void UnlockSkin()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Reward);
        ChooseSkin();
        MCache.Instance.UserData.IdSkins.Add(itemShop.ID);
    }
    public void ChooseSkin()
    {
        SelectThisSkin();
        GameController.Instance.CurrentIdBall = itemShop.ID;
    }
    private void SelectThisSkin()
    {
        imgBackgroundSelect.gameObject.SetActive(true);
        HideOldBackground();
        oldItemSelect = this;

        txtPrice.gameObject.SetActive(false);
        txtPrice.text = ConfigVariables.TXT_EQUIPED;
        itemShop.ADS = false;
        imgAds.gameObject.SetActive(false);
    }
    private void HideOldBackground()
    {
        if (oldItemSelect == null) return;
        oldItemSelect.ImgBackgroundSelect.gameObject.SetActive(false);
        oldItemSelect.TxtPrice.text = ConfigVariables.TXT_EQUIP;
    }
    private void Bounce()
    {
        DOVirtual.Float(imgIcon.transform.localScale.x, imgIcon.transform.localScale.x - 0.2f, 1, (x) =>
        {
            imgIcon.transform.localScale = new Vector3(x, x, 1);
        }).SetLoops(-1, LoopType.Yoyo).SetId(this).SetEase(Ease.Linear);
    }
    public void ResetStage()
    {
        OnCustomStart(itemShop.ID);
    }
}
