﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class UIBattleGameController : UIScreenBase
{
    [SerializeField]
    private TextMeshProUGUI txtTimeCounter;
    [SerializeField]
    private Text txtLevelNumber;
    [SerializeField]
    private Button btnTrackTouchScreen;
    [SerializeField]
    private Button btnHint;
    [SerializeField]
    private Button btnShadow;
    [SerializeField]
    private Button btnSkip;
    [SerializeField]
    private Text txtHintNumber;
    [SerializeField]
    private Text txtShadowNumber;
    [SerializeField]
    private Text txtSkipNumber;
    [SerializeField]
    private Image imgHintAds;
    [SerializeField]
    private Image imgShadowAds;
    [SerializeField]
    private Image imgSkipAds;
    [SerializeField]
    private BallItemView ballItemView;
    private BallItemView[] ballItems;
    private BallItemView oldBallItem;
    private BallItemView oldNextBallItem;
    [SerializeField]
    private Transform trsBoard;
    [SerializeField]
    private Transform trsToolTip;

    [SerializeField] private Button btnPause;
    [SerializeField] private Button btnRemoveAds;
    [SerializeField] private Button btnShop;
    [SerializeField] private Button btnRetry;

    private void OnEnable()
    {
        UpdateStageSkill();
    }
    public override void OnCustomStart()
    {
        btnTrackTouchScreen.onClick.AddListener(OnClickBtnTrackTouchInScreen);
        btnPause.onClick.AddListener(OnClickBtnPause);
        btnHint.onClick.AddListener(OnClickBtnHint);
        btnShadow.onClick.AddListener(OnClickBtnShadow);
        btnSkip.onClick.AddListener(OnClickBtnSkip);
        btnRetry.onClick.AddListener(OnclickBtnRetry);
        btnRemoveAds.onClick.AddListener(OnclickBtnRemoveAds);
        btnShop.onClick.AddListener(OnclickBtnShop);
    }
    public override void Show()
    {
        base.Show();
        HideTimeCounter();
        ShowSkillBar();
        InteractableToolTip();
        ShowButtons();
    }

    public void UpdateTimeCounter(float newValue)
    {
        txtTimeCounter.text = newValue.ToString();
    }
    public void ShowTimeCounter()
    {
        txtTimeCounter.gameObject.SetActive(true);
    }
    public void HideTimeCounter()
    {
        txtTimeCounter.gameObject.SetActive(false);
    }
    public void SetUpTurn(int numberTurn)
    {
        if (ballItems != null)
        {
            for (int i = 0; i < ballItems.Length; i++)
            {
                Destroy(ballItems[i].gameObject);
            }
        }
        ballItems = new BallItemView[numberTurn];
        ballItemView.gameObject.SetActive(false);
        for (int i = 0; i < numberTurn; i++)
        {
            ballItems[i] = Instantiate(ballItemView, trsBoard) as BallItemView;
            ballItems[i].Hide();
            ballItems[i].gameObject.SetActive(true);
            if (i == 0)
            {
                ballItems[i].ShowNext();
                oldNextBallItem = ballItems[i];
            }
        }
    }
    public void UpdateNextBall(int turn, int numberFull)
    {
        if (oldBallItem != null)
            oldBallItem.ShowNormal();
        if (oldNextBallItem != null)
            oldNextBallItem.HideNext();
        ballItems[turn].ShowCurrent(turn >= numberFull);
        if (turn < numberFull)
        {
            ballItems[turn + 1].ShowNext();
            oldNextBallItem = ballItems[turn + 1];
        }
        oldBallItem = ballItems[turn];
    }
    public void RevertNextBall(int turn)
    {
        if (oldNextBallItem != null)
        {
            oldNextBallItem.Hide();
        }
        oldBallItem.Hide();
        oldBallItem.ShowNext();
    }
    public void UpdateTextShowCurrentLevel(int currentLevel)
    {
        txtLevelNumber.text = currentLevel.ToString();
    }
    #region BUTTON
    private void OnClickBtnTrackTouchInScreen()
    {
        GameController.Instance.TouchInScreenToSpawnNextBall();
    }
    private void OnClickBtnPause()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupPause();
    }
    private void ActiveSkillHint()
    {
        GameController.Instance.HandleSkillHint();
        MCache.Instance.UserData.HintSkillNumber--;
        UpdateStageSkill();
    }
    private void OnClickBtnHint()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Active_Hint);
        if (MCache.Instance.UserData.HintSkillNumber > 0)
        {
            ActiveSkillHint();
        }
        else
        {
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                GameController.Instance.ShowPopupConfirmOpenAds(() =>
                {
                    AdsManager.Instance.ShowRewardedVideo(RewardedVideoDone, RewardedVideoFail, RewardedVideoNoRewardedVideo, "node", "node", "node");
                });
            }
            else
            {
                RewardedVideoDone();
            }
            void RewardedVideoDone()
            {
                ActiveSkillHint();
            }
            void RewardedVideoFail()
            {
                GameController.Instance.ShowPopupConfirm(ConfigVariables.POPUP_NOTIFICATION_CONTENT_WATCH_FAIL, null);
            }
            void RewardedVideoNoRewardedVideo()
            {
                ActiveSkillHint();
            }
        }
    }
    private void ActiveSkillShadown()
    {

        GameController.Instance.HandleSkillShadowBall();
        MCache.Instance.UserData.ShadowBallSkillNumber--;
        UpdateStageSkill();
        UnInteractableToolTip();
    }
    private void OnClickBtnShadow()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Active_MagicBall);
        if (MCache.Instance.UserData.ShadowBallSkillNumber > 0)
        {
            ActiveSkillShadown();
        }
        else
        {
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                AdsManager.Instance.ShowRewardedVideo(RewardedVideoDone, RewardedVideoFail, RewardedVideoNoRewardedVideo, "node", "node", "node");
            }
            else
            {
                RewardedVideoDone();
            }
            void RewardedVideoDone()
            {
                ActiveSkillShadown();
            }
            void RewardedVideoFail()
            {
                GameController.Instance.ShowPopupConfirm(ConfigVariables.POPUP_NOTIFICATION_CONTENT_WATCH_FAIL, null);
            }
            void RewardedVideoNoRewardedVideo()
            {
                ActiveSkillShadown();
            }
            //GameController.Instance.ShowPopupConfirmOpenAds(() => { GameController.Instance.HandleSkillShadowBall(); });
        }
    }
    private void ActiveSkillSkipTurn()
    {
        print("ActiveSkillShadown");
        GameController.Instance.HandleSkillSkipTurn();
        MCache.Instance.UserData.SkipTurnSkillNumber--;
        UpdateStageSkill();
    }
    private void OnClickBtnSkip()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Active_SkipMove);
        if (MCache.Instance.UserData.SkipTurnSkillNumber > 0)
        {
            ActiveSkillSkipTurn();
        }
        else
        {
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                GameController.Instance.ShowPopupConfirmOpenAds(() =>
                {
                    AdsManager.Instance.ShowRewardedVideo(RewardedVideoDone, RewardedVideoFail, RewardedVideoNoRewardedVideo, "node", "node", "node");
                });
            }
            else
            {
                RewardedVideoDone();
            }
            void RewardedVideoDone()
            {
                ActiveSkillSkipTurn();
            }
            void RewardedVideoFail()
            {
                GameController.Instance.ShowPopupConfirm(ConfigVariables.POPUP_NOTIFICATION_CONTENT_WATCH_FAIL, null);
            }
            void RewardedVideoNoRewardedVideo()
            {
                ActiveSkillSkipTurn();
            }
        }
    }
    private void OnclickBtnRetry()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        HideBtnRetry();
        if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
        {
            if (AdsManager.intervalAd <= 0)
            {
                AdsManager.Instance.ShowInterstitial(Done, Done, "none", "none", "none");
                void Done()
                {
                    Retry();
                    AdsManager.intervalAd = 30;
                }
            }
            else
            {
                Retry();
            }
        }
        else
        {
            Retry();
        }

        void Retry()
        {
            HideTimeCounter();
            ShowButtons();
            ShowSkillBar();
            GameController.Instance.GotoBattleGame();
        }
    }
    private void HideBtnRetry()
    {
        btnRetry.interactable = false;
        DOVirtual.DelayedCall(ConfigVariables.DELAY_TIME_WAITING, () =>
        {
            btnRetry.interactable = true;
        }).SetId(this);
    }
    private void OnclickBtnShop()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowUIShop();
    }
    private void OnclickBtnRemoveAds()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupRemoveAds();
    }
    private void UpdateStageSkill()
    {
        imgHintAds.gameObject.SetActive(MCache.Instance.UserData.HintSkillNumber <= 0);
        imgShadowAds.gameObject.SetActive(MCache.Instance.UserData.ShadowBallSkillNumber <= 0);
        imgSkipAds.gameObject.SetActive(MCache.Instance.UserData.SkipTurnSkillNumber <= 0);

        txtHintNumber.gameObject.SetActive(MCache.Instance.UserData.HintSkillNumber > 0);
        txtShadowNumber.gameObject.SetActive(MCache.Instance.UserData.ShadowBallSkillNumber > 0);
        txtSkipNumber.gameObject.SetActive(MCache.Instance.UserData.SkipTurnSkillNumber > 0);

        txtHintNumber.text = MCache.Instance.UserData.HintSkillNumber.ToString();
        txtShadowNumber.text = MCache.Instance.UserData.ShadowBallSkillNumber.ToString();
        txtSkipNumber.text = MCache.Instance.UserData.SkipTurnSkillNumber.ToString();
    }
    public void ShowSkillBar()
    {
        trsToolTip.gameObject.SetActive(true);
    }
    public void HideSkillBar()
    {
        trsToolTip.gameObject.SetActive(false);
    }

    protected override void EventBack()
    {
        if (GameController.Instance.PopupShowing == false && GameController.Instance.CurrentScreenType != EScreenName.SHOP)
            OnClickBtnPause();
    }
    public void ShowBtnShadown()
    {
        btnShadow.gameObject.SetActive(true);
    }
    private void HideBtnShadown()
    {
        print("HideBtnShadown");
        btnShadow.gameObject.SetActive(false);
    }

    public void HideButtonToCountDown()
    {
        btnRetry.gameObject.SetActive(false);
        btnShop.gameObject.SetActive(false);
        btnPause.gameObject.SetActive(false);
        btnRemoveAds.gameObject.SetActive(false);
    }
    private void ShowButtons()
    {
        btnRetry.gameObject.SetActive(true);
        btnShop.gameObject.SetActive(true);
        btnPause.gameObject.SetActive(true);
        btnRemoveAds.gameObject.SetActive(true);
    }

    public void ShowBtnHint()
    {
        btnHint.gameObject.SetActive(true);
    }
    public void HideBtnHint()
    {
        btnHint.gameObject.SetActive(false);
    }
    public void InteractableToolTip()
    {
        btnHint.interactable = true;
        btnShadow.interactable = true;
        btnSkip.interactable = true;
    }
    public void UnInteractableToolTip()
    {
        btnHint.interactable = false;
        btnShadow.interactable = false;
        btnSkip.interactable = false;
    }
    #endregion
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
}
