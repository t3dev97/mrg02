﻿using DG.Tweening;
using UnityEngine;
public class ToolTipMoveLR : MonoBehaviour
{
    [SerializeField] private float dir;
    private void Start()
    {
        transform.DOLocalMoveX(transform.localPosition.x + dir, 0.2f).SetLoops(-1, LoopType.Yoyo).SetId(this).SetEase(Ease.Linear);
    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
}
