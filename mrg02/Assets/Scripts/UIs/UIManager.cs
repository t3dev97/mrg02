﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    [SerializeField]
    private RectTransform rectCanvas;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    public float GetScaleUI()
    {
        return rectCanvas.localScale.x;
    }
}
