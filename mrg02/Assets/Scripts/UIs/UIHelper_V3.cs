﻿using UnityEngine;

public class UIHelper_V3 : MonoBehaviour
{
    private void Awake()
    {
        if (Camera.main.aspect < 0.5625)
        {
            transform.localScale = Vector3.one * (Camera.main.aspect / 0.5625f);
        }
    }
}
