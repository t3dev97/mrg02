﻿using DG.Tweening;
using UnityEngine;

public abstract class UIScreenBase : MonoBehaviour, ICustomInit
{
    [SerializeField]
    private EScreenName screenType;

    protected EScreenName ScreenType { get => screenType; set => screenType = value; }

    public abstract void OnCustomStart();
    public virtual void Show()
    {
        //if (trsFade != null)
        //{
        //    trsFade.gameObject.SetActive(true);
        //}
        GameController.Instance.CurrentScreenType = screenType;
        gameObject.SetActive(true);
    }
    public virtual void Hide()
    {
        //if (trsFade != null)
        //{
        //    trsFade.gameObject.SetActive(false);
        //}
        gameObject.SetActive(false);
    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
    private void Start()
    {
        OnCustomStart();
    }
    protected abstract void EventBack();
    private bool isCanBack = true;
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && isCanBack == true)
        {
            isCanBack = false;
            Invoke("ResetCanBack", 0.5f);
            EventBack();
        }
    }
    private void ResetCanBack()
    {
        isCanBack = !isCanBack;
    }
}
