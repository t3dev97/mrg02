﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
public abstract class UIPopupBase : MonoBehaviour, ICustomInit
{
    [SerializeField]
    private EPopupName popupType;
    [SerializeField]
    protected Text txtTitle;

    protected EPopupName PopupType { get => popupType; set => popupType = value; }

    public virtual void Show()
    {
        GameController.Instance.PopupShowing = true;
        gameObject.SetActive(true);

        if (txtTitle == null) return;

        float y = txtTitle.transform.localPosition.y;
        Vector3 posCurrent = txtTitle.transform.localPosition;
        DOVirtual.Float(ConfigVariables.POS_Y_BEGIN, ConfigVariables.POS_Y_SHOW, ConfigVariables.TIME_MOVE, (currentY) =>
        {
            posCurrent.y = currentY;
            txtTitle.transform.localPosition = posCurrent;
        }).SetId(this);
    }
    public virtual void Hide()
    {
        GameController.Instance.PopupShowing = false;
        if (txtTitle == null)
        {
        }
        gameObject.SetActive(false);
        return;
        float y = txtTitle.transform.localPosition.y;
        Vector3 posCurrent = txtTitle.transform.localPosition;
        DOVirtual.Float(ConfigVariables.POS_Y_SHOW, ConfigVariables.POS_Y_BEGIN, ConfigVariables.TIME_MOVE, (currentY) =>
        {
            posCurrent.y = currentY;
            txtTitle.transform.localPosition = posCurrent;
        }).OnComplete(() =>
        {
            gameObject.SetActive(false);
        }).SetId(this);
    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
    public abstract void OnCustomStart();
    private void Start()
    {
        OnCustomStart();
    }
    protected abstract void EventBack();
    private bool isCanBack = true;
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && isCanBack == true)
        {
            isCanBack = false;
            Invoke("ResetCanBack", 0.5f);
            EventBack();
        }
    }
    private void ResetCanBack()
    {
        isCanBack = !isCanBack;
    }
}
