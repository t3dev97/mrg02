﻿using Lean.Gui;
using UnityEngine;
using UnityEngine.UI;

public class UISettingController : UIScreenBase
{
    [SerializeField]
    private Button btnBackToMain;
    [SerializeField]
    private Button btnFb;
    [SerializeField]
    private Button btnLogin;
    [SerializeField]
    private Button btnCloud;
    [SerializeField]
    private Button btnSupport;
    [SerializeField]
    private Button btnRate;
    [SerializeField]
    private Button btnMoreGame;
    [SerializeField]
    private Button btnCredit;
    [SerializeField]
    private Button btnProvacy;
    [SerializeField]
    private Button btnLanguage;
    [SerializeField]
    private LeanToggle toggleMusic;
    [SerializeField]
    private LeanToggle toggleSound;
    [SerializeField]
    private LeanToggle toggleVibration;
    [SerializeField]
    private LeanToggle toggleNotification;
    [SerializeField]
    private Sprite cloudOn;
    [SerializeField]
    private Sprite cloudOff;
    private void OnEnable()
    {
        toggleMusic.On = ConfigVariables.Music;
        toggleSound.On = ConfigVariables.Sound;
        toggleVibration.On = ConfigVariables.Vibration;
        toggleNotification.On = ConfigVariables.Notification;
    }
    public override void OnCustomStart()
    {
        btnLanguage.onClick.RemoveAllListeners();
        btnLanguage.onClick.AddListener(GameController.Instance.ShowUILanguage);

        btnBackToMain.onClick.AddListener(OnClickBtnBackToMain);
        btnFb.onClick.AddListener(OnClickBtnFB);
        btnLogin.onClick.AddListener(OnClickBtnLogin);
        btnCloud.onClick.AddListener(OnClickBtnCloud);
        btnSupport.onClick.AddListener(OnClickBtnSupport);
        btnRate.onClick.AddListener(OnClickBtnRate);
        btnMoreGame.onClick.AddListener(OnClickBtnMoreGame);
        btnCredit.onClick.AddListener(OnClickBtnCredit);
        btnProvacy.onClick.AddListener(OnClickBtnProvacy);
        btnCloud.GetComponent<Image>().sprite = cloudOff;
        #region Toggle 


        toggleMusic.OnOn.AddListener(() =>
        {
            ConfigVariables.Music = true;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_music_ " + "on");
        });
        toggleMusic.OnOff.AddListener(() =>
        {
            ConfigVariables.Music = false;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_music_ " + "off");
        });

        toggleSound.OnOn.AddListener(() =>
        {
            ConfigVariables.Sound = true;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_sound_" + "on");
        });
        toggleSound.OnOff.AddListener(() =>
        {
            ConfigVariables.Sound = false;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_sound_" + "off");
        });

        toggleVibration.OnOn.AddListener(() => { ConfigVariables.Vibration = true; });
        toggleVibration.OnOff.AddListener(() => { ConfigVariables.Vibration = false; });

        toggleNotification.OnOn.AddListener(() =>
        {
            ConfigVariables.Notification = true;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_Push_" + "on");
        });
        toggleNotification.OnOff.AddListener(() =>
        {
            ConfigVariables.Notification = false;
            TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_Push_" + "off");
        });
        #endregion
    }
    private void OnClickBtnBackToMain()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Back_Button);
        GameController.Instance.ShowUIMainMenu();
    }
    private void OnClickBtnFB()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_facebook_login");
        FacebookManager.Instance.FacebookLoging(callback);

        void callback(bool islogin)
        {
            if (islogin)
            {
            }
            else
            {
                //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("Login Failed!", false, "ok", () =>
                //{
                //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
                //}));
                GameController.Instance.ShowPopupConfirm(ConfigVariables.POPUP_NOTIFICATION_CONTENT_FALSE, null);
            }
            //facebookImage.sprite = FacebookManager.Instance.isLogin ? facebookOnSprite : facebookOffSprite;
        }
        //GameController.Instance.ShowPopupConfirmOpenURL(() => { Application.OpenURL(ConfigVariables.strLinkCompany); });
    }
    private void OnClickBtnLogin()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_gamecenter_login");
        if (!GooglePlayGameManager.Instance.isAuthenticated)
        {
            GooglePlayGameManager.Instance.Activate((temp) =>
            {
                if (temp)
                {
                    UserProfileManager.Instance.SyncUserProfile(false);
                }
                else
                {
                    GameController.Instance.ShowPopupNotification(ConfigVariables.POPUP_NOTIFICATION_CONTENT_FALSE);
                }
            });
        }
        else
        {
            GameController.Instance.ShowPopupNotification(ConfigVariables.POPUP_NOTIFICATION_CONTENT_LOGIN_COMPLETE);
        }
    }
    private void OnClickBtnCloud()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        SaveDataManager.Instance.isCloudSave = !SaveDataManager.Instance.isCloudSave;
        UserProfileManager.Instance.SyncUserProfile(false);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_cloudsave_" + (SaveDataManager.Instance.isCloudSave ? "on" : "off"));
        //GameController.Instance.ShowPopupConfirm("Saved", null);
        if (SaveDataManager.Instance.isCloudSave)
        {
            btnCloud.GetComponent<Image>().sprite = cloudOn;
        }
        else
        {
            btnCloud.GetComponent<Image>().sprite = cloudOff;
        }
    }
    private void OnClickBtnSupport()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_support_click");
        GameController.Instance.ShowPopupSupport();
        //GooglePlayGameManager.Instance.PostLeaderboard(1);
    }
    private void OnClickBtnRate()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Application.OpenURL(ConfigVariables.strLinkCompany);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_rateus_click");
    }
    private void OnClickBtnMoreGame()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Application.OpenURL(ConfigVariables.strLinkCompany);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_moregames_click");
    }
    private void OnClickBtnCredit()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_credits_click");
        GameController.Instance.ShowUICredit();
    }
    private void OnClickBtnProvacy()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Application.OpenURL("http://antada.com.vn/privacy-policy");
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_privacypolicy_click");
    }

    protected override void EventBack()
    {
        bool isShowOtherScreen = false;
        foreach (Transform item in transform.parent)
        {
            if (item.name != transform.name && item.gameObject.activeSelf && item.name.Equals("FPS") == false)
            {
                isShowOtherScreen = true;
            }
        }
        if (GameController.Instance.PopupShowing == false && isShowOtherScreen == false)
            OnClickBtnBackToMain();
    }
}
