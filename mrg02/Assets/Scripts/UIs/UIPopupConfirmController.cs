﻿using System;
using UnityEngine;
using UnityEngine.UI;
public class UIPopupConfirmController : UIPopupBase, ICustomInit
{
    [SerializeField]
    private Text txtContent;
    [SerializeField]
    private Button btnCancel;
    [SerializeField]
    private Button btnConfirm;
    public override void OnCustomStart()
    {
        btnCancel.onClick.AddListener((UnityEngine.Events.UnityAction)(() =>
        {
            SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
            Hide();
        }));
    }

    public void Show(string name, string content, bool showCancel = true, Action actionConfirm = null)
    {
        txtTitle.text = name;
        txtContent.text = content;
        btnCancel.gameObject.SetActive(showCancel);
        if (actionConfirm != null)
        {
            btnConfirm.gameObject.SetActive(true);
            btnConfirm.onClick.AddListener(() =>
            {
                SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);                
                Hide();
                actionConfirm();
            });
        }
        else
        {
            btnConfirm.gameObject.SetActive(false);
        }
        gameObject.SetActive(true);
    }
    public void Show(string name, string content, string option_1_name = "option_1_name", string option_2_name = "option_2_name", Action actionOption_1 = null,Action actionOption_2 = null)
    {
        txtTitle.text = name;
        txtContent.text = content;
        btnCancel.gameObject.SetActive(true);
        btnCancel.GetComponentInChildren<Text>().text = option_1_name;
        btnConfirm.GetComponentInChildren<Text>().text = option_2_name;

        if (actionOption_1 != null)
        {
            btnCancel.gameObject.SetActive(true);
            btnCancel.onClick.AddListener(() =>
            {
                //SoundController.Instance.PlayEffect(EIDSoundEffect.BUTTON);
                Hide();
                actionOption_1();
            });
        }
        else
        {
            btnCancel.gameObject.SetActive(false);
        }

        if (actionOption_2 != null)
        {
            btnConfirm.gameObject.SetActive(true);
            btnConfirm.onClick.AddListener(() =>
            {
                //SoundController.Instance.PlayEffect(EIDSoundEffect.BUTTON);
                Hide();
                actionOption_2();
            });
        }
        else
        {
            btnConfirm.gameObject.SetActive(false);
        }

        gameObject.SetActive(true);
    }
    public override void Hide()
    {
        gameObject.SetActive(false);
    }

    protected override void EventBack()
    {
        throw new NotImplementedException();
    }
}
