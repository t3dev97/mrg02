﻿using UnityEngine;

public class UIHelper : MonoBehaviour
{
    [SerializeField]
    private bool fixWidth;
    [SerializeField]
    private bool fixHeight;
    [SerializeField]
    private float ratio;
    [SerializeField]
    private float aspect;
    [SerializeField]
    private bool isL;
    public float Ratio { get => ratio; set => ratio = value; }

    private void Awake()
    {
        aspect = Camera.main.aspect;
        if (!fixWidth && !fixHeight)
        {
            if (Camera.main.aspect < 0.5625)
            {
                transform.localScale = Vector3.one * (Camera.main.aspect / 0.5625f);
            }
            else
            {
                if (isL)
                {
                    if (Camera.main.aspect > 0.5625)
                    {
                        transform.localScale = Vector3.one * Camera.main.aspect;
                    }
                }
            }
        }
        else
        {
            if (fixWidth)
            {
                if (Camera.main.aspect < 0.5625)
                {
                    Vector3 vec = Vector3.one * (Camera.main.aspect / 0.5625f);
                    vec.y = transform.localScale.y/* + ConfigVariables.SCALE_RATIO*/;
                    transform.localScale = vec;
                }
            }
            if (fixHeight)
            {
                if (Camera.main.aspect < 0.5625)
                {
                    Vector3 vec = Vector3.one * (Camera.main.aspect / 0.5625f);
                    vec.x = transform.localScale.x /*+ ConfigVariables.SCALE_RATIO*/;
                    transform.localScale = vec;
                }
            }
        }
        ratio = transform.localScale.x;
    }
}
