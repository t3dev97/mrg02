﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class UIPopupSupportController : UIPopupBase
{
    [SerializeField]
    private Button btnBug;
    [SerializeField]
    private Button btnLostContent;
    [SerializeField]
    private Button btnCrash;
    [SerializeField]
    private Button btnOther;
    [SerializeField]
    private Button btnNoThanks;

    public override void OnCustomStart()
    {
        btnBug.onClick.AddListener(() => { SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button); SendEmail("Support Just Lines And Beats: Bug"); });
        btnLostContent.onClick.AddListener(() => { SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button); SendEmail("Support Just Lines And Beats: Lost Content"); });
        btnCrash.onClick.AddListener(() => { SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button); SendEmail("Support Just Lines And Beats: Crash"); });
        btnOther.onClick.AddListener(() => { SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button); SendEmail("Support Just Lines And Beats: Other"); });
        btnNoThanks.onClick.AddListener(() => { SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button); Hide(); });
    }
    public void SendEmail(string str)
    {
        string email = "support@antada.com.vn";
        string subject = MyEscapeURL(str);
        string body = MyEscapeURL("Platform: Android \r\n");

        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string URL)
    {
        return UnityWebRequest.EscapeURL(URL).Replace("+", "%20");
    }
    protected override void EventBack()
    {
        Hide();
    }
}
