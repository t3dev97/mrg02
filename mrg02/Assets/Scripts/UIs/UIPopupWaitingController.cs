﻿using DG.Tweening;
using UnityEngine;
public class UIPopupWaitingController : MonoBehaviour
{
    [SerializeField]
    private Transform trsIconWaiting;
    private void OnEnable()
    {
        trsIconWaiting.DOLocalRotate(new Vector3(0,0,-180), 0.2f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental).SetId(this);
    }
    private void OnDisable()
    {
        DOTween.Kill(this);
    }
}
