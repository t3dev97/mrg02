﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
public class UIEndGameController : UIPopupBase
{
    [SerializeField]
    private Transform titleWin;
    [SerializeField]
    private Transform titleLose;
    [SerializeField]
    private Button btnNextPlay;
    [SerializeField]
    private Button btnSkip;
    [SerializeField]
    private Button btnHome;
    [SerializeField]
    private Button btnRetry;
    [SerializeField]
    private Image imgBG;
    [SerializeField]
    private Image imgFade;
    [SerializeField]
    private Image imgStroke;
    [SerializeField] private Image imgAds;


    [SerializeField]
    private ParticleSystem fXDrop;
    [SerializeField]
    private ParticleSystem[] fXCelebrations;
    private EEndGameStage eEndGame;
    [SerializeField]
    private UIHelper_V2 uIHelper;

    public override void OnCustomStart()
    {
        btnNextPlay.onClick.AddListener(OnClickBtnNextPlay);
        btnHome.onClick.AddListener(OnClickBtnHome);
        btnRetry.onClick.AddListener(OnClickRetry);
        btnSkip.onClick.AddListener(OnClickSkip);
    }
    private void EffectFadeTakePhoto()
    {
        DOVirtual.Float(Camera.main.orthographicSize, 2.0f, 0.1f, (x) =>
        {
            Camera.main.orthographicSize = x;
        }).OnComplete(() =>
        {
            DOVirtual.Float(2.0f, 9.0f, 0.45f, (x) =>
            {
                Camera.main.orthographicSize = x;
            });
        });

        imgFade.gameObject.SetActive(true);
        imgFade.color = Color.white;
        DOVirtual.Float(imgFade.color.a, 0.0f, 1.0f, (x) =>
        {
            imgFade.color = new Color(imgFade.color.r, imgFade.color.g, imgFade.color.b, x);
        }).OnComplete(() =>
        {
            imgFade.gameObject.SetActive(false);
        }).SetId(this);
    }
    private void EffectZoomPhoto()
    {
        float scaleX = 1;

        if (uIHelper != null)
            scaleX = uIHelper.Ratio;

        imgStroke.gameObject.SetActive(true);
        DOVirtual.Float(ConfigVariables.SCALE_EFFECT_ZOOM_PHOTO, scaleX, 0.5f, (x) =>
        {
            transform.localScale = new Vector3(x, x, 1);
        }).SetId(this);
    }
    private void OnDisable()
    {
        if (fXDrop != null)
            fXDrop.gameObject.SetActive(false);

        if (Camera.main != null)
        {
            Camera.main.orthographicSize = 6.5f;
        }

        DOTween.Kill(this);
    }
    private void OnClickBtnNextPlay()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (GameController.Instance.CheckNextLevelLive())
        {
            GameController.Instance.GotoBattleGame();
        }
        else
        {
            GameController.Instance.UIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_COMING_SOON, true, () =>
             {
                 GameController.Instance.ShowUIMainMenu();
             });
        }
    }
    private void OnClickSkip()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (GameController.Instance.CheckNextLevelIsUnlock())
        {
            SkillSkipLevel();
        }
        else
        {
            GameController.Instance.ShowPopupConfirmOpenAds(() =>
            {
                AdsManager.Instance.ShowRewardedVideo(SkillSkipLevel, Error, SkillSkipLevel, "SkillSkip_END_GAME", GameController.Instance.CurrentIDLevel.ToString(), "END GAME SCREEN");
            });
        }

        void Error()
        {

        }
    }
    private void SkillSkipLevel()
    {
        GameController.Instance.HandleSkillSkipLevel();
    }
    private void OnClickBtnHome()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowUIMainMenu();
    }
    private void OnClickRetry()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
        {
            if (AdsManager.intervalAd <= 0)
            {
                AdsManager.Instance.ShowInterstitial(Done, Done, "none", "none", "none");
                void Done()
                {
                    Retry();
                    AdsManager.intervalAd = 30;
                }
            }
            else
            {
                Retry();
            }
        }
        else
        {
            Retry();
        }

        void Retry()
        {

            if (eEndGame == EEndGameStage.WIN)
            {
                GameController.Instance.CurrentIDLevel -= 1;
            }
            GameController.Instance.GotoBattleGame();
        }
    }
    public override void Show()
    {
        base.Show();
        btnNextPlay.gameObject.SetActive(true);
        btnSkip.gameObject.SetActive(false);
        GameController.Instance.HideTimeCounter();
        eEndGame = GameController.Instance.EEndGame;

        imgFade.gameObject.SetActive(false);
        imgStroke.gameObject.SetActive(false);

        switch (eEndGame)
        {
            case EEndGameStage.WIN:
                {
                    fXDrop.gameObject.SetActive(true);
                    fXDrop.enableEmission = true;
                    EffectFadeTakePhoto();
                    EffectZoomPhoto();
                }
                break;
            case EEndGameStage.LOSE:
                {
                    float scaleX = 1;

                    if (uIHelper != null)
                        scaleX = uIHelper.Ratio;

                    transform.localScale = new Vector3(scaleX, scaleX, 1);

                }
                break;
        }
    }

    public void ShowWinGame()
    {
        this.Show();
        ShowTitleWin();
        btnNextPlay.gameObject.SetActive(true);
        btnHome.gameObject.SetActive(true);
        btnRetry.gameObject.SetActive(true);
        btnSkip.gameObject.SetActive(false);
        imgBG.gameObject.SetActive(false);
        eEndGame = EEndGameStage.WIN;
    }

    public void ShowLoseGame()
    {
        this.Show();
        ShowTitleLose();
        btnNextPlay.gameObject.SetActive(false);
        btnHome.gameObject.SetActive(true);
        btnRetry.gameObject.SetActive(true);
        btnSkip.gameObject.SetActive(true);
        imgBG.gameObject.SetActive(true);
        eEndGame = EEndGameStage.LOSE;
        if (GameController.Instance.CheckNextLevelIsUnlock())
        {
            imgAds.gameObject.SetActive(false);
        }
        else
        {
            imgAds.gameObject.SetActive(true);
        }
    }
    private void ShowTitleWin()
    {
        titleWin.gameObject.SetActive(true);
        titleLose.gameObject.SetActive(false);
    }
    private void ShowTitleLose()
    {
        titleWin.gameObject.SetActive(false);
        titleLose.gameObject.SetActive(true);
    }

    protected override void EventBack()
    {
        throw new System.NotImplementedException();
    }
}
