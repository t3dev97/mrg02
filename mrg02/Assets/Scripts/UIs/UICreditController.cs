﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UICreditController : UIScreenBase
{
    [SerializeField]
    private Transform trsContent;
    [SerializeField]
    private float durationMoveY;
    [SerializeField]
    private Button btnBackToSetting;
    private Vector3 posDefault;
    private bool isShow = false;
    public override void OnCustomStart()
    {
        posDefault = trsContent.localPosition;
        btnBackToSetting.onClick.AddListener(OnClickBtnBackToSetting);
    }

    private void OnClickBtnBackToSetting()
    {
        Hide();
    }

    private void OnEnable()
    {
        isShow = true;
        Invoke("BoardMoveUp", 1);
    }
    private void BoardMoveUp()
    {
        trsContent.DOLocalMoveY(2500, durationMoveY).SetId(this).SetEase(Ease.Linear);
    }
    private void OnDisable()
    {
        trsContent.localPosition = posDefault;
        DOTween.Kill(this);
    }

    protected override void EventBack()
    {
        OnClickBtnBackToSetting();
    }
    public override void Show()
    {
        base.Show();
        AdsManager.Instance.HideBanner();
    }
    public override void Hide()
    {
        base.Hide();
        if (isShow)
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                AdsManager.Instance.ShowBanner("Screen Banner", "none", "none");
            }
    }
}
