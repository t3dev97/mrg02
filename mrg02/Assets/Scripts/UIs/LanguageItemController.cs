﻿using UnityEngine;
using UnityEngine.UI;

public class LanguageItemController : MonoBehaviour
{
    public static Transform imgOldBackgroundSelect;
    [SerializeField]
    private int id;
    [SerializeField]
    private Button btnSelect;
    [SerializeField]
    private Transform imgBackgroundSelect;
    private ELanguage language;
    public void OnCustomStart(int id)
    {
        this.id = id;
        switch (id)
        {
            case 0: language = ELanguage.VN; break;
            case 1: language = ELanguage.US; break;
            case 2: language = ELanguage.JAPAN; break;
            case 3: language = ELanguage.CHINESE; break;
            default:
                language = ELanguage.US;
                break;
        }
        btnSelect.onClick.AddListener(OnClickBtnSelect);
        if (language == LocalizationsDataManager.Instance.GetCurrentLanguage())
            OnClickBtnSelect();
    }
    private void OnClickBtnSelect()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        imgBackgroundSelect.gameObject.SetActive(false);
        HideOldBackground();
        imgOldBackgroundSelect = imgBackgroundSelect;
        LocalizationsDataManager.Instance.SetLanguage(language);
    }
    private void HideOldBackground()
    {
        if (imgOldBackgroundSelect == null) return;
        imgOldBackgroundSelect.gameObject.SetActive(true);
    }
}
