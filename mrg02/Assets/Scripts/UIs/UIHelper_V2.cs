﻿using UnityEngine;

public class UIHelper_V2 : MonoBehaviour
{
    [SerializeField]
    private bool fixWidth;
    [SerializeField]
    private bool fixHeight;
    private float ratio;

    public float Ratio { get => ratio; set => ratio = value; }

    private void Awake()
    {
        if (!fixWidth && !fixHeight)
        {
            if (Camera.main.aspect < 0.5625)
            {
                transform.localScale = Vector3.one * (Camera.main.aspect / 0.5625f);
            }
        }
        else
        {
            if (fixWidth)// lock x
            {
                if (Camera.main.aspect < 0.5625)
                {
                    Vector3 vec = Vector3.one * (Camera.main.aspect / 0.5625f);
                    vec.y = transform.localScale.y + ConfigVariables.SCALE_RATIO_Y;
                    transform.localScale = vec;
                }
            }
            if (fixHeight)// lock y
            {
                if (Camera.main.aspect < 0.5625)
                {
                    Vector3 vec = Vector3.one * (Camera.main.aspect / 0.5625f);
                    vec.x = transform.localScale.x + ConfigVariables.SCALE_RATIO_X;
                    transform.localScale = vec;
                }
            }
        }
        ratio = transform.localScale.x;
    }
}
