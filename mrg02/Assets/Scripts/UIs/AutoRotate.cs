﻿using UnityEngine;
using UnityEngine.UI;

public class AutoRotate : MonoBehaviour
{    
    [SerializeField] private float x;

    public void UpdateThis()
    {
        return;
        transform.GetChild(0).GetComponent<Image>().sprite = MCache.Instance.PlayerBalls[GameController.Instance.CurrentIdBall].GetComponentInChildren<SpriteRenderer>().sprite;
        transform.GetChild(0).GetComponent<Image>().SetNativeSize();
        transform.GetChild(0).localScale = new Vector3(0.8f, 0.8f, 1);
    }
    private void OnEnable()
    {
        UpdateThis();
    }
    private void Update()
    {
        x -= Time.deltaTime * 25;
        transform.rotation = Quaternion.Euler(0, 0, x);
    }
    //// Update is called once per frame
}
