﻿using UnityEngine;
using UnityEngine.UI;

public class UILanguageController : UIScreenBase
{
    [SerializeField]
    private Button btnBackToMain;
    [SerializeField]
    private ScrollRect scrollMain;

    [SerializeField]
    private LanguageItemController languageItem;
    [SerializeField]
    private ScrollContentFillSize contentFillSize;
    [SerializeField]
    private Transform trsContent;
    private void OnEnable()
    {
        scrollMain.verticalNormalizedPosition = 1;
    }
    public override void OnCustomStart()
    {
        btnBackToMain.onClick.AddListener(OnClickBtnBackToMain);
        SpawnItemFromData();
    }
    private void OnClickBtnBackToMain()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Back_Button);
        Hide();
    }
    private void SpawnItemFromData()
    {
        for (int index = 0; index < trsContent.childCount; index++)
        {
            trsContent.GetChild(index).GetComponent<LanguageItemController>().OnCustomStart(index);
        }
        contentFillSize.FillSize(1);
    }

    protected override void EventBack()
    {
        Hide();
    }
}
