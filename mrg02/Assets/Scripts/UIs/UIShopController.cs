﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShopController : UIScreenBase
{
    [SerializeField]
    private Button btnBackToMain;
    [SerializeField]
    private Button btnVipGold;
    [SerializeField]
    private Button btnAds;
    [SerializeField]
    private ScrollRect scrollMain;
    [SerializeField]
    private SkinItemController skinItem;
    [SerializeField]
    private ScrollContentFillSize contentFillSize;
    [SerializeField]
    private Transform trsContent;
    private List<SkinItemController> skins = new List<SkinItemController>();
    private void Awake()
    {
        if (Camera.main.aspect < 0.5625)
        {
            scrollMain.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1520);
            scrollMain.transform.parent.localPosition = new Vector3(0, 66.0f, 0);
        }
    }
    private void OnEnable()
    {
        scrollMain.verticalNormalizedPosition = 1;
    }
    public override void OnCustomStart()
    {

    }
    public void OnCustomStart_v2()
    {
        skinItem.gameObject.SetActive(false);

        btnBackToMain.onClick.AddListener(OnClickBtnBackToMain);
        btnVipGold.onClick.AddListener(OnClickBtnVipGold);
        btnAds.onClick.AddListener(OnClickBtnAds);
        SpawnItemFromData();
    }
    private void OnClickBtnBackToMain()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Back_Button);
        if (GameController.Instance.CurrentGameStage == EGameStage.PRE)
        {
            GameController.Instance.ShowUIMainMenu();
        }
        else
        {
            this.Hide();
        }
    }
    private void SpawnItemFromData()
    {
        for (int i = 1; i < MCache.Instance.ItemsShopData.Count; i++)
        {

            SkinItemController skin = Instantiate(skinItem, trsContent);
            skin.OnCustomStart(i);
            skin.gameObject.SetActive(true);
            skins.Add(skin);
            MCache.Instance.SkinItemsUI.Add(i, skin);
        }
        Debug.LogError("PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED) ...." + PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED));
        skins[PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED, 1) - 1].ChooseSkin();
    }
    private void OnClickBtnVipGold()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupVipGold();
    }
    private void OnClickBtnAds()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupRemoveAds();
    }

    protected override void EventBack()
    {
        OnClickBtnBackToMain();
    }
}
