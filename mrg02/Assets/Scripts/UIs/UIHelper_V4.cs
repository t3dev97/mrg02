﻿using UnityEngine;

public class UIHelper_V4 : MonoBehaviour
{
    [SerializeField] private Transform trsBtnLeaderBoard;
    [SerializeField] private Transform trsBtnShop;
    [SerializeField] private Transform trsBtnSetting;
    [SerializeField] private Transform trsBtnAchievement;
    [SerializeField] private Transform trsSpace;
    [SerializeField] private Transform trsBGDown;
    [SerializeField] private Transform trsBGGlow;
    [SerializeField] private Transform scrollWorld;//1450    
    //[SerializeField] private Transform trsBGGlow;
    private void Awake()
    {
        Debug.Log("Camera.main.aspect" + Camera.main.aspect);
        if (Camera.main.aspect < 0.5625)
        {
            transform.localScale = Vector3.one * (Camera.main.aspect / 0.5625f);

            trsBGDown.GetComponent<RectTransform>().sizeDelta = new Vector2(1536, 2800);

            //trsBGGlow.GetComponent<RectTransform>().sizeDelta = new Vector2(1536, 2800);
            trsBGGlow.localPosition = new Vector3(0, -2400, 0);

            scrollWorld.GetComponent<RectTransform>().sizeDelta = new Vector2(1080, 1450);
        }
        else
        {
            if (Camera.main.aspect > 0.7)
            {
                trsBtnSetting.localPosition = new Vector3(480, -115, 0);
                trsBtnAchievement.localPosition = new Vector3(480, -320, 0);

                trsBtnLeaderBoard.localPosition = new Vector3(-480, -115, 0);
                trsBtnShop.localPosition = new Vector3(-480, -320, 0);

                trsSpace.GetComponent<RectTransform>().sizeDelta = new Vector2(1400, 100);
            }
        }
    }
}
