﻿using Lean.Gui;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupPauseController : UIPopupBase
{
    [SerializeField]
    private Button btnHome;
    [SerializeField]
    private Button btResume;
    [SerializeField]
    private Button btnReTry;
    [SerializeField]
    private LeanToggle toggleMusic;
    [SerializeField]
    private LeanToggle toggleSound;
    private bool isCanBack = false;
    public override void OnCustomStart()
    {
        btnHome.onClick.AddListener(OnClickBtnHome);
        btResume.onClick.AddListener(OnClickBtnResume);
        btnReTry.onClick.AddListener(OnClickBtnRetry);


        toggleMusic.OnOn.AddListener(() => { ConfigVariables.Music = true; });
        toggleMusic.OnOff.AddListener(() => { ConfigVariables.Music = false; });

        toggleSound.OnOn.AddListener(() => { ConfigVariables.Sound = true; });
        toggleSound.OnOff.AddListener(() => { ConfigVariables.Sound = false; });
    }
    private void OnEnable()
    {
        toggleMusic.On = ConfigVariables.Music;
        toggleSound.On = ConfigVariables.Sound;
        Invoke("ResetToBack", 0.2f);
    }
    private void OnDisable()
    {
        isCanBack = false;
    }
    private void ResetToBack()
    {
        isCanBack = !isCanBack;
    }
    private void OnClickBtnHome()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
        {
            if (AdsManager.intervalAd <= 0)
            {
                AdsManager.Instance.ShowInterstitial(Done, Done, "none", "none", "none");
                void Done()
                {
                    Activited();
                    AdsManager.intervalAd = 30;
                }
            }
            else
            {
                Activited();
            }
        }
        else
        {
            Activited();
        }

        void Activited()
        {
            GameController.Instance.ShowUIMainMenu();
        }
    }
    private void OnClickBtnResume()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Hide();
    }
    private void OnClickBtnRetry()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
        {
            if (AdsManager.intervalAd <= 0)
            {
                AdsManager.Instance.ShowInterstitial(Done, Done, "none", "none", "none");
                void Done()
                {
                    Activited();
                    AdsManager.intervalAd = 30;
                }
            }
            else
            {
                Activited();
            }
        }
        else
        {
            Activited();
        }

        void Activited()
        {
            GameController.Instance.GotoBattleGame();
        }
    }

    protected override void EventBack()
    {
        if (isCanBack)
            OnClickBtnResume();
    }
}
