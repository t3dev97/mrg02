﻿using UnityEngine;
using UnityEngine.UI;

public class UIAchievementController : UIScreenBase
{
    [SerializeField]
    private Button btnBackToMain;
    [SerializeField]
    private ScrollRect scrollMain;

    [SerializeField]
    private AchievenmentItemController achievenmentItem;
    [SerializeField]
    private ScrollContentFillSize contentFillSize;
    [SerializeField]
    private Transform trsContent;
    private void OnEnable()
    {
        scrollMain.verticalNormalizedPosition = 1;
    }
    public override void OnCustomStart()
    {
        btnBackToMain.onClick.AddListener(OnClickBtnBackToMain);
        SpawnItemFromData();
    }
    private void OnClickBtnBackToMain()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Back_Button);
        GameController.Instance.ShowUIMainMenu();
    }
    private void SpawnItemFromData()
    {
        achievenmentItem.gameObject.SetActive(false);
        for (int i = 1; i < 30; i++)
        {
            AchievenmentItemController achievenment = Instantiate(achievenmentItem, trsContent);
            achievenment.OnCustomStart(i);
            achievenment.gameObject.SetActive(true);
        }
        contentFillSize.FillSize(1);
    }

    protected override void EventBack()
    {
        OnClickBtnBackToMain();
    }
}
