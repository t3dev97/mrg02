﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupReviveController : UIPopupBase
{
    [SerializeField]
    private Button btnRevive;
    [SerializeField]
    private Button btnBack;
    public override void OnCustomStart()
    {
        btnRevive.onClick.AddListener(OnClickBtnRevive);
        btnBack.onClick.AddListener(OnClickBtnBack);
    }
    private void OnClickBtnRevive()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupCommingSoon();
    }
    private void OnClickBtnBack()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Hide();
    }

    protected override void EventBack()
    {
        throw new System.NotImplementedException();
    }
}
