﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIMapLevel : UIScreenBase
{
    [SerializeField]
    private ScrollRect scrMain;
    [SerializeField]
    private ItemLevelToChoose toChoose;
    [SerializeField]
    private Transform trsContent;
    public override void OnCustomStart()
    {
        int numberLevel = MCache.Instance.Levels.Count;
        Dictionary<int, LevelController> levels = MCache.Instance.Levels;
        foreach (var item in levels)
        {
            ItemLevelToChoose itemLevel = Instantiate(toChoose.gameObject, trsContent).GetComponent<ItemLevelToChoose>();
            itemLevel.IdLevel = item.Value.Id;
            itemLevel.TxtLevelName.text = "Level " + itemLevel.IdLevel;
            itemLevel.GetComponent<Button>().onClick.AddListener(() =>
            {
                itemLevel.OnClickThis();
            });
            itemLevel.gameObject.SetActive(true);
        }
        //for (int i = 0; i < numberLevel; i++)
        //{
        //    ItemLevelToChoose itemLevel = Instantiate(toChoose.gameObject, trsContent).GetComponent<ItemLevelToChoose>();
        //    itemLevel.IdLevel = i + 1;
        //    itemLevel.TxtLevelName.text = "Level " + itemLevel.IdLevel;
        //    itemLevel.GetComponent<Button>().onClick.AddListener(() =>
        //    {
        //        itemLevel.OnClickThis();
        //    });
        //    itemLevel.gameObject.SetActive(true);
        //}
    }

    protected override void EventBack()
    {
       
    }
}
