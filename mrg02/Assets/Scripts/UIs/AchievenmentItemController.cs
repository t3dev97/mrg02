﻿using UnityEngine;
using UnityEngine.UI;

public class AchievenmentItemController : MonoBehaviour
{
    public static Transform imgOldBackgroundSelect;
    [SerializeField]
    private int id;
    [SerializeField]
    private Button btnSelect;
    [SerializeField]
    private Transform imgBackgroundSelect;

    public void OnCustomStart(int id)
    {
        this.id = id;
        btnSelect.onClick.AddListener(OnClickBtnSelect);
        imgBackgroundSelect.gameObject.SetActive(false);
    }
    private void OnClickBtnSelect()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        imgBackgroundSelect.gameObject.SetActive(true);
        HideOldBackground();
        imgOldBackgroundSelect = imgBackgroundSelect;
        //GameController.Instance.CurrentIdBall = id;        
        GameController.Instance.ShowPopupCommingSoon();
    }
    private void HideOldBackground()
    {
        if (imgOldBackgroundSelect == null) return;
        imgOldBackgroundSelect.gameObject.SetActive(false);
    }
}
