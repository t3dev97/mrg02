﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupRemoveAdsController : UIPopupBase
{
    [SerializeField]
    private Button btnGet;
    [SerializeField]
    private Button btnCancel;
    public override void OnCustomStart()
    {
        btnGet.onClick.AddListener(OnClickBtnGet);
        btnCancel.onClick.AddListener(OnClickBtnCancel);
        if (MCache.Instance.UserData.IsRemove)
        {

        }
    }
    private void OnClickBtnGet()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        IAPManager.Instance.BuyNo_ads((b) =>
        {
            //noAdsObj.SetActive(!b);
            MCache.Instance.UserData.IsRemove = b;
        });
    }
    private void OnClickBtnCancel()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        Hide();
    }

    protected override void EventBack()
    {
        Hide();
    }
}
