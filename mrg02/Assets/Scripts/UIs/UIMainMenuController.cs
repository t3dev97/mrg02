﻿using Mosframe;
using UnityEngine;
using UnityEngine.UI;
public class UIMainMenuController : UIScreenBase
{
    public enum EScrollActived
    {
        Main = 0,
        Level
    }

    public delegate void OnScrollActionChange(EScrollActived actived);
    public OnScrollActionChange onScrollActionChange;

    [Header("Buttons")]
    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Button btnShowMain;
    [SerializeField]
    private Button btnRemoveAds;
    [SerializeField]
    private Button btnShop;
    [SerializeField]
    private Button btnSetting;
    [SerializeField]
    private Button btnAchievement;
    [SerializeField]
    private Button btnLeaderBoard;
    [SerializeField] private Button btnNext;
    [SerializeField] private Button btnBack;
    [Header("Objects Preference")]
    [SerializeField]
    private ScrollRect scrollMain;
    [SerializeField]
    private ScrollRect scrollLevel;
    [SerializeField]
    private Scrollbar scrollBarLevel;
    [SerializeField]
    private Scrollbar scrollBarMain;
    [SerializeField]
    private DynamicVScrollView dynamicVLevel;
    [SerializeField]
    private Transform trsFade;
    [SerializeField]
    private Transform trsContent;
    [SerializeField]
    private Text txtStar;
    private float lastValue;
    private Vector3 posHeadScrollMain;
    private float valueScrollMain = 0;
    private float valueScrollLevel = 0;
    [SerializeField]
    private bool isViewMain = false;
    [SerializeField]
    private bool isViewLevel = false;
    [SerializeField]
    private Transform imgBGTop;
    private bool isShowBtnShowMain = false;

    private EScrollActived scrollActived = EScrollActived.Main;
    private bool isInit = false;
    [SerializeField]
    private DynamicScrollViewItemExample dynamicScrollViewItem;
    [SerializeField]
    private DynamicScrollViewItemExample dynamicScrollViewItem2;
    [SerializeField]
    private Transform trsContentLevelItem;
    [SerializeField]
    private Transform trsSpace;
    [SerializeField] private Transform trsMiniWorld;
    [SerializeField] private Transform trsMiniWorldItem;

    [SerializeField] private Transform trsWorldController;
    [SerializeField] private WorldController worldController;
    [SerializeField] private swipe swipeController;
    private int currentWorld = 0;

    public bool IsShowBtnShowMain { get => isShowBtnShowMain; set => isShowBtnShowMain = value; }

    void OnEnable()
    {
        //Subscribe to the Scrollbar event
        //lastValue = scrollBarLevel.value;
        HideBtnGotoMain();
    }

    public override void OnCustomStart()
    {
        #region Add Listener
        btnPlay.onClick.AddListener(OnClickBtnPlay);
        btnShowMain.onClick.AddListener(OnClickBtnShowMain);
        btnRemoveAds.onClick.AddListener(OnClickBtnRemoveAds);
        btnShop.onClick.AddListener(OnClickBtnShop);
        btnSetting.onClick.AddListener(OnClickBtnSetting);
        btnAchievement.onClick.AddListener(OnClickBtnAchievement);
        btnLeaderBoard.onClick.AddListener(OnClickBtnLeaderBoard);
        btnNext.onClick.AddListener(OnClickBtnNext);
        btnBack.onClick.AddListener(OnClickBtnBack);
        #endregion

        //scrollMain.onValueChanged.AddListener(OnScrollMainValueChange);
        //scrollLevel.onValueChanged.AddListener(OnScrollLevelValueChange);
        //scrollBarLevel.onValueChanged.AddListener(OnScrollBarLevelValueChange);
        //scrollBarMain.onValueChanged.AddListener(OnScrollBarMainValueChange);

        onScrollActionChange += ScrollActionChange;
    }
    public override void Show()
    {
        base.Show();
        btnPlay.gameObject.SetActive(true);
        ShowBGTop();
        //onScrollActionChange?.Invoke(EScrollActived.Main);

        isViewMain = true;
        isViewLevel = false;
        //trsContent.GetComponent<RectTransform>().position = Vector3.zero;
        print("MCache.Instance.UserData.StarNumber " + MCache.Instance.UserData.StarNumber);
        //GooglePlayGameManager.Instance.PostLeaderboard(MCache.Instance.UserData.StarNumber);
    }
    public void UpdateLevels()
    {
        trsMiniWorldItem.gameObject.SetActive(false);
        Debug.Log("fadfdfg");
        for (int idLevel = 0; idLevel < 30; idLevel++)
        {
            WorldController world = Instantiate(worldController, trsWorldController);
            world.UpdateWorld(idLevel);
            world.UpdateTitle("WORLD " + (idLevel + 1));

            var cache = Instantiate(trsMiniWorldItem, trsMiniWorld);
            cache.gameObject.SetActive(true);
        }



        //dynamicScrollViewItem.gameObject.SetActive(false);
        //dynamicScrollViewItem2.gameObject.SetActive(false);
        //// First
        //DynamicScrollViewItemExample temp = Instantiate(dynamicScrollViewItem, trsContentLevelItem);
        //temp.onUpdateItem(0);
        //temp.gameObject.SetActive(true);

        //for (int i = 1; i < 30; i++)
        //{
        //    DynamicScrollViewItemExample cache = Instantiate(dynamicScrollViewItem2, trsContentLevelItem);
        //    cache.onUpdateItem(i);
        //    cache.gameObject.SetActive(true);
        //}
        //Transform space = Instantiate(trsSpace, trsContentLevelItem);
        //space.gameObject.SetActive(true);
    }
    public override void Hide()
    {
        base.Hide();
        btnPlay.gameObject.SetActive(false);
        onScrollActionChange -= ScrollActionChange;
    }
    private void OnClickBtnRemoveAds()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.main_menu.ToString().ToLower() + "_buynoads_click");
        GameController.Instance.ShowPopupRemoveAds();
    }
    private void OnClickBtnShop()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.main_menu.ToString().ToLower() + "_shop_click");
        GameController.Instance.ShowUIShop();
    }
    private void OnClickBtnSetting()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowUISetting();
    }
    private void OnClickBtnAchievement()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        GameController.Instance.ShowPopupCommingSoon();
    }
    private void OnClickBtnBack()
    {
        currentWorld = (currentWorld > 0) ? currentWorld - 1 : currentWorld;
        swipeController.MoveTo(currentWorld);
    }
    private void OnClickBtnNext()
    {
        currentWorld = (currentWorld < 31) ? currentWorld + 1 : currentWorld;
        swipeController.MoveTo(currentWorld);
    }
    private void OnClickBtnLeaderBoard()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Click_Button);
        //GameController.Instance.ShowUIAchievement();
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.main_menu.ToString().ToLower() + "_leaderboard_click");
        GooglePlayGameManager.Instance.Activate((temp) =>
        {
            if (temp)
            {
                GooglePlayGameManager.Instance.ShowLeaderboard();
            }
            else
            {
                GameController.Instance.ShowPopupNotification("Login failed");
            }
        });
    }
    private void OnClickBtnPlay()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Select_Level);
        TrackingLog.Instance.SendEventWithSessionInfo("gameplay_stage00_start", "gameplay_level_sort", GameController.Instance.CurrentIDLevel.ToString());
        TrackingLog.Instance.trackingHelper.userJourneySum.gameplay_stage_start++;
        TrackingLog.Instance.CurrentGameScreen = GameScreen.action_phase;
        GameController.Instance.GotoBattleGame();
    }
    private void OnClickBtnShowMain()
    {
        scrollMain.verticalNormalizedPosition = 1;
        imgBGTop.gameObject.SetActive(true);
        HideBtnGotoMain();
    }
    public bool CheckShowBtnGotoMain()
    {
        return btnShowMain.gameObject.activeSelf;
    }
    public void ShowBGTop()
    {
        //imgBGTop.gameObject.SetActive(true);
    }
    public void HideBGTop()
    {
        //imgBGTop.gameObject.SetActive(false);
    }
    public void HideBtnRemoveAds()
    {
        btnRemoveAds.gameObject.SetActive(false);
    }
    public void ShowBtnRemoveAds()
    {
        btnRemoveAds.gameObject.SetActive(true);
    }
    public void UpdateStar(int number)
    {
        txtStar.text = number.ToString();
    }
    private void OnScrollMainValueChange(Vector2 vector2)
    {
        //if (isInit && vector2.y > 0.05f && scrollActived != EScrollActived.Level)// Down
        //{
        //    print("vector2.y " + vector2.y);
        //    onScrollActionChange?.Invoke(EScrollActived.Level);

        //}
        //else if (scrollActived != EScrollActived.Main)//Up
        //{

        //    onScrollActionChange?.Invoke(EScrollActived.Main);
        //}

        //isInit = true;
    }
    private void OnScrollLevelValueChange(Vector2 vector2)
    {
    }
    private void OnScrollBarLevelValueChange(float value)
    {
        //valueScrollLevel = value;
        //if (lastValue > value)
        //{
        //    //UnityEngine.Debug.Log("OnScrollBarLevelValueChange UP: " + value);
        //}
        //else
        //{
        //    //UnityEngine.Debug.Log("OnScrollBarLevelValueChange DOWN: " + value);
        //    if (value > 1)
        //    {
        //        // OffScrollLevel();
        //        //OnScrollMain();
        //        isViewLevel = false;
        //        isViewMain = true;
        //    }
        //}

        //lastValue = value;
        ////if (isViewLevel)
        ////{
        ////    if (value > 1)
        ////    {
        ////        OffScrollLevel();
        ////        OnScrollMain();
        ////        isViewLevel = false;
        ////        isViewMain = true;
        ////    }
        ////}

    }

    private void OnScrollBarMainValueChange(float value)
    {
        //var val = System.Math.Round(value, 1, System.MidpointRounding.AwayFromZero);
        //Debug.LogError("OnScrollBarMainValueChange: " + val);

        //if (val < 0.05f && scrollActived != EScrollActived.Level)// Down
        //{
        //    onScrollActionChange?.Invoke(EScrollActived.Level);

        //}
        //else if (scrollActived != EScrollActived.Main)//Up
        //{

        //    onScrollActionChange?.Invoke(EScrollActived.Main);
        //}

        //if (Mathf.Approximately((float)val, 1))
        //{
        //    Debug.LogError("init done");
        //    isInit = true;
        //}

    }

    public void ScrollActionChange(EScrollActived active)
    {
        //Debug.LogError("ScrollActionChange: " + active);
        switch (active)
        {
            case EScrollActived.Main:
                {
                    scrollMain.enabled = true;
                    scrollLevel.enabled = false;
                    btnShowMain.gameObject.SetActive(false);
                }
                break;
            case EScrollActived.Level:
                {
                    scrollMain.enabled = false;
                    scrollLevel.enabled = true;
                    btnShowMain.gameObject.SetActive(true);
                }
                break;
        }

        scrollActived = active;
    }
    public void ShowBtnGotoMain()
    {
        btnShowMain.gameObject.SetActive(true);
    }
    public void HideBtnGotoMain()
    {
        btnShowMain.gameObject.SetActive(false);
    }

    protected override void EventBack()
    {

    }
}
