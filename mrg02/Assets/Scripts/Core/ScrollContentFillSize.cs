﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollContentFillSize : MonoBehaviour
{
    [SerializeField]
    GridLayoutGroup grid;
    public void FillSize(int numberCol = 3)
    {
        int numberChild = (transform.childCount / numberCol) + 1;
        float height = numberChild * (grid.cellSize.y + grid.spacing.y);
        GetComponent<RectTransform>().sizeDelta = new Vector2(0, height);
    }
}
