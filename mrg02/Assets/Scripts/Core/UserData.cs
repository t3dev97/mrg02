﻿using System.Collections.Generic;
using UnityEngine;

public class UserData
{
    private int id;
    private int idSkinSelected;
    private List<int> idSkins = new List<int>();
    private List<int> idLevelsUnlocked = new List<int>();
    private List<int> idLevelsComplete = new List<int>();
    private List<int> idAchiementDoned = new List<int>();
    private int starNumber;
    private int hintSkillNumber;
    private int shadowBallSkillNumber;
    private int skipSkillNumber;
    private bool isVip;
    private bool isRemoveAds;

    public int Id { get => id; set => id = value; }
    public List<int> IdSkins { get => idSkins; set => idSkins = value; }
    public List<int> IdLevelsUnlocked { get => idLevelsUnlocked; set => idLevelsUnlocked = value; }
    public int StarNumber { get => starNumber; set => starNumber = value; }
    public int IdSkinSelected { get => idSkinSelected; set => idSkinSelected = value; }
    public List<int> IdLevelsComplete { get => idLevelsComplete; set => idLevelsComplete = value; }
    public int HintSkillNumber { get => hintSkillNumber; set => hintSkillNumber = value; }
    public int ShadowBallSkillNumber { get => shadowBallSkillNumber; set => shadowBallSkillNumber = value; }
    public int SkipTurnSkillNumber { get => skipSkillNumber; set => skipSkillNumber = value; }
    public bool IsVip
    {
        get => isVip;
        set
        {
            isVip = value;
            if (value == true)
            {
                Debug.Log("IsVip");
                AdsManager.Instance.HideBanner();
            }
        }
    }
    public bool IsRemove
    {
        get => isRemoveAds;
        set
        {
            isRemoveAds = value;
            if (value == true)
            {
                Debug.Log("IsRemove");
                AdsManager.Instance.HideBanner();
            }
        }
    }
    public List<int> IdAchiementDoned { get => idAchiementDoned; set => idAchiementDoned = value; }

    public void ToUserData(UserProfile userProfile)
    {
        id = userProfile.Version;
        idSkins.Clear();
        idLevelsUnlocked.Clear();
        idLevelsComplete.Clear();
        idAchiementDoned.Clear();
        for (int index = 0; index < userProfile.SkinUnlockedId.Count; index++)
        {
            idSkins.Add(int.Parse(userProfile.SkinUnlockedId[index]));
        }
        for (int index = 0; index < userProfile.LevelsUnlockId.Count; index++)
        {
            idLevelsUnlocked.Add(int.Parse(userProfile.LevelsUnlockId[index]));
        }
        for (int index = 0; index < userProfile.LevelsCompleteId.Count; index++)
        {
            idLevelsComplete.Add(int.Parse(userProfile.LevelsCompleteId[index]));
        }
        for (int index = 0; index < userProfile.AchievementDoneId.Count; index++)
        {
            idAchiementDoned.Add(int.Parse(userProfile.AchievementDoneId[index]));
        }
        starNumber = userProfile.TotalStar;
        hintSkillNumber = userProfile.TotalStar;
        shadowBallSkillNumber = userProfile.TotalStar;
        skipSkillNumber = userProfile.TotalStar;
        isVip = userProfile.IsVipGold;
        isRemoveAds = userProfile.IsRemoveAds;
    }
}
