﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
public class SoundController : MonoBehaviour
{
    public static SoundController Instance;
    [SerializeField]
    private AudioClip[] musics;
    [SerializeField]
    private AudioClip[] effects;
    private AudioSource audioBG;
    [SerializeField]
    private Transform trsContentBeatInLevel;
    [SerializeField]
    private List<int> listMusicActive = new List<int>();
    [SerializeField]
    private List<int> listBeatActive = new List<int>();
    [SerializeField]
    private List<AudioSource> listAudioBeat = new List<AudioSource>();

    public List<int> ListMusicActive { get => listMusicActive; set => listMusicActive = value; }
    public List<int> ListBeatActive { get => listBeatActive; set => listBeatActive = value; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    public void PlayMusicBG(int id, bool loop = true)
    {
        if (ConfigVariables.Music == false) return;
        if (id >= musics.Length)
            id = musics.Length - 1;

        if (listMusicActive.Contains(id)) return;
        listMusicActive.Add(id);
        GameObject go = new GameObject(musics[id].name);
        go.transform.parent = transform;
        audioBG = go.AddComponent<AudioSource>();
        audioBG.clip = musics[id];
        audioBG.volume = 0.55f;
        audioBG.loop = loop;
        audioBG.Play();
    }
    public void PlayMuteBeat(int musicID, int beatID)
    {
        MusicData musicData = MCache.Instance.MusicsData[musicID];
        listBeatActive.Add(beatID);
        GameObject beat = new GameObject(musicData.Name + "->" + musicData.Audios[beatID - 1].name);
        beat.transform.parent = trsContentBeatInLevel;
        AudioSource audioBeat = beat.AddComponent<AudioSource>();
        listAudioBeat.Add(audioBeat);
        audioBeat.clip = musicData.Audios[beatID - 1];
        audioBeat.volume = 0;
        audioBeat.loop = true;
        audioBeat.Play();
    }
    public void PlayOnBeat(int beatID)
    {
        if (ConfigVariables.Music == false) return;
        int index = listBeatActive.Find(x => x == beatID);
        DOVirtual.Float(0, 1, ConfigVariables.TIME_DELAY_OFF_SOUND, (x) =>
        {
            listAudioBeat[index - 1].volume = x;
        }).SetId(this);
    }
    public void PlayEffect(EIDSoundEffect eID, float timeSound = -1)
    {
        if (ConfigVariables.Sound == false) return;
        GameObject go = new GameObject(eID.ToString());
        go.transform.parent = transform;
        AudioSource audio = go.AddComponent<AudioSource>();
        Debug.Log("PlayEffect =>>" + eID.ToString());
        audio.clip = MCache.Instance.Sfx[eID.ToString()];
        audio.volume = ConfigVariables.SFX_VOLUMN;
        audio.Play();
        DOVirtual.DelayedCall(timeSound <= 0 ? ConfigVariables.TIME_AUTO_DESTROY_SOUND_AFFECT : timeSound, () =>
        {
            DOVirtual.Float(audio.volume, 0, timeSound <= 0 ? ConfigVariables.TIME_AUTO_DESTROY_SOUND_AFFECT : timeSound, (x) =>
            {
                audio.volume = x;
            }).OnComplete(() =>
            {
                Destroy(go);
            }).SetEase(Ease.Linear).SetId(this);
        }).SetId(this);
    }
    public void StopMusicBG()
    {
        if (audioBG == null) return;

        DOVirtual.Float(audioBG.volume, 0, ConfigVariables.TIME_DELAY_OFF_SOUND, (x) =>
        {
            audioBG.volume = x;
        }).OnComplete(() =>
        {
            listMusicActive.Clear();
            Destroy(audioBG.gameObject);
            DOTween.Kill(this);
        }).SetId(this);
    }
    public void StopAllBeat()
    {
        foreach (Transform item in trsContentBeatInLevel)
        {
            AudioSource audio = item.GetComponent<AudioSource>();
            DOVirtual.Float(audio.volume, 0, ConfigVariables.TIME_DELAY_OFF_SOUND, (x) =>
            {
                audio.volume = x;
            }).OnComplete(() =>
            {
                listBeatActive.Clear();
                listAudioBeat.Clear();
                Destroy(item.gameObject);
            }).SetId(this);
        }
    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
}
