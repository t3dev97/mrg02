﻿using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;
    private Vector3 posDefault;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    private void Start()
    {
        posDefault = transform.localPosition;
    }
    public void Shake()
    {
        if (ConfigVariables.Vibration)
        {
            transform.DOShakePosition(0.4f,1,10,45).OnComplete(() =>
            {
                transform.localPosition = posDefault;
            });
        }
    }
}
