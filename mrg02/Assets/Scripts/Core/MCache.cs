﻿using System.Collections.Generic;
using UnityEngine;

public class MCache : MonoBehaviour
{
    public static MCache Instance;
    private Dictionary<int, LevelItem> levelsUI = new Dictionary<int, LevelItem>();
    private Dictionary<int, SkinItemController> skinItemsUI = new Dictionary<int, SkinItemController>();
    private Dictionary<int, LevelController> levels = new Dictionary<int, LevelController>();
    private Dictionary<int, LevelController> levelsHint = new Dictionary<int, LevelController>();
    private Dictionary<int, BallBase> playerBalls = new Dictionary<int, BallBase>();
    private Dictionary<int, ItemShopRow> itemsShopData = new Dictionary<int, ItemShopRow>();
    private Dictionary<int, MusicData> musicsData = new Dictionary<int, MusicData>();
    private Dictionary<string, Material> materials = new Dictionary<string, Material>();
    private Dictionary<string, Sprite> imgBalls = new Dictionary<string, Sprite>();
    private Dictionary<string, Sprite> imgItemsShop = new Dictionary<string, Sprite>();
    private Dictionary<string, Sprite> imgMasks = new Dictionary<string, Sprite>();
    private Dictionary<int, Sprite> imgIconLevels = new Dictionary<int, Sprite>();
    private Dictionary<int, MusicLevelsData> musicLevelsData = new Dictionary<int, MusicLevelsData>();
    private Dictionary<int, HintSkillData> hintsSkillData = new Dictionary<int, HintSkillData>();
    private Dictionary<string, AudioClip> sfx = new Dictionary<string, AudioClip>();
    private Dictionary<string, LevelItem> levelsUItem = new Dictionary<string, LevelItem>();
    private UserData userData = new UserData();
    private int currentLevel = 0;
    public Dictionary<int, LevelController> Levels { get => levels; }
    public Dictionary<int, BallBase> PlayerBalls { get => playerBalls; set => playerBalls = value; }
    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }
    public Dictionary<string, Sprite> ImgBalls { get => imgBalls; set => imgBalls = value; }
    public Dictionary<int, ItemShopRow> ItemsShopData { get => itemsShopData; set => itemsShopData = value; }
    public UserData UserData { get => userData; set => userData = value; }
    public Dictionary<int, LevelItem> LevelsUI { get => levelsUI; set => levelsUI = value; }
    public Dictionary<int, MusicData> MusicsData { get => musicsData; set => musicsData = value; }
    public Dictionary<string, Material> MaterialsLine { get => materials; set => materials = value; }
    public Dictionary<string, Sprite> ImgMasks { get => imgMasks; set => imgMasks = value; }
    public Dictionary<string, Sprite> ImgItemsShop { get => imgItemsShop; set => imgItemsShop = value; }
    public Dictionary<int, Sprite> ImgIconLevels { get => imgIconLevels; set => imgIconLevels = value; }
    public Dictionary<int, MusicLevelsData> MusicLevelsData { get => musicLevelsData; set => musicLevelsData = value; }
    public Dictionary<int, SkinItemController> SkinItemsUI { get => skinItemsUI; set => skinItemsUI = value; }
    public Dictionary<int, HintSkillData> HintsSkillData { get => hintsSkillData; set => hintsSkillData = value; }
    public Dictionary<int, LevelController> LevelsHint { get => levelsHint; set => levelsHint = value; }
    public Dictionary<string, AudioClip> Sfx { get => sfx; set => sfx = value; }
    public Dictionary<string, LevelItem> LevelsUItem { get => levelsUItem; set => levelsUItem = value; }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        DontDestroyOnLoad(this);

        #region Load Levels
        var data = Resources.LoadAll("Prefabs/Levels", typeof(LevelController));
        foreach (LevelController item in data)
        {
            if (!levels.ContainsKey(item.Id))
            {
                levels.Add(item.Id, item);
            }
            else
            {
                //Debug.Log("Level ID " + item.Id + "Error", this, true);
            }
        }

        data = Resources.LoadAll("Prefabs/UIs/UIScreens/UIMainMenu/ScrollLevel", typeof(LevelItem));
        foreach (LevelItem item in data)
        {
            if (!levelsUItem.ContainsKey(item.name))
            {
                levelsUItem.Add(item.name, item);
            }
            else
            {
                //Debug.Log("Level ID " + item.Id + "Error", this, true);
            }
        }

        data = Resources.LoadAll("Prefabs/Levels Hint", typeof(LevelController));
        foreach (LevelController item in data)
        {
            if (!levelsHint.ContainsKey(item.Id))
            {
                levelsHint.Add(item.Id, item);
            }
            else
            {
                //Debug.Log("Level ID " + item.Id + "Error", this, true);
            }
        }
        Debug.Log("Load done: levels hint" + levelsHint.Count);

        #endregion

        #region Load SFX
        data = Resources.LoadAll("Sounds/SFX", typeof(AudioClip));
        foreach (AudioClip item in data)
        {
            if (!sfx.ContainsKey(item.name))
            {
                sfx.Add(item.name, item);
            }
        }
        Debug.Log("sfx " + sfx.Count);
        #endregion

        #region Load PlayerBalls
        data = Resources.LoadAll("Prefabs/Players/PlayerBalls", typeof(BallBase));
        foreach (BallBase item in data)
        {
            if (!playerBalls.ContainsKey(item.Id))
            {
                playerBalls.Add(item.Id, item);
            }
            else
            {
                //Debug.Log("playerBalls ID " + item.Id + "Error", this, true);
            }
        }
        //Debug.Log("Load done: playerBalls " + playerBalls.Count, this, true);
        #endregion

        #region Load ImgBalls
        data = Resources.LoadAll("Textures/Characters", typeof(Sprite));
        foreach (Sprite item in data)
        {
            if (!imgBalls.ContainsKey(item.name))
            {
                imgBalls.Add(item.name, item);
            }
            else
            {
                //Debug.Log("playerBalls ID " + item.name + "Error", this, true);
            }
        }
        //Debug.Log("Load done: playerBalls " + playerBalls.Count, this, true);
        #endregion

        #region Load ImgItemShop
        data = Resources.LoadAll("Textures/Shop", typeof(Sprite));
        foreach (Sprite item in data)
        {
            if (!imgItemsShop.ContainsKey(item.name))
            {
                imgItemsShop.Add(item.name, item);
            }
            else
            {
                //Debug.Log("ImgItemShop ID " + item.name + "Error", this, true);
            }
        }
        //Debug.Log("Load done: ImgItemShop " + imgItemsShop.Count, this, true);
        #endregion

        #region Load Img Masks
        data = Resources.LoadAll("Textures/Masks", typeof(Sprite));
        foreach (Sprite item in data)
        {
            if (!imgMasks.ContainsKey(item.name))
            {
                imgMasks.Add(item.name, item);
            }
            else
            {
                //Debug.Log("imgMasks ID " + item.name + "Error", this, true);
            }
        }
        //Debug.Log("Load done: imgMasks " + imgMasks.Count, this, true);
        #endregion

        #region Load Img IconLevel
        data = Resources.LoadAll("Textures/IconLevels", typeof(Sprite));
        foreach (Sprite item in data)
        {
            if (!imgIconLevels.ContainsKey(int.Parse(item.name)))
            {
                imgIconLevels.Add(int.Parse(item.name), item);
            }
            else
            {
                //Debug.Log("imgIconLevels ID " + item.name + "Error", this, true);
            }
        }
        //Debug.Log("Load done: imgIconLevels " + imgIconLevels.Count, this, true);
        #endregion

        #region Load Data Config
        AvConfigManager.Instance.LoadAll();
        // ------------------------------ itemsShopConfig ------------------------
        var itemsShopConfig = AvConfigManager.S_ItemShopConfig.GetAllItem();
        foreach (var item in itemsShopConfig)
        {
            ItemShopRow itemShop = new ItemShopRow();
            itemShop.ID = item.ID;
            itemShop.NAME = item.NAME;
            itemShop.PIRCE = item.PIRCE;
            itemShop.ADS = item.ADS;
            if (itemsShopData.ContainsKey(itemShop.ID) == false)
            {
                itemsShopData.Add(itemShop.ID, itemShop);
            }
            else
            {
                //Debug.Log("itemsShopData ID " + item.ID + "Error", this, true);
            }
        }
        //---------------------------- userData ------------------------------------
        //var userConfig = AvConfigManager.S_UserConfig.GetAllItem();
        //UserDataConfig userDataConfig = userConfig[0];

        //userData.Id = userDataConfig.ID;
        //userData.IdSkinSelected = userDataConfig.ID_SKIN_SELECTED;
        //userData.StarNumber = userDataConfig.STAR_NUMBER;
        //userData.HintSkillNumber = userDataConfig.HINT_SKILL_NUMBER;
        //userData.ShadowBallSkillNumber = userDataConfig.SHADOW_BALL_SKILL_NUMBER;
        //userData.SkipTurnSkillNumber = userDataConfig.SKIP_SKILL_NUMBER;

        //string[] cacheIdLevelUnlock = userDataConfig.ID_LEVEL_UNLOCKED.Split(',');
        //userData.IdLevelsUnloced = new List<int>();
        //for (int index = 0; index < cacheIdLevelUnlock.Length; index++)
        //{
        //    userData.IdLevelsUnloced.Add(int.Parse(cacheIdLevelUnlock[index]));
        //}

        //string[] cacheIdLevelComplete = userDataConfig.ID_LEVEL_COMPLETE.Split(',');
        //userData.IdLevelsComplete = new List<int>();
        //for (int index = 0; index < cacheIdLevelComplete.Length; index++)
        //{
        //    userData.IdLevelsComplete.Add(int.Parse(cacheIdLevelComplete[index]));
        //}

        //string[] cacheIdSkin = userDataConfig.ID_SKINS.Split(',');
        //userData.IdSkins = new List<int>();
        //for (int index = 0; index < cacheIdSkin.Length; index++)
        //{
        //    userData.IdSkins.Add(int.Parse(cacheIdSkin[index]));
        //}
        //---------------------------- List Music ------------------------------------
        var musicConfig = AvConfigManager.S_ListMusicConfig.GetAllItem();
        foreach (var item in musicConfig)
        {
            if (!musicsData.ContainsKey(item.ID))
            {
                var cacheMusic = Resources.LoadAll("Sounds/MusicSounds/" + item.NAME, typeof(AudioClip));
                MusicData msdata = new MusicData();
                msdata.Id = item.ID;
                msdata.Name = item.NAME;
                msdata.Audios = new List<AudioClip>();
                for (int i = 0; i < cacheMusic.Length; i++)
                {
                    msdata.Audios.Add(cacheMusic[i] as AudioClip);
                }

                musicsData.Add(item.ID, msdata);
            }
            else
            {
                //Debug.Log("musicsData ID " + item.ID + "Error", this, true);
            }
        }
        //---------------------------- musicLevelsData ------------------------------
        var musicLevelsConfig = AvConfigManager.S_MusicLevelsConfig.GetAllItem();
        foreach (var item in musicLevelsConfig)
        {
            MusicLevelsData musicLevelDataCache = new MusicLevelsData();
            musicLevelDataCache.Id = item.ID_LEVEL;
            musicLevelDataCache.IdMusic = item.ID_MUSIC;
            musicLevelDataCache.ConverToTurns(item.TURNS);
            musicLevelsData.Add(musicLevelDataCache.Id, musicLevelDataCache);
        }
        //---------------------------- hintSkillConfig ------------------------------
        var hintSkillConfig = AvConfigManager.S_HintSkillConfig.GetAllItem();
        foreach (var item in hintSkillConfig)
        {
            HintSkillData hintSkill = new HintSkillData();
            hintSkill.Id = item.ID_LEVEL;
            hintSkill.ConverToTurns(item.TURNS);
            hintsSkillData.Add(hintSkill.Id, hintSkill);
        }
        //---------------------------- Localization ---------------------------------
        LocalizationsDataManager.Instance.Init();
        //---------------------------- Load Mat -------------------------------------
        var materialsData = Resources.LoadAll("Materials", typeof(Material));
        foreach (Material item in materialsData)
        {
            if (!materials.ContainsKey(item.name))
            {
                materials.Add(item.name, item);
            }
            else
            {
                //Debug.Log("materials ID " + item.name + "Error", this, true);
            }
        }
        //Debug.Log("Load done: materials " + materials.Count, this, true);
        #endregion
    }
}
