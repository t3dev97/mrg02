﻿using UnityEngine;
using UnityEngine.UI;

public class ItemLevelToChoose : MonoBehaviour
{
    [SerializeField]
    private int idLevel;
    [SerializeField]
    private Text txtLevelName;
    public int IdLevel { get => idLevel; set => idLevel = value; }
    public Text TxtLevelName { get => txtLevelName; set => txtLevelName = value; }

    public void OnClickThis()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Select_Level);
        GameController.Instance.CurrentIDLevel = idLevel;
        GameController.Instance.GotoBattleGame();
    }
}
