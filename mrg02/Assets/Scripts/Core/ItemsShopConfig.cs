﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using FileHelpers;
using System.IO;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class ItemShopRow
{
  	public int ID;
	public string NAME;
    public int PIRCE;
    public bool ADS;
}
public class ItemShopConfig : GConfigDataTable<ItemShopRow>
{
    public ItemShopConfig() : base("ItemShopConfig")
    {

    }
    public ItemShopConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID");
    }

    public ItemShopRow GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID", ID);
    }
    public List<ItemShopRow> GetAllItem()
    {
        return records;
    }

}
