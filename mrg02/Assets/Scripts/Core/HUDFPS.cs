﻿using UnityEngine;
using UnityEngine.UI;

public class HUDFPS : MonoBehaviour
{
    public static HUDFPS Instance;
    public float updateInterval = 0.5F;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval

    private Text lbFPS;
    public float FPS = 0;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }
    void Start()
    {
        lbFPS = GetComponent<Text>();
        if (lbFPS == null)
        {
            Debug.Log("UtilityFramesPerSecond needs a UILabel component!");
            enabled = false;
            return;
        }
        timeleft = updateInterval;

    }

    void Update()
    {

        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        if (timeleft <= 0.0)
        {
            float fps = accum / frames;
            FPS = fps;
            string format = System.String.Format("{0:F2} FPS", fps);

            lbFPS.text = format;

            if (fps < 30)
            {
                lbFPS.color = Color.yellow;
            }

            else
                if (fps < 10)
            {
                lbFPS.color = Color.red;
            }
            else
            {
                lbFPS.color = Color.green;
            }

            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
        }
    }
}