﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
public static class CSVDowloader
{
    private const string k_googleSheetDocID = "1Ml3TnX39yw9fhjopd_KfX8eaxBWs0Sd34UK_dmd09ao";
    private const string url = "https://docs.google.com/spreadsheets/d/" + k_googleSheetDocID + "/export?format=csv";
    internal static IEnumerator DownloadData(System.Action<string> onComplete)
    {
        yield return new WaitForEndOfFrame();
        string downloadData = null;
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError)
            {
                Debug.Log("Dowload Error: " + webRequest.error);
                downloadData = PlayerPrefs.GetString("LastDataDownloaded", null);
                string versionText = PlayerPrefs.GetString("LastDataDownloaded", null);
                Debug.Log("Using stale data version: " + versionText);
            }
            else
            {
                Debug.Log("Download success");
                Debug.Log("Data: "+ webRequest.downloadHandler.text);
            }
        }

        onComplete(downloadData);
    }
}
