﻿using UnityEngine;

public class FXCelebrationController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem[] particles;
    public void PlayFX()
    {
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].enableEmission = true;
        }
    }
    public void StopFX()
    {
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].gameObject.SetActive(false);
            particles[i].enableEmission = false;
        }
    }
}
