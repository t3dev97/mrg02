﻿using BansheeGz.BGSpline.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LevelController : MonoBehaviour
{
    [SerializeField]
    private bool isLevelHint;
    private BallBase prefabBall;
    [SerializeField]
    private Transform trsPointGroup;
    [SerializeField]
    private Transform trsLineGroup;
    [SerializeField]
    private Transform trsMoveLine;
    [Header("Setting Level")]
    [SerializeField]
    private int idWorld;
    [SerializeField]
    private int id;
    [SerializeField]
    private int musicID;
    [SerializeField]
    private bool isAllCircle;
    [SerializeField]
    private bool isAllEllipse;
    [SerializeField]
    private bool isAllSquareNormal;
    [SerializeField]
    private bool isAllTriangleNormal;
    [SerializeField]
    private bool isAllRhombus;
    [SerializeField]
    private bool isAllLines;
    [SerializeField]
    private bool isCustom;
    [Tooltip("Thời gian di chuyển hết 1 vòng")]
    [SerializeField]
    private float moveTime;

    [Tooltip("Số nút/đoạn của Line (chỉ sử dụng cho kiểu di chuyển Worm)")]
    [SerializeField]
    private int numberStep;

    [Tooltip("Thời gian điếm ngược để win game khi đã ra đủ số Ball")]
    [SerializeField]
    private float timeWin;

    [Tooltip("Thứ tự ra Line của mỗi lần Tap")]
    [SerializeField]
    private TurnInfo[] turns;

    [Tooltip("Danh sách các Line có Ball chạy tự động(Các Ball enemy)")]
    [SerializeField]
    private int[] lineBallsAuto;
    [SerializeField]
    private int[] lineBallsAutoForHint;

    [Tooltip("Danh sách các Line có khả năng tự XOAY")]
    [SerializeField]
    private LineRotate[] linesRotate;
    private Transform[] points;
    private Transform[] lines;
    [SerializeField]
    private List<BallController> ballsActive;
    private List<BallController> ballsActiveDraft;
    private List<BallController> ballsActiveHint;
    private List<BallController> autoBallsActive;
    private List<BGCcCursorObjectTranslate> bGCcsActive = new List<BGCcCursorObjectTranslate>();
    private int indexBallActive = 0;
    private int nextTurnIndex = 0;
    private int currentTurn = 0;
    private bool hasCaptainBall = false;
    private bool isFullBall = false;
    private bool isCountingDownToWin = false;
    private Transform trsBG;
    private bool shadowSkillOn;
    private bool hintSkillOn = false;

    [SerializeField]
    private List<LineHightLight> lineHightLights = new List<LineHightLight>();
    public int NumberStep { get => numberStep; set => numberStep = value; }
    public float MoveTime { get => moveTime; set => moveTime = value; }
    public int Id { get => id; set => id = value; }
    public TurnInfo[] Turns { get => turns; set => turns = value; }
    public int IdWorld { get => idWorld; set => idWorld = value; }
    public bool IsFullBall { get => isFullBall; set => isFullBall = value; }
    public bool IsCountingDownToWin { get => isCountingDownToWin; set => isCountingDownToWin = value; }
    public int CurrentTurn { get => currentTurn; set => currentTurn = value; }
    public int NextTurnIndex { get => nextTurnIndex; set => nextTurnIndex = value; }
    public bool HintSkillOn { get => hintSkillOn; set => hintSkillOn = value; }
    public bool ShadowSkillOn { get => shadowSkillOn; set => shadowSkillOn = value; }
    public bool IsLevelHint { get => isLevelHint; set => isLevelHint = value; }

    public void OnCustomStart(BallBase ball)
    {
        shadowSkillOn = false;
        hintSkillOn = false;
        trsBG = transform.GetChild(1);
        if (isLevelHint)
        {
            trsBG.gameObject.SetActive(false);
            transform.localPosition = new Vector3(0, 0, ConfigVariables.DISTANCE_HINT_LEVEl_Z);
        }
        else
        {
            trsBG.localScale = new Vector3(1, 1, 1);
        }
        prefabBall = ball;
        points = new Transform[trsPointGroup.childCount];
        lines = new Transform[trsLineGroup.childCount];
        ballsActive = new List<BallController>();
        ballsActiveDraft = new List<BallController>();
        ballsActiveHint = new List<BallController>();
        autoBallsActive = new List<BallController>();

        #region points
        for (int index = 0; index < points.Length; index++)
        {
            points[index] = trsPointGroup.GetChild(index);
            points[index].gameObject.SetActive(false);
        }
        #endregion
        GameObject trsContentShowLine = new GameObject("trsContentShowLine");
        trsContentShowLine.transform.parent = transform;
        trsContentShowLine.transform.localPosition = new Vector3(0, 0, -2);
        #region lines
        if (isAllCircle || isAllEllipse)
            InitAllLine(ELineType.CIRCLE, trsContentShowLine.transform);
        else if (isAllSquareNormal)
            InitAllLine(ELineType.SQUARE, trsContentShowLine.transform);
        else if (isAllTriangleNormal)
            InitAllLine(ELineType.TRIANGLE, trsContentShowLine.transform);
        else if (isAllRhombus)
            InitAllLine(ELineType.RHOMBUS, trsContentShowLine.transform);
        else if (isAllLines)
            InitAllLine(ELineType.LINE, trsContentShowLine.transform);
        else
            InitAllLine(ELineType.CUSTOM, trsContentShowLine.transform);
        #endregion

        SpawnBallsAuto(StartBallsAuto);
        ShowPoints(nextTurnIndex);
        AddLinesRotate();

        //switch (GameController.Instance.CurrentIDLevel)
        //{
        //    case 35:
        //        Camera.main.orthographicSize = 9.0f;
        //        break;
        //    case 40:
        //    case 45:
        //    case 55:
        //    case 58:
        //    case 63:
        //    case 65:
        //        Camera.main.orthographicSize = 7.0f;
        //        break;
        //    default:
        //        break;
        //}

        //StartCoroutine(ActiveSkillHint());
    }
    private void InitAllLine(ELineType eLineType, Transform trsContentShowLine)
    {
        for (int index = 0; index < lines.Length; index++)
        {
            lines[index] = trsLineGroup.GetChild(index);
            Vector3 vec3 = lines[index].position;
            vec3.z = 0;
            lines[index].transform.position = vec3;
            if (lines[index].GetComponent<BGCcCursorObjectTranslate>().Cursor.DistanceRatio == 1)
            {
                lines[index].GetComponent<BGCcCursorObjectTranslate>().Cursor.DistanceRatio = 0;
            }
            LineHightLight lineHight;

            if (eLineType != ELineType.CUSTOM)
            {
                lineHight = lines[index].gameObject.AddComponent<LineHightLight>();
                lineHight.LineType = eLineType;
            }
            else
            {
                lineHight = lines[index].gameObject.GetComponent<LineHightLight>();
            }
            lineHight.OnCustomStart();
            lineHight.OffHighlight();

            LineHightLight lineHight_show = Instantiate(lineHight, trsContentShowLine);
            lineHight_show.OnCustomStart();
            lineHight_show.OnHighlight();
            lineHight_show.gameObject.SetActive(false);
            lineHightLights.Add(lineHight_show);
        }
    }
    private int indexDraftCurrentTurn;
    public void TouchInScreenToSpawnNextBall(bool isDraft = false)
    {
        if (shadowSkillOn == false)
        {
            if (GameController.Instance.CurrentGameStage == EGameStage.PLAYING && currentTurn < turns.Length && !isLevelHint)
            {
                SoundController.Instance.PlayEffect(EIDSoundEffect.Release_Beat);
                if (isDraft)
                    indexDraftCurrentTurn = currentTurn;
                if (currentTurn == 0)
                {
                    PlayMuteAllBeat();
                }

                if (shadowSkillOn)
                {
                    BallDraftToLive();
                }

                if (hintSkillOn)
                {
                    ClearAllBallDraft();
                }
                StartCurrentTurn(isDraft);
            }
        }
    }
    public void SkipTurn()
    {
        if (GameController.Instance.CurrentGameStage == EGameStage.PLAYING && currentTurn < turns.Length)
        {
            if (currentTurn == 0)
            {
                PlayMuteAllBeat();
            }
            StartSkipTurn();
            IncreaseCurrentTurn();
            if (isFullBall && isCountingDownToWin == false)
            {
                ShowAndCountDownToWin();
            }
        }
    }
    public void PlayMuteAllBeat()
    {
        int musicID = MCache.Instance.MusicLevelsData[id].IdMusic;
        for (int i = 0; i < MCache.Instance.MusicsData[musicID].Audios.Count; i++)
        {
            SoundController.Instance.PlayMuteBeat(musicID, i + 1);
        }

        //for (int i = 0; i < MCache.Instance.MusicsData[musicID].Audios.Count; i++)
        //{
        //    SoundController.Instance.PlayMuteBeat(musicID, i + 1);
        //}
    }
    public void IncreaseCurrentTurn()
    {
        ++currentTurn;
        if (currentTurn == turns.Length && ballsActiveDraft.Count <= 0)
        {
            // count down to win           
            FullBall();
            Debug.Log("IncreaseCurrentTurn");
        }
    }
    private void FullBall()
    {
        isFullBall = true;
        GameController.Instance.UnInteractableToolTip();
    }
    public void IncreaseAndShowNextPoint()
    {
        if (nextTurnIndex < turns.Length - 1)
        {
            ++nextTurnIndex;
            ShowPoints(nextTurnIndex);
        }
    }
    private void PlayOnBeat()
    {
        List<int> beats = MCache.Instance.MusicLevelsData[id].Turns[currentTurn];
        for (int i = 0; i < beats.Count; i++)
        {
            SoundController.Instance.PlayOnBeat(beats[i]);
        }

        //for (int i = 0; i < turns[currentTurn].BeatsID.Length; i++)
        //{
        //    //print(turns[currentTurn].BeatsID[i]);
        //    SoundController.Instance.PlayOnBeat(turns[currentTurn].BeatsID[i]);
        //}
    }
    private void StartSkipTurn()
    {
        PlayOnBeat();
        GameController.Instance.UpdateBoardNextBall(currentTurn);
        HideAllPointSpawnBall(true);
        IncreaseAndShowNextPoint();
    }
    private void StartCurrentTurn(bool isDraft = false)
    {
        if (isDraft == false)
        {
            PlayOnBeat();
            GameController.Instance.UpdateBoardNextBall(currentTurn);
        }

        #region Spawn Balls
        BallBase[] balls = new BallBase[turns[nextTurnIndex].LineNumber.Length];
        int indexBall = 0;
        SpawnLinesInTurn(nextTurnIndex, (bGCcTranslate) =>
        {
            balls[indexBall] = SpawnBall(bGCcTranslate);
            (balls[indexBall] as BallController).IsBallLast = (currentTurn == turns.Length - 1) ? true : false;
            if (isDraft)
            {
                balls[indexBall].tag = ETagName.PlayerDraft.ToString();
            }

            ++indexBall;
        });
        #endregion

        #region StartAllBallMove
        for (int index = 0; index < indexBall; index++)
        {
            balls[index].OnCustomStart();
            (balls[index] as BallController).IsCaptainBall_V2 = !hasCaptainBall;
            balls[index].MoveBall();

            if (balls[index].CompareTag(ETagName.Player.ToString()))
            {

                ballsActive.Add(balls[index] as BallController);
            }
            else
            {
                balls[index].GetComponentInChildren<SpriteRenderer>().color = Color.gray;
                ballsActiveDraft.Add(balls[index] as BallController);
            }
        }
        #endregion

        HideAllPointSpawnBall();
        IncreaseAndShowNextPoint();
        IncreaseCurrentTurn();
        hasCaptainBall = true;
    }
    private int currentTurnTemp = -1;
    public void ClearAllBallDraft()
    {
        shadowSkillOn = false;
        hintSkillOn = false;
        Debug.Log("currentTurn => " + currentTurn);
        Debug.Log("nextTurnIndex => " + nextTurnIndex);
        HideAllPointSpawnBall();
        currentTurn = indexDraftCurrentTurn;
        nextTurnIndex = indexDraftCurrentTurn;
        ShowPoints(nextTurnIndex);
        foreach (var item in ballsActiveDraft)
        {
            Destroy(item.gameObject);
        }
        ballsActiveDraft.Clear();
        GameController.Instance.ShowBtnShadown();
    }
    public void BallDraftToLive()
    {
        if (ballsActiveDraft.Count > 0)
        {
            currentTurn--;
            PlayOnBeat();
            currentTurn++;
            GameController.Instance.UpdateBoardNextBall(currentTurn - 1);
            foreach (var item in ballsActiveDraft)
            {
                item.gameObject.tag = ETagName.Player.ToString();
                item.GetComponentInChildren<SpriteRenderer>().color = Color.white;
                item.BallLastEndGame();
                ballsActive.Add(item);
            }
            ballsActiveDraft.Clear();
            GameController.Instance.ShowBtnShadown();

            if (currentTurn == turns.Length)
            {
                FullBall();
            }
        }
    }
    private void StarHintTurn(int nextTurnIndex)
    {
        BallBase[] balls = new BallBase[turns[nextTurnIndex].LineNumber.Length];
        int indexBall = 0;
        SpawnLinesInTurn(nextTurnIndex, (bGCcTranslate) =>
        {
            balls[indexBall] = SpawnBall(bGCcTranslate);

            balls[indexBall].tag = ETagName.PlayerDraft.ToString();

            ++indexBall;
        });

        #region StartAllBallMove
        for (int index = 0; index < indexBall; index++)
        {
            balls[index].OnCustomStart();
            (balls[index] as BallController).IsCaptainBall_V2 = !hasCaptainBall;
            balls[index].MoveBall();
            balls[index].GetComponentInChildren<SpriteRenderer>().color = Color.gray;
            ballsActiveDraft.Add(balls[index] as BallController);
        }
        #endregion   
    }
    private int turnHint = 0;
    public IEnumerator ActiveSkillHint()
    {
        StarHintTurn(turnHint);
        Debug.Log("ActiveSkillHint ==> " + MCache.Instance.HintsSkillData[id].Turns[turnHint][0]);
        yield return new WaitForSeconds(MCache.Instance.HintsSkillData[id].Turns[turnHint][0]);
        if (turnHint < turns.Length)
        {
            ++turnHint;
            StartCoroutine(ActiveSkillHint());
        }
    }
    private void AddLinesRotate()
    {
        for (int index = 0; index < linesRotate.Length; index++)
        {
            LineController lineController = lines[linesRotate[index].LineNumber - 1].gameObject.AddComponent<LineController>();
            lineController.LineRotateData = linesRotate[index];
            points[linesRotate[index].LineNumber - 1].parent = lineController.transform;
        }
    }
    public void StartWormsMove()
    {
        for (int i = 0; i < ballsActive.Count; i++)
        {
            ballsActive[i].StartWormMove();
        }
    }
    public void StopWormsMoving()
    {
        for (int i = 0; i < ballsActive.Count; i++)
        {
            ballsActive[i].StopWormMoving();
        }
    }
    #region Balls 
    private void SpawnBallsAuto(Action<BallBase[]> onStartBalls)
    {
        BallBase[] ballsAuto = new BallBase[lineBallsAuto.Length];
        int indexBall = 0;
        for (int index = 0; index < lineBallsAuto.Length; index++)
        {
            SpawnLine(lineBallsAuto[index], (bGCcTranslate) =>
            {
                ballsAuto[indexBall] = SpawnBall(bGCcTranslate, true);

                autoBallsActive.Add(ballsAuto[indexBall] as BallController);
                //ballsAuto[indexBall].transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.red;                
                ballsAuto[indexBall].OnCustomStart();
                ballsAuto[indexBall].gameObject.layer = (int)ELayer.Obstacle;
                if (isLevelHint)
                {
                    ballsAuto[indexBall].tag = ETagName.PlayerDraft.ToString();
                    bool checkBallLive()
                    {
                        for (int idTurn = 0; idTurn < lineBallsAutoForHint.Length; idTurn++)
                        {
                            if (lineBallsAuto[index] == lineBallsAutoForHint[idTurn])
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    if (checkBallLive())
                    {
                        ballsAuto[indexBall].transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = MCache.Instance.PlayerBalls[GameController.Instance.CurrentIdBall].GetComponentInChildren<SpriteRenderer>().sprite;
                    }
                    else
                    {
                        Debug.Log("checkBallLive" + lineBallsAuto[index]);
                    }
                    //ballsAuto[indexBall].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(100, 100, 100, 100);
                }
                ++indexBall;
            });
        }
        onStartBalls(ballsAuto);
    }
    private void StartBallsAuto(BallBase[] ballsAuto)
    {
        for (int index = 0; index < ballsAuto.Length; index++)
        {
            ballsAuto[index].MoveBall();
        }
    }
    private BallBase SpawnBall(BGCcCursorObjectTranslate bGCcTranslate, bool isAutoBeat = false)
    {
        BallBase ball;
        if (isAutoBeat)
        {
            ball = Instantiate(MCache.Instance.PlayerBalls[10], transform);
        }
        else
        {
            ball = Instantiate(MCache.Instance.PlayerBalls[GameController.Instance.CurrentIdBall], transform);
        }
        ball.transform.position = new Vector3(UnityEngine.Random.Range(100, 200), 0, 0);
        bGCcTranslate.ObjectToManipulate = ball.transform;
        bGCcsActive.Add(bGCcTranslate);
        ball.MyLevelController = this;
        ball.BGTranslate = bGCcTranslate;
        ball.name = bGCcTranslate.name;
        ball.gameObject.SetActive(true);
        return ball;
    }
    #endregion

    #region Lines
    private void SpawnLinesInTurn(int nextTurnIndex, Action<BGCcCursorObjectTranslate> onSpawnBall = null)
    {
        for (int index = 0; index < turns[nextTurnIndex].LineNumber.Length; index++)
        {
            int indexLine = turns[nextTurnIndex].LineNumber[index] - 1;
            Transform line = Instantiate(lines[indexLine], trsMoveLine);
            line.GetComponent<LineRenderer>().enabled = false;
            BGCcCursorObjectTranslate bGCcTranslate = line.GetComponent<BGCcCursorObjectTranslate>();

            onSpawnBall(bGCcTranslate);
        }
    }
    private void SpawnLine(int indexLine, Action<BGCcCursorObjectTranslate> onSpawnBall = null)
    {
        Transform line = Instantiate(lines[indexLine - 1], trsMoveLine);
        line.GetComponent<LineRenderer>().enabled = false;
        line.gameObject.AddComponent<LineController>();
        BGCcCursorObjectTranslate bGCcTranslate = line.GetComponent<BGCcCursorObjectTranslate>();
        //print(bGCcTranslate.gameObject.name);
        onSpawnBall(bGCcTranslate);
    }
    #endregion

    #region Points    
    public void HideAllPointSpawnBall(bool isAutoHide = false)
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].gameObject.SetActive(false);
            lineHightLights[i].gameObject.SetActive(false);
        }
    }
    public void ShowPoints(int nextTurnIndex)
    {
        if (turns.Length > 0)
        {
            for (int i = 0; i < turns[nextTurnIndex].LineNumber.Length; i++)
            {
                ShowPoint(turns[nextTurnIndex].LineNumber[i]);
                lineHightLights[turns[nextTurnIndex].LineNumber[i] - 1].gameObject.SetActive(true);
            }
        }
    }
    private void ShowPoint(int index)
    {
        points[index - 1].gameObject.SetActive(true);
    }
    #endregion

    #region END GAME
    public void ShowAndCountDownToWin()
    {
        if (GameController.Instance.CurrentGameStage == EGameStage.END_GAME) return;
        isCountingDownToWin = true;
        GameController.Instance.ShowTimeCounter();
        GameController.Instance.HideToolTip();
        GameController.Instance.ShowUIToCountDown();
        StartCoroutine(CountDownToWin());
    }
    IEnumerator CountDownToWin()
    {
        GameController.Instance.UpdateTimeCounter(timeWin);
        yield return new WaitForSeconds(1);
        timeWin -= 1;
        if (timeWin > 0 && GameController.Instance.CurrentGameStage != EGameStage.END_GAME)
        {
            StartCoroutine(CountDownToWin());
        }
        else
        {
            if (timeWin > 0)
            {
                GameController.Instance.ShowEndGame(EEndGameStage.LOSE);
            }
            else
            {
                GameController.Instance.ShowEndGame(EEndGameStage.WIN);
                for (int i = 0; i < lineHightLights.Count; i++)
                {
                    lineHightLights[i].OnHighlightNoMask();
                }
            }
        }
    }
    public void ProcessGameLose()
    {
        for (int index = 0; index < bGCcsActive.Count; index++)
        {
            bGCcsActive[index].ObjectToManipulate = null;
        }
        for (int index = 0; index < ballsActive.Count; index++)
        {
            ballsActive[index].OnDrop();
        }
        for (int index = 0; index < autoBallsActive.Count; index++)
        {
            autoBallsActive[index].OnDrop();
        }
    }
    public void ProcessGameWin()
    {
        for (int index = 0; index < ballsActive.Count; index++)
        {
            ballsActive[index].UptoFX();
        }
    }
    public void HideActiveBall()
    {
        foreach (var item in ballsActive)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in autoBallsActive)
        {
            item.gameObject.SetActive(false);
        }
    }
    public void ShowActiveBall()
    {
        foreach (var item in ballsActive)
        {
            item.gameObject.SetActive(true);
        }
        foreach (var item in autoBallsActive)
        {
            item.gameObject.SetActive(true);
        }
    }
    #endregion
    [ContextMenu("Add Line Hight Light")]
    public void AddLineHightLight()
    {
        isCustom = true;
        isAllCircle = false;
        isAllEllipse = false;
        isAllRhombus = false;
        isAllSquareNormal = false;
        isAllTriangleNormal = false;
        foreach (Transform item in trsLineGroup)
        {
            item.gameObject.AddComponent<LineHightLight>();
        }
    }
}

[Serializable]
public class TurnInfo
{
    [Tooltip("ID Beat sẻ bật của Turn này")]
    [SerializeField]
    private int[] beatsID;
    [Tooltip("Danh sách các Line sẻ xuất hiện trong lần Tap này, 1 Tap có thể nhiều Line xuất hiện đồng thời.")]
    [SerializeField]
    private int[] lineNumber;
    public int[] LineNumber { get => lineNumber; set => lineNumber = value; }
    public int[] BeatsID { get => beatsID; set => beatsID = value; }
}
[Serializable]
public class LineRotate
{
    [Tooltip("Line số ? sẻ XOAY (Line đâu tiên là 1)")]
    [SerializeField]
    private int lineNumber;
    [Tooltip("Tốc độ XOAY của Line (rotateSpeed > 0: XOAY PHẢI; rotateSpeed < 0: XOAY TRÁI; rotateSpeed == 0: KHÔNG XOAY (ĐỪNG ĐƯA VÀO LIST NÀY))")]
    [SerializeField]
    private float rotateTime = 4;
    [Tooltip("Hướng Xoay")]
    [SerializeField]
    private bool isRight = true;

    public int LineNumber { get => lineNumber; set => lineNumber = value; }
    public float RotateTime { get => rotateTime; set => rotateTime = value; }
    public bool IsRight { get => isRight; set => isRight = value; }
}
