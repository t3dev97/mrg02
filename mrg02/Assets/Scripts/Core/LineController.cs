﻿using DG.Tweening;
using UnityEngine;
public class LineController : MonoBehaviour
{
    [SerializeField]
    private LineRotate lineRotateData;

    public LineRotate LineRotateData { get => lineRotateData; set => lineRotateData = value; }
    private void Start()
    {
        if (lineRotateData != null)
            StartRotate();
    }
    public void StartRotate()
    {
        transform.DORotate(new Vector3(0, 0, lineRotateData.IsRight ? -360 : 360), lineRotateData.RotateTime, RotateMode.LocalAxisAdd).SetLoops(-1).SetEase(Ease.Linear);
    }
}
