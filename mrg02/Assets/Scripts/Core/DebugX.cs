﻿using UnityEngine;

public static class DebugX
{
    public static void Log(string mes, MonoBehaviour monoBehaviour, bool warring = false)
    {
        if (ConfigVariables.OnDebug)
        {
            if (!warring)
            {
                Debug.Log(mes + " //" + monoBehaviour.name);
            }
            else
            {
                Debug.LogWarning(mes + " //" + monoBehaviour.name);
            }
        }
    }
    public static void Log(string mes, bool warring = false)
    {
        if (ConfigVariables.OnDebug)
        {
            if (!warring)
            {
                Debug.Log(mes);
            }
            else
            {
                Debug.LogWarning(mes);
            }
        }
    }
}
