﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class HintSkillRow
{
    public int ID_LEVEL;    
    public string TURNS;
    public string TURN_TEMP;
}
public class HintSkillConfig : GConfigDataTable<HintSkillRow>
{
    public HintSkillConfig() : base("ItemShopConfig")
    {

    }
    public HintSkillConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID_LEVEL");
    }

    public HintSkillRow GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID_LEVEL", ID);
    }
    public List<HintSkillRow> GetAllItem()
    {
        return records;
    }
}