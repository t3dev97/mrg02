﻿using BansheeGz.BGSpline.Components;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BallBase : MonoBehaviour
{
    [SerializeField]
    protected int id;
    protected LevelController levelController;
    protected float dis;
    protected bool lineClosed = true;
    protected bool allowColision = true;
    protected BGCcCursorObjectTranslate bGTranslate;
    protected Tween twMainMove;
    public BGCcCursorObjectTranslate BGTranslate { get => bGTranslate; set => bGTranslate = value; }
    public LevelController MyLevelController { get => levelController; set => levelController = value; }
    public bool AllowColision { get => allowColision; set => allowColision = value; }
    public int Id { get => id; set => id = value; }
    public abstract void OnCustomStart();
    public abstract void MoveBall();
    private void OnDestroy()
    {
        twMainMove.Kill();
    }
}
