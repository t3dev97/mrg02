﻿using UnityEngine;

public class BallColistion : MonoBehaviour
{
    [SerializeField]
    private BallBase controller;
    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (GameController.Instance.CurrentGameStage != EGameStage.PLAYING || controller.MyLevelController.IsLevelHint) return;
        // use skill Shadow Ball 
        //if (controller.LevelController.IsShadowSkill)
        //{
        //    controller.LevelController.IsShadowSkill = false;
        //    Destroy(gameObject);
        //    return;
        //}
        string tagName = obj.tag;
        if (controller.MyLevelController.ShadowSkillOn == false)
        {
            if (gameObject.CompareTag((ETagName.Player.ToString())))
            {
                if (obj.CompareTag(ETagName.Player.ToString()) || obj.CompareTag(ETagName.Obstacle.ToString()))
                {
                    GameController.Instance.ShowEndGame(EEndGameStage.LOSE);                    
                }
            }
        }
        else
        {
            if (gameObject.CompareTag(ETagName.PlayerDraft.ToString()))
            {
                GameController.Instance.ClearAllBallDraft();
            }
        }
    }
}
