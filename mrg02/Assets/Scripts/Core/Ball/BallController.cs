﻿using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using DG.Tweening;
using System.Collections;
using UnityEngine;
public class BallController : BallBase
{
    private EGameMode gameMode;
    private Rigidbody2D rigi;
    private ParticleSystem fxMoving;
    // Of Worm Move v2
    [SerializeField]
    private bool isCaptainBall_V2 = false;
    private BGCurve bGCurve;
    private BGCcMath gCcMath;
    private float[] steps;
    private int nextStep = 0;
    private float moveTime;
    [SerializeField]
    private LineRenderer mainRenderer;
    [SerializeField]
    private LineRenderer mainShowRenderer;
    [SerializeField]
    private LineRenderer headRenderer;
    [SerializeField]
    private TrailRenderer trailRenderer;
    private float[] distanceMainWorm; // max
    private float[] distanceMainWormShow; // normal
    private float[] distanceWorm; // scale
    private float[] disHead;
    private int lenghtMainWorm = 8;
    private int lenghtMainWormShow = 4;
    private int lenghtHeadWorm = 4;
    private float lenghtScaleHeadWorm = .4f;
    [SerializeField]
    private bool isBallLast = false;
    //-------------------------------------------------
    // Of Worm Move V1
    Tween tween_V1;
    //-------------------------------------------------
    [SerializeField]
    private int numberSpace;
    private float disPerMain = 0.1f;
    [SerializeField]
    private AnimationCurve curve;

    public bool IsCaptainBall_V2 { get => isCaptainBall_V2; set => isCaptainBall_V2 = value; }
    public bool IsBallLast { get => isBallLast; set => isBallLast = value; }

    private void Awake()
    {
        fxMoving = GetComponentInChildren<ParticleSystem>();
        HideFXMoving();
        Invoke("ShowFXMoving", 0.2f);
    }
    private void ShowFXMoving()
    {
        if (fxMoving != null)
            fxMoving.enableEmission = true;
    }
    private void HideFXMoving()
    {
        if (fxMoving != null)
            fxMoving.enableEmission = false;
    }
    public void UptoFX()
    {
        if (fxMoving != null)
            fxMoving.startLifetime = 1;
    }
    // End of Worm Move
    public override void OnCustomStart()
    {
        rigi = GetComponent<Rigidbody2D>();
        bGCurve = BGTranslate.gameObject.GetComponent<BGCurve>();
        gCcMath = bGCurve.gameObject.GetComponent<BGCcMath>();
        dis = gCcMath.GetDistance();
        lineClosed = BGTranslate.gameObject.GetComponent<BGCurve>().Closed;
        gameMode = GameController.Instance.GameMode;
        BallLastEndGame();
        switch (gameMode)
        {
            case EGameMode.NORMAL:
                {
                    ShowCharacter();
                }
                break;
            case EGameMode.WORM_V1:
                {
                    HideCharacterWorm();
                    ShowMainWorm();
                    steps = new float[levelController.NumberStep];
                    moveTime = levelController.MoveTime / levelController.NumberStep;
                    float disStep = dis / levelController.NumberStep;
                    float total = 0;
                    bool target = false;
                    for (int i = 0; i < levelController.NumberStep; i++)
                    {
                        total += disStep;
                        steps[i] = total;

                        if (BGTranslate.Cursor.Distance < steps[i] && target == false)
                        {
                            nextStep = i;
                            target = true;
                        }
                    }
                }
                break;
            case EGameMode.WORM_V2:
                {
                    HideCharacterWorm();

                    steps = new float[levelController.NumberStep];
                    moveTime = levelController.MoveTime / levelController.NumberStep;
                    float timeStep = levelController.MoveTime / (float)levelController.NumberStep;
                    float disStep = dis / levelController.NumberStep;
                    float total = 0;
                    for (int i = 0; i < levelController.NumberStep; i++)
                    {
                        total += disStep;
                        steps[i] = total;
                    }
                    distanceMainWormShow = new float[lenghtMainWormShow];
                    distanceMainWorm = new float[lenghtMainWorm];
                    distanceWorm = new float[lenghtMainWorm];
                    disHead = new float[lenghtHeadWorm];
                    headRenderer.positionCount = lenghtHeadWorm;

                    CreateMainWorm();
                }
                break;
        }
    }

    public void BallLastEndGame()
    {
        DOVirtual.DelayedCall(levelController.MoveTime, () =>
        {
            if (isBallLast)
            {
                Debug.Log("BallLastEndGame");
                if (GameController.Instance.CurrentGameStage != EGameStage.END_GAME)
                    ProcessEndLineMoveNormal();
            }
        }).SetId(this);
    }

    public override void MoveBall()
    {
        switch (gameMode)
        {
            case EGameMode.NORMAL:
                {
                    if (lineClosed)
                    {
                        MoveForward();
                    }
                    else
                    {
                        MovePingPong();
                    }
                }
                break;
            case EGameMode.WORM_V1:
                {
                    MoveStep_V1();
                }
                break;
            case EGameMode.WORM_V2:
                {
                    MoveStep_V2();
                }
                break;
        }
    }

    #region WORM MOVE V1
    private void MoveStep_V1()
    {
        CaptainBallStartOtherWormsMove();
        float posTarget = steps[nextStep];
        twMainMove = DOVirtual.Float(BGTranslate.Cursor.Distance, posTarget, moveTime, (x) =>
        {
            BGTranslate.Cursor.Distance = x;
        }).OnComplete(() =>
        {
            CaptainStopOtherWormMove();

            ProcessEndLineMoveStep();

            MoveStep_V1();
        }).SetEase(curve);

        if (isCaptainBall_V2)
        {
            twMainMove.Play();
        }
        else
        {
            twMainMove.Pause();
        }
    }
    #endregion

    #region WORM MOVE V2    
    private bool isCreate = true;
    private int indexPos = 0;
    private bool isDoneScale = false;
    private void CreateMainWorm()
    {
        indexPos = 0;
        float disBall = dis;
        disBall = BGTranslate.Cursor.Distance - (lenghtMainWorm / 2) * disPerMain;
        float disTarget = disBall + (lenghtMainWorm / 2) * disPerMain;
        Tween tweenMain = DOVirtual.Float(disBall, disTarget, moveTime / 6, (x) =>
        {
            if (isCreate)
            {
                mainRenderer.positionCount = indexPos + 1;
                distanceMainWorm[indexPos] = disBall;
                Vector3 vec3Test = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceMainWorm[indexPos]);
                mainRenderer.SetPosition(indexPos, vec3Test);
                disBall += disPerMain;
                indexPos++;
                if (indexPos >= lenghtMainWorm)
                {
                    isCreate = false;
                }
            }
        }).OnComplete(() =>
        {
            CreateMainWormShow();
        });
        tweenMain.Play();
    }
    private void CreateMainWormShow(int numberSpace = 2)
    {
        mainShowRenderer.positionCount = lenghtMainWormShow;
        Vector3[] vecs = new Vector3[lenghtMainWormShow];
        for (int i = 0; i < lenghtMainWormShow; i++)
        {
            distanceMainWormShow[i] = distanceMainWorm[i + numberSpace];
            vecs[i] = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceMainWormShow[i]);
        }
        mainShowRenderer.SetPositions(vecs);
    }
    private void SCaleHeadWorm(int numberScale, bool isFirst)
    {
        mainShowRenderer.positionCount = lenghtMainWormShow;
        Vector3[] vecs = new Vector3[lenghtMainWormShow];
        for (int i = 0; i < lenghtMainWormShow; i++)
        {
            distanceMainWormShow[i] = distanceMainWorm[i + numberSpace];
            vecs[i] = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceMainWormShow[i]);
        }
        mainShowRenderer.SetPositions(vecs);
    }

    private void MoveStep_V2()
    {
        #region Trail Renderer
        trailRenderer.time = disPerMain;
        trailRenderer.enabled = false;
        #endregion

        CaptainBallStartOtherWormsMove();

        if (this.isActiveAndEnabled)
        {
            //StartCoroutine(PushLenghtMain());
        }
        isDoneScale = false;
        DOVirtual.Float(0.0f, lenghtScaleHeadWorm, .8f, (x) =>
        {
            Vector3 vec3 = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceWorm[mainShowRenderer.positionCount - 1] + x);
            mainShowRenderer.SetPosition(mainShowRenderer.positionCount - 1, vec3);
        }).SetLoops(1, LoopType.Yoyo).OnComplete(() =>
         {
             //DOVirtual.Float(0.0f, 0.4f, 0.2f, (x) =>
             //{
             //    Vector3 vec3 = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceWorm[mainShowRenderer.positionCount - 1] - x);
             //    mainShowRenderer.SetPosition(mainShowRenderer.positionCount - 1, vec3);
             //}).OnComplete(() =>
             //{
             //});
             isDoneScale = true;
         });
        DOVirtual.DelayedCall(0.2f, () =>
        {
            Tween twMainWorm = DOVirtual.Float(BGTranslate.Cursor.Distance, steps[nextStep], moveTime, (x) =>
            {
                BGTranslate.Cursor.Distance = x;

                #region draw worm main move
                for (int i = 0; i < mainRenderer.positionCount; i++)
                {
                    float newDis = distanceMainWorm[i] + x;
                    distanceWorm[i] = newDis;
                    mainRenderer.SetPosition(i, gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, newDis));
                }
                #endregion

                #region draw worm main show move
                Vector3[] vecs = new Vector3[mainShowRenderer.positionCount];
                for (int i = 0; i < mainShowRenderer.positionCount; i++)
                {
                    float newDis = distanceMainWormShow[i] + x;
                    distanceWorm[i] = newDis;
                    vecs[i] = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, newDis);
                }
                mainShowRenderer.SetPositions(vecs);
                //if (distanceWorm[mainShowRenderer.positionCount - 1] == distanceWorm[mainShowRenderer.positionCount - 1] + lenghtScaleHeadWorm)
                //{
                //    float newDis = distanceMainWormShow[mainShowRenderer.positionCount - 1] + x;
                //    distanceWorm[mainShowRenderer.positionCount - 1] = newDis;
                //    Vector3 vec3 = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, newDis);
                //    mainShowRenderer.SetPosition(mainShowRenderer.positionCount - 1, vec3);
                //}
                #endregion

            }).OnComplete(() =>
            {
                CaptainStopOtherWormMove();

                ProcessEndLineMoveStep();

                MoveStep_V2();
            }).SetEase(curve);

            if (isCaptainBall_V2)
            {
                twMainWorm.Play();
            }
        }).SetId(this);

    }
    private int posHeadWorm = 1;
    private int posTargetHeadWorm = 3;
    IEnumerator PushLenghtMain()
    {
        // Draw head worm;
        Vector3 vec = gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, distanceWorm[lenghtMainWormShow + posHeadWorm]);
        mainShowRenderer.positionCount = lenghtMainWormShow + posHeadWorm;
        mainShowRenderer.SetPosition(mainShowRenderer.positionCount - 1, vec);
        print("PushLenghtMain");
        yield return new WaitForSeconds(Time.deltaTime);
        ++posHeadWorm;
        yield return null;
        if (posHeadWorm < posTargetHeadWorm)
        {
            StartCoroutine(PushLenghtMain());
        }
        else
        {
            posHeadWorm = 0;
            mainShowRenderer.positionCount = lenghtMainWormShow;
        }
    }

    private void CaptainBallStartOtherWormsMove()
    {
        if (isCaptainBall_V2)
        {
            levelController.StartWormsMove();
        }
    }
    private void CaptainStopOtherWormMove()
    {
        if (isCaptainBall_V2)
        {
            levelController.StopWormsMoving();
        }
    }
    private void ProcessEndLineMoveStep()
    {
        ++nextStep;
        if (nextStep >= steps.Length)
        {
            nextStep = 0;
            BGTranslate.Cursor.Distance = 0;

            ProcessEndLineMoveNormal();
        }
    }

    private void ProcessEndLineMoveNormal()
    {
        if (levelController.IsFullBall && levelController.IsCountingDownToWin == false)
        {
            levelController.ShowAndCountDownToWin();
        }
    }
    public void StopWormMoving()
    {
        twMainMove.Pause();
    }
    public void StartWormMove()
    {
        twMainMove.Play();
    }
    private void HideMainWorm()
    {
        transform.GetChild(1).GetComponent<TrailRenderer>().enabled = false;
    }
    private void ShowMainWorm()
    {
        transform.GetChild(1).GetComponent<TrailRenderer>().enabled = true;
    }
    private void HideCharacterWorm()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
    }
    private void ShowCharacter()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
    }
    private void OnDrawGizmos()
    {
        if (gameMode == EGameMode.NORMAL) return;

        for (int i = 0; i < steps.Length; i++)
        {
            Gizmos.DrawSphere(gCcMath.CalcByDistance(BGCurveBaseMath.Field.Position, steps[i]), 0.08f);
        }
    }
    #endregion

    #region NORMAL MOVE
    private float moveTimeMoveForward;
    private bool isDoneMoveForward = false;
    int isfirst = 0;
    float disBegin;
    bool isDone = false;
    private float beginDisMoveForWard;
    private void MoveForward()
    {
        detal = ConfigVariables.DETAL_MOVE;
        if (HUDFPS.Instance.FPS < 40)
            detal *= 2;
        Debug.Log("Time.deltaTime" + Time.deltaTime);
        Debug.Log("Time.smoothDeltaTime" + Time.smoothDeltaTime);
        allowMoveForward = true;
        //dis = 8.79114f;
        return;

        //allowMoveForward = true;
        if (isDoneMoveForward == false)
        {
            disBegin = BGTranslate.Cursor.Distance;
            float ratio = BGTranslate.Cursor.Distance / dis;
            moveTimeMoveForward = levelController.MoveTime - levelController.MoveTime * ratio;
            moveTimeMoveForward = (moveTimeMoveForward <= 0) ? levelController.MoveTime : moveTimeMoveForward;
        }
        else
        {
            moveTimeMoveForward = levelController.MoveTime;
        }
        //float a = 2.5f;
        float a = moveTimeMoveForward / 3;
        Debug.Log(name + " time " + a + " dis " + dis);
        //float b = BGTranslate.Cursor.Distance + 17.93148f;
        //twMainMove = DOTween.To(() => BGTranslate.Cursor.Distance, x => BGTranslate.Cursor.Distance = x, dis, a)
        // .OnComplete(() =>
        //    {
        //        if (isDoneMoveForward == false)
        //            isDoneMoveForward = true;
        //        float newTimer = Time.time;
        //        print(name + " timer =>" + (newTimer - timer) + " ==> " + ((newTimer - timer) - moveTime));
        //        //Debug.Log("BGTranslate.Cursor.Distance " + BGTranslate.Cursor.Distance);
        //        BGTranslate.Cursor.Distance = 0;
        //        ProcessEndLineMoveNormal();
        //        //twMainMove.Play();
        //        //if (isDone == false)
        //        MoveForward();

        //    }).SetEase(Ease.Linear);

    }
    private float moveTimeMovePingPong;
    private bool isDoneMovePingPong = false;
    private float detal;
    private float beginDisMovePingPong = 0;
    private void MovePingPong()
    {
        detal = ConfigVariables.DETAL_MOVE;
        if (HUDFPS.Instance.FPS < 32)
            detal *= 2;
        allowMovePingPong = true;
        beginDisMovePingPong = BGTranslate.Cursor.Distance;
        return;
        //Debug.Log("MovePingPong " + name + " =>>" + BGTranslate.Cursor.Distance);        
        //allowMovePingPong = true;

        if (isDoneMovePingPong == false)
        {
            float ratio = BGTranslate.Cursor.Distance / dis;
            moveTimeMovePingPong = (levelController.MoveTime / 2) - (levelController.MoveTime / 2) * ratio;
            moveTimeMovePingPong = (moveTimeMovePingPong <= 0) ? levelController.MoveTime : moveTimeMovePingPong;
        }
        else
        {
            moveTimeMovePingPong = levelController.MoveTime / 2;
        }
        //print(name + " moveTime => " + moveTime);
        twMainMove = DOVirtual.Float(BGTranslate.Cursor.Distance, dis, moveTimeMovePingPong, (x) =>
         {
             BGTranslate.Cursor.Distance = x;
             Kill();
         }).OnComplete(() =>
         {
             BGTranslate.Cursor.Distance = dis;
             moveTimeMovePingPong = levelController.MoveTime / 2;
             //float timer = Time.time;
             twMainMove = DOVirtual.Float(BGTranslate.Cursor.Distance, 0, moveTimeMovePingPong, (x) =>
               {
                   BGTranslate.Cursor.Distance = x;
                   Kill();
               }).OnComplete(() =>
               {
                   if (isDoneMovePingPong == false)
                   {
                       isDoneMovePingPong = true;
                   }
                   //float newTimer = Time.time;
                   //print(name + " timer =>" + (newTimer - timer) + " ==> " + ((newTimer - timer) - moveTime));
                   BGTranslate.Cursor.Distance = 0;
                   ProcessEndLineMoveNormal();
                   MovePingPong();
               }).SetEase(Ease.Linear);
         }).SetEase(Ease.Linear);
        twMainMove.Play();
    }
    private void Kill()
    {
        if (GameController.Instance.CurrentGameStage == EGameStage.END_GAME && GameController.Instance.EEndGame == EEndGameStage.LOSE)
        {
            twMainMove.Kill();
            return;
        }
    }
    #endregion
    public void OnDrop()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 0.8f, .25f).SetId(this).SetEase(Ease.InBounce);
        transform.DOLocalMoveX(transform.localPosition.x + UnityEngine.Random.Range(-1.5f, 1.5f), .25f)
        .OnComplete(() =>
        {
            rigi.bodyType = RigidbodyType2D.Dynamic;
            rigi.AddForce(Vector3.down * 300);
        }).SetId(this).SetEase(Ease.Linear);
    }
    private void OnDestroy()
    {
        twMainMove.Kill();
        DOTween.Kill(this);
    }
    private bool allowMoveForward = false;
    private bool allowMovePingPong = false;
    private float timer = 0;
    private float moveSpeed;
    private int dir = 1;
    private void Update()
    {
        if (allowMoveForward)
        {

            if (isDoneMoveForward == false)
            {
                beginDisMoveForWard = BGTranslate.Cursor.Distance;
                float ratio = BGTranslate.Cursor.Distance / dis;
                moveTimeMoveForward = levelController.MoveTime - levelController.MoveTime * ratio;
                moveTimeMoveForward = (moveTimeMoveForward <= 0) ? levelController.MoveTime : moveTimeMoveForward;
                moveSpeed = (dis - BGTranslate.Cursor.Distance) / moveTimeMoveForward;
            }
            else
            {
                moveTimeMoveForward = levelController.MoveTime;
                moveSpeed = dis / moveTimeMoveForward;
            }
            BGTranslate.Cursor.Distance += moveSpeed * detal;
            if (BGTranslate.Cursor.Distance >= dis)
            {
                BGTranslate.Cursor.Distance = 0;
                isDoneMoveForward = true;
                //ProcessEndLineMoveNormal();
            }
            return;
        }
        if (allowMovePingPong)
        {
            if (isDoneMovePingPong == false)
            {
                beginDisMovePingPong = BGTranslate.Cursor.Distance;
                float ratio = BGTranslate.Cursor.Distance / dis;
                moveTimeMovePingPong = (levelController.MoveTime / 2) - (levelController.MoveTime / 2) * ratio;
                moveTimeMovePingPong = (moveTimeMovePingPong <= 0) ? levelController.MoveTime : moveTimeMovePingPong;
                moveSpeed = (dis - BGTranslate.Cursor.Distance) / moveTimeMovePingPong;
            }
            else
            {
                moveTimeMovePingPong = levelController.MoveTime / 2;
                moveSpeed = dis / moveTimeMovePingPong;
            }
            BGTranslate.Cursor.Distance += moveSpeed * dir * detal;
            if (BGTranslate.Cursor.Distance >= dis || BGTranslate.Cursor.Distance <= 0)
            {
                if (BGTranslate.Cursor.Distance > dis)
                {
                    BGTranslate.Cursor.Distance = dis;
                }
                if (BGTranslate.Cursor.Distance < 0)
                {
                    BGTranslate.Cursor.Distance = 0;
                }
                dir *= -1;
                isDoneMovePingPong = true;
                //ProcessEndLineMoveNormal();
            }
        }
    }
    bool isOutBeginDis(float beginDis)
    {
        return BGTranslate.Cursor.Distance > beginDis + 0.025f || BGTranslate.Cursor.Distance < beginDis - 0.025f;
    }
}
