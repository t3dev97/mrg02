﻿using System.Collections.Generic;
using UnityEngine;

public class MusicLevelsData
{
    private int id;
    private int idMusic;
    private List<List<int>> turns;

    public List<List<int>> Turns { get => turns; set => turns = value; }
    public int IdMusic { get => idMusic; set => idMusic = value; }
    public int Id { get => id; set => id = value; }
    public void ConverToTurns(string str)
    {
        turns = new List<List<int>>();

        string[] strCol = str.Split('|');
        for (int index = 0; index < strCol.Length; index++)
        {
            List<int> values = new List<int>();
            string[] row = strCol[index].Split(',');
            for (int i = 0; i < row.Length; i++)
            {
                //print("BeatID " + int.Parse(row[i]));
                values.Add(int.Parse(row[i]));
            }
            //print("row.Length =" + row.Length);
            //print("********************************************** Turn =" + index);
            turns.Add(values);
        }
    }
}
