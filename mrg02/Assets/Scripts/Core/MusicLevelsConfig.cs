﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class MusicLevelRow
{
    public int ID_LEVEL;
    public int ID_MUSIC;
    public string TURNS;
    public string TURN_TEMP;
}
public class MusicLevelsConfig : GConfigDataTable<MusicLevelRow>
{
    public MusicLevelsConfig() : base("ItemShopConfig")
    {

    }
    public MusicLevelsConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID_LEVEL");
    }

    public MusicLevelRow GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID_LEVEL", ID);
    }
    public List<MusicLevelRow> GetAllItem()
    {
        return records;
    }
}