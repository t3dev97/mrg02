﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class UserDataConfig
{
    public int ID;
    public int ID_SKIN_SELECTED;
    public string ID_SKINS;
    public string ID_LEVEL_UNLOCKED;
    public string ID_LEVEL_COMPLETE;
    public int STAR_NUMBER;
    public int HINT_SKILL_NUMBER;
    public int SHADOW_BALL_SKILL_NUMBER;
    public int SKIP_SKILL_NUMBER;
}
public class UserConfig : GConfigDataTable<UserDataConfig>
{
    public UserConfig() : base("ItemShopConfig")
    {

    }
    public UserConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID");
    }

    public UserDataConfig GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID", ID);
    }
    public List<UserDataConfig> GetAllItem()
    {
        return records;
    }
}