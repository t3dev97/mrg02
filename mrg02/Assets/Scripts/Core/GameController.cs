﻿
using DG.Tweening;
using System;
using UnityEngine;
public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public static Action<EGameStage> SetGameStage;
    [Header("UI Screens")]
    [SerializeField]
    private UIEndGameController uIEndGame;
    [SerializeField]
    private UIBattleGameController uIBattleGame;
    [SerializeField]
    private UIMainMenuController uIMainMenu;
    [SerializeField]
    private UIMapLevel uIMapLevel;
    [SerializeField]
    private UIAchievementController uIAchievement;
    [SerializeField]
    private UISettingController uISetting;
    [SerializeField]
    private UIShopController uIShop;
    [SerializeField]
    private UILanguageController uILanguage;
    [SerializeField]
    private UICreditController uICredit;
    [Header("UI Popup")]
    [SerializeField]
    private UIPopupConfirmController uIPopupConfirm;
    [SerializeField]
    private Transform uIWaiting;
    [SerializeField]
    private UIPopupPauseController uIPopupPause;
    [SerializeField]
    private UIPopupVipGoldController uIPopupVipGold;
    [SerializeField]
    private UIPopupRemoveAdsController uIPopupRemoveAds;
    [SerializeField]
    private UIPopupReviveController uIPopupRevive;
    [SerializeField]
    private UIPopupSupportController uIPopupSupport;
    [Space]
    [SerializeField]
    private Transform trsLogo;
    [SerializeField]
    private Transform trsLoading;
    private LevelController objLevel;
    private LevelController objLevelHint;
    [SerializeField]
    private bool isHasLevelHint = true;
    [Space]
    [SerializeField]
    private int currentLevel;
    [SerializeField]
    private int currentIdBall;
    [SerializeField]
    private EGameStage currentGameStage = EGameStage.PRE;
    [SerializeField]
    private EGameMode gameMode = EGameMode.NORMAL;
    private EEndGameStage eEndGame;
    private bool couting = false;
    private bool onHint = false;
    private bool popupShowing = false;
    [SerializeField] private AutoRotate autoRotate_1;
    [SerializeField] private AutoRotate autoRotate_2;
    [SerializeField]
    private EScreenName currentScreenTye;
    public EGameStage CurrentGameStage
    {
        get => currentGameStage;
        set
        {
            currentGameStage = value;
        }
    }
    public int CurrentIDLevel { get => currentLevel; set => currentLevel = value; }
    public UIPopupConfirmController UIPopupConfirm { get => uIPopupConfirm; set => uIPopupConfirm = value; }
    public EGameMode GameMode { get => gameMode; set => gameMode = value; }
    public EEndGameStage EEndGame { get => eEndGame; set => eEndGame = value; }
    public int CurrentIdBall
    {
        get => currentIdBall;
        set
        {
            currentIdBall = value > 0 ? value : 1;
        }
    }

    public bool Couting { get => couting; set => couting = value; }
    public LevelController ObjLevel { get => objLevel; set => objLevel = value; }
    public EScreenName CurrentScreenType { get => currentScreenTye; set => currentScreenTye = value; }
    public bool PopupShowing { get => popupShowing; set => popupShowing = value; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        trsLoading.gameObject.SetActive(true);
        DOVirtual.DelayedCall(1.0f, () =>
        {
            trsLoading.gameObject.SetActive(false);
            Application.targetFrameRate = 60;
            ShowUIMainMenu();
            UpdateShowStar();
            InitLevels();
            uIShop.OnCustomStart_v2();
            autoRotate_1.UpdateThis();
            autoRotate_2.UpdateThis();
            //currentIdBall = PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED, 1);            
            GooglePlayGameManager.Instance.Activate((temp) =>
            {
                if (temp)
                {
                    UserProfileManager.Instance.SyncUserProfile(true);
                }
                else
                {
                    ShowPopupConfirm("Login Fail", null);
                }
            });
            if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            {
                AdsManager.Instance.initBanner();
            }
        }).SetId(this);
    }
    public void GotoBattleGame()
    {
        ClearOldLevel();
        uIEndGame.Hide();
        uIMainMenu.Hide();
        uIMapLevel.Hide();
        uIPopupPause.Hide();
        uISetting.Hide();
        uILanguage.Hide();
        uIPopupVipGold.Hide();
        uIPopupRemoveAds.Hide();
        uIPopupRevive.Hide();
        uICredit.Hide();
        uIBattleGame.Show();
        currentGameStage = EGameStage.PLAYING;

        // Load current Level
        if (CheckNextLevelLive())
        {
            int indexLevel = currentLevel;
            SoundController.Instance.StopAllBeat();
            DOVirtual.DelayedCall(ConfigVariables.TIME_DELAY_OFF_SOUND, () =>
            {
                HideTimeCounter();

                objLevel = Instantiate(MCache.Instance.Levels[indexLevel], transform);

                objLevel.OnCustomStart(MCache.Instance.PlayerBalls[currentIdBall]);
                objLevel.gameObject.SetActive(true);
                uIBattleGame.SetUpTurn(objLevel.Turns.Length);
                UpdateTextShowCurrentLevel(currentLevel);
                if (MCache.Instance.LevelsHint.ContainsKey(indexLevel))
                {
                    objLevelHint = Instantiate(MCache.Instance.LevelsHint[indexLevel], objLevel.transform);
                    objLevelHint.OnCustomStart(MCache.Instance.PlayerBalls[currentIdBall]);
                    objLevelHint.gameObject.SetActive(false);
                }
                else
                {
                    isHasLevelHint = false;
                }
            });
        }
        else
        {
            uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_COMING_SOON, false, () =>
             {
                 ShowUIMainMenu();
             });
        }
    }
    public void HideTimeCounter()
    {
        uIBattleGame.HideTimeCounter();
    }
    public void ShowTimeCounter()
    {
        couting = true;
        uIBattleGame.ShowTimeCounter();
    }
    public void UpdateTimeCounter(float time)
    {
        uIBattleGame.UpdateTimeCounter(time);
    }
    public bool CheckNextLevelLive()
    {
        return MCache.Instance.Levels.ContainsKey(currentLevel);
    }
    public bool CheckNextLevelIsUnlock()
    {
        return MCache.Instance.UserData.IdLevelsUnlocked.Contains(currentLevel);
    }
    public void TouchInScreenToSpawnNextBall()
    {
        if (onHint)
            OffSkillHint();
        else
            objLevel.TouchInScreenToSpawnNextBall();
    }
    public EEndGameStage CheckEndGameStage()
    {
        return eEndGame;
    }
    public int SetOldLevel()
    {
        int oldLevel = (currentLevel - 1) > 0 ? currentLevel - 1 : 1; ;
        currentLevel = oldLevel;
        return oldLevel;
    }
    public void UpdateBoardNextBall(int turn)
    {
        uIBattleGame.UpdateNextBall(turn, objLevel.Turns.Length - 1);
    }
    public void RevertNextBall(int turn)
    {
        uIBattleGame.RevertNextBall(turn);
    }
    public void UpdateTextShowCurrentLevel(int currentLevel)
    {
        uIBattleGame.UpdateTextShowCurrentLevel(currentLevel);
    }
    public void InitLevels()
    {
        Debug.Log("fadfdfg");
        uIMainMenu.UpdateLevels();
    }
    public void InitUserData()
    {
        //currentIdBall = MCache.Instance.UserData.IdSkinSelected;
        //currentIdBall = PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED, 1);
    }
    public void ShowBtnGotoMain()
    {
        uIMainMenu.ShowBtnGotoMain();
    }
    public void HideBtnGotoMain()
    {
        uIMainMenu.HideBtnGotoMain();
    }
    public void HideBtnRemoveAds()
    {
        uIMainMenu.HideBtnRemoveAds();
    }
    public void ShowBtnAds()
    {
        uIMainMenu.ShowBtnRemoveAds();
    }
    public void ShowImgBGTopMainMenu()
    {
        uIMainMenu.ShowBGTop();
    }
    public void HideImgBGTopMainMenu()
    {
        uIMainMenu.HideBGTop();
    }
    public bool CheckShowBtnGotoMain()
    {
        return uIMainMenu.CheckShowBtnGotoMain();
    }
    private void UpdateShowStar()
    {
        uIMainMenu.UpdateStar(MCache.Instance.UserData.StarNumber);
    }
    public void UpdateShowStar(int numberStar)
    {
        uIMainMenu.UpdateStar(numberStar);
    }
    public void IncreaseStar(int increaseNumber)
    {
        MCache.Instance.UserData.StarNumber += increaseNumber;
        uIMainMenu.UpdateStar(MCache.Instance.UserData.StarNumber);
        GooglePlayGameManager.Instance.PostLeaderboard(MCache.Instance.UserData.StarNumber);
    }
    public void ReductionStar(int reductionNumber)
    {
        MCache.Instance.UserData.StarNumber -= reductionNumber;
        uIMainMenu.UpdateStar(MCache.Instance.UserData.StarNumber);
    }
    #region SKILL
    Tween TWHandleSkillShadowBall;
    public void HandleSkillShadowBall()
    {
        objLevel.TouchInScreenToSpawnNextBall(true);
        objLevel.ShadowSkillOn = true;
        TWHandleSkillShadowBall = DOVirtual.DelayedCall(objLevel.MoveTime, () =>
        {
            objLevel.ShadowSkillOn = false;
            print("Final Skill Shadown");
            BallDraftToLive();
        });
    }
    public void ShowBtnSkillHint()
    {
        uIBattleGame.InteractableToolTip();
    }
    public void ShowBtnShadown()
    {
        uIBattleGame.InteractableToolTip();
    }
    public void KillHandleSkillShadowBall()
    {
        TWHandleSkillShadowBall.Kill();
    }
    public void ClearAllBallDraft()
    {
        objLevel.ClearAllBallDraft();
        KillHandleSkillShadowBall();
        ShowBtnSkillHint();
    }
    private void BallDraftToLive()
    {
        objLevel.BallDraftToLive();
    }
    public void HandleSkillSkipTurn()
    {
        objLevel.SkipTurn();
    }
    public void HandleSkillSkipLevel()
    {
        currentLevel++;
        MCache.Instance.LevelsUI[currentLevel].ShowLevelUnLock();
        MCache.Instance.UserData.IdLevelsUnlocked.Add(currentLevel);
        GotoBattleGame();
    }
    public void HandleSkillHint()
    {
        //objLevel.HintSkillOn = true; 
        if (isHasLevelHint)
        {
            onHint = true;
            objLevelHint.gameObject.SetActive(true);
            uIBattleGame.UnInteractableToolTip();
            objLevel.HideActiveBall();
        }
        else
        {
            ShowPopupNotification(ConfigVariables.POPUP_NOTIFICATION_CONTENT_1);
            MCache.Instance.UserData.HintSkillNumber++;
        }
    }
    public void OffSkillHint()
    {
        onHint = false;
        objLevel.ShowActiveBall();
        SoundController.Instance.PlayEffect(EIDSoundEffect.Release_Beat);
        uIBattleGame.InteractableToolTip();
        if (objLevelHint != null)
        {
            objLevelHint.gameObject.SetActive(false);
        }
    }
    public void UnInteractableToolTip()
    {
        uIBattleGame.UnInteractableToolTip();
    }
    #endregion
    private void ClearOldLevel()
    {
        if (objLevel != null)
        {
            Destroy(objLevel.gameObject);
        }
    }
    public void HideToolTip()
    {
        uIBattleGame.HideSkillBar();
    }
    public void ShowUIToCountDown()
    {
        uIBattleGame.HideButtonToCountDown();
    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
    // +++++++++++++++++++++++++++++++++ Screens +++++++++++++++++++++++++++++++++++++++++++++
    #region Show Screens
    public void ShowEndGame(EEndGameStage endGameStage)
    {
        SoundController.Instance.StopMusicBG();
        //SoundController.Instance.StopAllBeat();
        eEndGame = endGameStage;
        if (currentGameStage == EGameStage.END_GAME) return;
        HideTimeCounter();
        uIBattleGame.Hide();
        currentGameStage = EGameStage.END_GAME;
        switch (endGameStage)
        {
            case EEndGameStage.WIN:
                {
                    if (!MCache.Instance.UserData.IdLevelsComplete.Contains(currentLevel))
                    {
                        IncreaseStar(1);
                        UpdateShowStar();
                        MCache.Instance.UserData.IdLevelsComplete.Add(currentLevel);
                        MCache.Instance.LevelsUI[currentLevel].ShowLevelComplete();
                    }
                    MCache.Instance.LevelsUI[currentLevel].Selected();
                    ++currentLevel;

                    if (!MCache.Instance.UserData.IdLevelsUnlocked.Contains(currentLevel))
                    {
                        ReUpdateUnlockLevel(currentLevel);
                        MCache.Instance.UserData.IdLevelsUnlocked.Add(currentLevel);
                    }
                    SoundController.Instance.PlayEffect(EIDSoundEffect.Win, ConfigVariables.TIME_WIN_GAME_SOUND);
                    //SoundController.Instance.PlayEffect(EIDSoundEffect.GAME_WIN, ConfigVariables.TIME_END_GAME_SOUND);
                    uIEndGame.ShowWinGame();
                    objLevel.ProcessGameWin();
                }
                break;
            case EEndGameStage.LOSE:
                {
                    CameraController.Instance.Shake();
                    SoundController.Instance.StopAllBeat();
                    SoundController.Instance.PlayEffect(EIDSoundEffect.Lose, ConfigVariables.TIME_LOSE_GAME_SOUND);
                    //SoundController.Instance.PlayEffect(EIDSoundEffect.GAME_OVER, ConfigVariables.TIME_END_GAME_SOUND);
                    uIEndGame.ShowLoseGame();
                    objLevel.ProcessGameLose();
                }
                break;
        }
    }
    public void ReShowLockLevel(int level)
    {
        MCache.Instance.LevelsUI[level].ShowLevelLock();
    }
    public void ReUpdateUnlockLevel(int level)
    {
        MCache.Instance.LevelsUI[level].ShowLevelUnLock();
    }
    public void ReUpdateLevelComplete(int level)
    {
        MCache.Instance.LevelsUI[level].ShowLevelComplete();
    }
    public void ReUpdateSkin(int id)
    {
        //MCache.Instance.SkinItemsUI[id].UnlockSkin();
    }
    public void ReResetSkin()
    {
        for (int index = 0; index < MCache.Instance.SkinItemsUI.Count; index++)
        {
            MCache.Instance.SkinItemsUI[index].ResetStage();
        }
    }
    public void ShowUIMainMenu()
    {
        ClearOldLevel();
        SoundController.Instance.StopMusicBG();
        SoundController.Instance.StopAllBeat();
        currentGameStage = EGameStage.PRE;
        uIEndGame.Hide();
        uIBattleGame.Hide();
        uIMapLevel.Hide();
        uIAchievement.Hide();
        uIPopupPause.Hide();
        uISetting.Hide();
        uIShop.Hide();
        uILanguage.Hide();
        uIPopupVipGold.Hide();
        uIPopupRemoveAds.Hide();
        uIPopupRevive.Hide();
        uICredit.Hide();
        uIMainMenu.Show();
        //MCache.Instance.LevelsUI[currentLevel].Selected();
    }
    public void ShowUIMapLevel()
    {
        ClearOldLevel();
        uIMainMenu.Hide();
        uIMapLevel.Show();
    }
    public void ShowUIAchievement()
    {
        uIMainMenu.Hide();
        uIAchievement.Show();
    }
    public void ShowUISetting()
    {
        uIMainMenu.Hide();
        uISetting.Show();
    }
    public void ShowUILanguage()
    {
        uIMainMenu.Hide();
        uILanguage.Show();
        TrackingLog.Instance.SendEventWithSessionInfo("ux_screenflow", "screen_action", "scr_" + GameScreen.settings.ToString().ToLower() + "_language_click");
    }
    public void ShowUIShop()
    {
        uIMainMenu.Hide();
        uIShop.Show();
    }
    public void ShowUICredit()
    {
        uIMainMenu.Hide();
        uICredit.Show();
    }
    #endregion
    // +++++++++++++++++++++++++++++++++ Popups +++++++++++++++++++++++++++++++++++++++++++++
    #region Show Popups
    public bool CheckPopupShowing()
    {
        return true;
    }
    public void ShowPopupSupport()
    {
        uIPopupSupport.Show();
    }
    public void ShowPopupWaiting()
    {
        uIWaiting.gameObject.SetActive(true);
    }
    public void HidePopupWaiting()
    {
        uIWaiting.gameObject.SetActive(false);
    }
    public void ShowPopupConfirm(string str, Action action)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, str, true, () =>
        {
            if (action != null)
                action();
        });
    }
    public void ShowPopupNotification(string str, bool showCancel = false, Action action = null)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, str, showCancel, () =>
        {
            if (action != null)
                action();
        });
    }
    public void ShowPopupCommingSoon()
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_COMING_SOON, true);
    }
    public void ShowPopupLockLevel()
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_LEVEL_LOCK, true);
    }
    public void ShowPopupNotEnoughStar()
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_SKIN_UNLOCK, true);
    }
    public void ShowPopupUnlockByAds(Action onComplete)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_UNLOCK_BY_ADS, false, () =>
        {
            onComplete();
        });
    }
    public void ShowPopupConfirmOpenURL(Action onComplete)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_OPENT_URL_LINK, true, () =>
        {
            onComplete();
        });
    }
    public void ShowPopupConfirmOpenAds(Action onComplete)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_OPENT_ADS, true, () =>
        {
            onComplete();
        });
    }
    public void ShowPopupConfirmOptions(Action option_1, Action option_2)
    {
        uIPopupConfirm.Show(ConfigVariables.POPUP_NAME_NOTIFICATION, ConfigVariables.POPUP_NOTIFICATION_CONTENT_CONFIRM_UPDATE_DATA_USER, ConfigVariables.POPUP_NOTIFICATION_NAME_OPTION_1_CONFIRM_UPDATE_DATA_USER, ConfigVariables.POPUP_NOTIFICATION_NAME_OPTION_2_CONFIRM_UPDATE_DATA_USER, option_1, option_2);
    }
    public void ShowPopupPause()
    {
        uIPopupPause.Show();
    }
    public void ShowPopupVipGold()
    {
        uIPopupVipGold.Show();
    }
    public void ShowPopupRemoveAds()
    {
        uIPopupRemoveAds.Show();
    }
    #endregion
}
