﻿using UnityEngine;

public class BallItemView : MonoBehaviour
{
    [SerializeField]
    private Transform imgBeatOff;
    [SerializeField]
    private Transform imgBeatOn;
    [SerializeField]
    private Transform imgBeatOutLine;
    [SerializeField]
    private Transform imgBeatSelectNextBall;
    public void ShowNormal()
    {
        imgBeatOff.gameObject.SetActive(false);
        imgBeatOn.gameObject.SetActive(true);
        imgBeatOutLine.gameObject.SetActive(false);
        imgBeatSelectNextBall.gameObject.SetActive(false);
    }
    public void ShowCurrent(bool isFull = false)
    {
        imgBeatOff.gameObject.SetActive(false);
        imgBeatOn.gameObject.SetActive(true);
        imgBeatOutLine.gameObject.SetActive(false);
        //imgBeatSelectNextBall.gameObject.SetActive(!isFull);
    }
    public void ShowNext()
    {
        imgBeatSelectNextBall.gameObject.SetActive(false);
        imgBeatOutLine.gameObject.SetActive(true);
    }
    public void HideNext()
    {
        imgBeatSelectNextBall.gameObject.SetActive(false);
    }    
    public void Hide()
    {
        imgBeatOff.gameObject.SetActive(true);
        imgBeatOn.gameObject.SetActive(false);
        imgBeatOutLine.gameObject.SetActive(false);
        imgBeatSelectNextBall.gameObject.SetActive(false);
    }
}
