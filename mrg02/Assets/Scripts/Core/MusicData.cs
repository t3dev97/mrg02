﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicData
{
    private int id;
    private string name;
    private List<AudioClip> audios;

    public int Id { get => id; set => id = value; }
    public string Name { get => name; set => name = value; }
    public List<AudioClip> Audios { get => audios; set => audios = value; }
}
