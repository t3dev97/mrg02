﻿using UnityEngine;

public class LineHightLight : MonoBehaviour
{
    [SerializeField]
    private ELineType lineType = ELineType.CIRCLE;
    private GameObject mask;
    private Transform child;
    private Transform trsContent;
    public ELineType LineType { get => lineType; set => lineType = value; }
    public Transform TrsContent { get => trsContent; set => trsContent = value; }

    public void OnCustomStart()
    {
        mask = new GameObject("Mask");
        mask.transform.parent = transform;
        mask.transform.localPosition = Vector3.zero;
        HideMask();
        SpriteRenderer sprite = mask.gameObject.AddComponent<SpriteRenderer>();
        switch (lineType)
        {
            case ELineType.CIRCLE:
                sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_circle_mask];
                mask.transform.localScale = ConfigVariables.SCALE_CIRCLE;
                break;
            case ELineType.ELLIPSE:
                sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_ellipse_mask];
                mask.transform.localScale = ConfigVariables.SCALE_ELLIPSE;
                break;
            case ELineType.SQUARE:
                sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_square_mask];
                mask.transform.localRotation = new Quaternion(0, 0, 0, 0);
                mask.transform.localScale = ConfigVariables.SCALE_SQUARE;
                break;
            case ELineType.TRIANGLE:
                sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_triangle_mask];
                mask.transform.localRotation = new Quaternion(0, 0, 0, 0);
                mask.transform.localScale = ConfigVariables.SCALE_TRIANGLE;
                mask.transform.localPosition = new Vector3(0, 0.45f, 0);
                break;
            case ELineType.RHOMBUS:
                sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_rhombus_mask];
                mask.transform.localRotation = new Quaternion(0, 0, 0, 0);
                mask.transform.localScale = ConfigVariables.SCALE_RHOMBUS;
                break;
            case ELineType.LINE:
                //sprite.sprite = MCache.Instance.ImgMasks[ConfigVariables.shape_rhombus_mask];
                //mask.transform.localRotation = new Quaternion(0, 0, 0, 0);
                mask.gameObject.SetActive(false);
                break;
            case ELineType.CUSTOM:
                break;
        }
    }
    private void ShowMask()
    {
        if (lineType != ELineType.LINE)
            mask.gameObject.SetActive(true);
    }
    private void HideMask()
    {
        if (lineType != ELineType.LINE)
            mask.gameObject.SetActive(false);
    }
    public void OnHighlight()
    {
        ShowMask();
        gameObject.GetComponent<LineRenderer>().material = MCache.Instance.MaterialsLine[ConfigVariables.LINE_SHOW];
        if (transform.childCount > 1)
        {
            foreach (Transform item in transform)
            {
                LineRenderer lineRenderer = item.gameObject.GetComponent<LineRenderer>();
                if (lineRenderer)
                    item.gameObject.GetComponent<LineRenderer>().material = MCache.Instance.MaterialsLine[ConfigVariables.LINE_SHOW];
            }
        }
    }
    public void OffHighlight()
    {
        HideMask();
        gameObject.GetComponent<LineRenderer>().material = MCache.Instance.MaterialsLine[ConfigVariables.LINE_HIDE];
        if (transform.childCount > 1)
        {
            foreach (Transform item in transform)
            {
                LineRenderer lineRenderer = item.gameObject.GetComponent<LineRenderer>();
                if (lineRenderer)
                    item.gameObject.GetComponent<LineRenderer>().material = MCache.Instance.MaterialsLine[ConfigVariables.LINE_HIDE];
            }
        }
    }
    public void OnHighlightNoMask()
    {
        gameObject.SetActive(true);
        HideMask();
    }
}
