﻿using System.Collections.Generic;
using UnityEngine;

public class HintSkillData
{
    private int id;
    private List<List<float>> turns;

    public List<List<float>> Turns { get => turns; set => turns = value; }
    public int Id { get => id; set => id = value; }
    public void ConverToTurns(string str)
    {
        turns = new List<List<float>>();
        //Debug.Log("str " + str);
        string[] strCol = str.Split('|');
        for (int index = 0; index < strCol.Length; index++)
        {
            List<float> values = new List<float>();
            string[] row = strCol[index].Split(',');
            for (int i = 0; i < row.Length; i++)
            {
                values.Add(float.Parse(row[i]));                
            }
            turns.Add(values);
        }
    }
}
