﻿using UnityEngine;
using UnityEngine.UI;

public class LevelItem : MonoBehaviour
{
    public static Transform ImgOldBGOuline;
    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Text txtNumberLevel;
    [SerializeField]
    private Text txtSpecical;
    [SerializeField]
    private Transform imgAds;
    [SerializeField]
    private Image imgStar;
    [SerializeField]
    private Image imgBG;
    [SerializeField]
    private Sprite imgBGLock;
    [SerializeField]
    private Sprite imgBGUnLock;
    [SerializeField]
    private Transform imgBGOuline;
    [SerializeField]
    private Image imgIConLevel;
    [SerializeField]
    private Image imgLock;

    private int idLevel;

    public int IdLevel { get => idLevel; set => idLevel = value; }

    private void Start()
    {
        imgBGOuline.gameObject.SetActive(false);
        btnPlay.onClick.AddListener(OnClickBtnPlay);
    }
    public void UpdateLevel(int level)
    {
        txtNumberLevel.text = level.ToString();
        idLevel = level;
        if (MCache.Instance.ImgIconLevels.ContainsKey(level) && imgIConLevel != null)
        {
            imgIConLevel.sprite = MCache.Instance.ImgIconLevels[level];
            imgIConLevel.SetNativeSize();
            imgIConLevel.gameObject.SetActive(true);
        }
    }
    public void UpdateLevel()
    {
        txtNumberLevel.text = idLevel.ToString();        
        if (MCache.Instance.ImgIconLevels.ContainsKey(idLevel) && imgIConLevel != null)
        {
            imgIConLevel.sprite = MCache.Instance.ImgIconLevels[idLevel];
            imgIConLevel.SetNativeSize();
            imgIConLevel.gameObject.SetActive(true);
        }
    }
    public void OnClickBtnPlay()
    {
        SoundController.Instance.PlayEffect(EIDSoundEffect.Select_Level);
        bool checkLevelUnlock = MCache.Instance.UserData.IdLevelsUnlocked.Contains(idLevel);
        if (checkLevelUnlock)
        {
            if (ImgOldBGOuline != null)
                ImgOldBGOuline.gameObject.SetActive(false);
            imgBGOuline.gameObject.SetActive(true);
            ImgOldBGOuline = imgBGOuline;
            GameController.Instance.CurrentIDLevel = idLevel;
            GameController.Instance.GotoBattleGame();
        }
        else
        {
            GameController.Instance.ShowPopupLockLevel();
        }
    }
    public void Selected()
    {
        if (ImgOldBGOuline != null)
            ImgOldBGOuline.gameObject.SetActive(false);
        imgBGOuline.gameObject.SetActive(true);
        ImgOldBGOuline = imgBGOuline;
    }
    public void ShowLevelLock()
    {
        imgIConLevel.transform.SetAsFirstSibling();

        imgBG.sprite = imgBGLock;
        if (imgAds != null)
            imgAds.gameObject.SetActive(false);

        if (txtSpecical != null)
            txtSpecical.gameObject.SetActive(false);

        if (imgStar != null)
            imgStar.gameObject.SetActive(false);

        imgLock.gameObject.SetActive(true);

    }
    public void ShowLevelUnLock()
    {
        imgIConLevel.transform.SetAsLastSibling();
        imgBG.sprite = imgBGUnLock;
        if (imgAds != null)
            imgAds.gameObject.SetActive(false);

        if (txtSpecical != null)
            txtSpecical.gameObject.SetActive(false);

        if (imgStar != null)
            imgStar.gameObject.SetActive(false);

        if (imgStar != null)
            imgStar.gameObject.SetActive(false);

        imgLock.gameObject.SetActive(false);
    }
    public void ShowLevelComplete()
    {
        if (MCache.Instance.UserData.IdLevelsUnlocked.Contains(idLevel) == false) return;

        if (imgStar != null)
            imgStar.gameObject.SetActive(true);
    }
}
