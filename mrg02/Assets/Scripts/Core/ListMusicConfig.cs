﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class MusicConfig
{
    public int ID;
    public string NAME;
}
public class ListMusicConfig : GConfigDataTable<MusicConfig>
{
    public ListMusicConfig() : base("ItemShopConfig")
    {

    }
    public ListMusicConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID");
    }

    public MusicConfig GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID", ID);
    }
    public List<MusicConfig> GetAllItem()
    {
        return records;
    }
}