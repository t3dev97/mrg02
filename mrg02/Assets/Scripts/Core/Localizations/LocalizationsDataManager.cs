﻿using System.Collections.Generic;
using UnityEngine;
public enum ELanguage
{
    VN = 0,
    US = 1,
    JAPAN = 2,
    CHINESE = 3
}
public class LocalizationsDataManager : SingletonMono<LocalizationsDataManager>
{

    [SerializeField]
    private ELanguage language;
    private Dictionary<string, LocalizationData> localizationsData = new Dictionary<string, LocalizationData>();
    public Dictionary<string, LocalizationData> LocalizationData { get => localizationsData; set => localizationsData = value; }

    public void Init()
    {
        var localizationConfig = AvConfigManager.S_LocalizationConfig.GetAllItem();
        foreach (var item in localizationConfig)
        {
            LocalizationData cache = new LocalizationData();
            cache.Id = item.ID;
            cache.Key = item.KEY;
            cache.Vn = item.VN;
            cache.Chines = item.CHINESE;
            cache.Japan = item.JAPAN;
            cache.Us = item.US;
            localizationsData.Add(cache.Key, cache);
        }
    }
    public ELanguage GetCurrentLanguage()
    {
        return language;
    }
    public void SetLanguage(ELanguage newLanguage)
    {
        if (newLanguage == language) return;
        language = newLanguage;
        TextLocalization.OnChangeLangauge(newLanguage);
    }
}
