﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class LocalizationDataConfig
{
    public int ID;
    public string KEY;
    public string VN;
    public string US;
    public string JAPAN;
    public string CHINESE;
}
public class LocalizationConfig : GConfigDataTable<LocalizationDataConfig>
{
    public LocalizationConfig() : base("ItemShopConfig")
    {

    }
    public LocalizationConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID");
    }

    public LocalizationDataConfig GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID", ID);
    }
    public List<LocalizationDataConfig> GetAllItem()
    {
        return records;
    }
}