﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextLocalization : MonoBehaviour
{
    public static Action<ELanguage> OnChangeLangauge;
    [SerializeField]
    private string key;
    private Text txtTarget;
    private void Awake()
    {
        txtTarget = GetComponent<Text>();
    }
    private void OnEnable()
    {        
        SwitchLanguage(LocalizationsDataManager.Instance.GetCurrentLanguage());
        OnChangeLangauge += SwitchLanguage;
    }
    private void OnDisable()
    {
        OnChangeLangauge -= SwitchLanguage;
    }
    private void SwitchLanguage(ELanguage language)
    {
        string str = key;
        try
        {
            switch (language)
            {
                case ELanguage.VN:
                    str = LocalizationsDataManager.Instance.LocalizationData[key].Vn;
                    break;
                case ELanguage.US:
                    str = LocalizationsDataManager.Instance.LocalizationData[key].Us;
                    break;
                case ELanguage.JAPAN:
                    str = LocalizationsDataManager.Instance.LocalizationData[key].Japan;
                    break;
                case ELanguage.CHINESE:
                    str = LocalizationsDataManager.Instance.LocalizationData[key].Chines;
                    break;
                default:
                    str = key;
                    break;
            }
        }
        catch (System.Exception)
        {
            Debug.Log("Key is error" + key + "in object name " + gameObject.name);
        }

        txtTarget.text = str;
    }
}
