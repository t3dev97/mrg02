﻿using UnityEngine;

public class LocalizationData
{
    private int id;
    private string key;
    private string vn;
    private string us;
    private string japan;
    private string chines;

    public LocalizationData()
    {
    }
    public int Id { get => id; set => id = value; }
    public string Key { get => key; set => key = value; }
    public string Vn { get => vn; set => vn = value; }
    public string Us { get => us; set => us = value; }
    public string Japan { get => japan; set => japan = value; }
    public string Chines { get => chines; set => chines = value; }
}
