﻿using UnityEngine;

public class ConfigVariables : MonoBehaviour
{
    public static bool OnDebug = true;
    public static bool Sound = true;
    public static bool Music = true;
    public static bool Vibration = true;
    public static bool Notification = true;
    public static int LevelUnLock = 20;
    public static float LimitHeightScreen = 6.0f;
    public static float LimitWidthScreen = 2.0f;
    public const float TIME_AUTO_DESTROY_SOUND_AFFECT = 0.7f;
    public const float TIME_DELAY_OFF_SOUND = 0.2f;
    public const float TIME_DELAY_ON_SOUND = 0.1f;
    public const float TIME_WIN_GAME_SOUND = 1.0f;
    public const float TIME_LOSE_GAME_SOUND = 1.0f;
    public const float SFX_VOLUMN = 1.0f; 
    public const float SCALE_EFFECT_ZOOM_PHOTO = 5.0f;
    public const float SCALE_RATIO_X = 0.15f;
    public const float SCALE_RATIO_Y = 0.15f;
    public const float DELAY_TIME_WAITING = 0.2f;
    public const float FACTOR_MOVE = 0.18f; 
    public const float DETAL_MOVE = 0.0186639f; 
    public const float DISTANCE_HINT_LEVEl_Z = -5.5f;

    public const string LEVEL_ITEM = "Level_Item";
    public const string LEVEL_ITEM_SPACIAL = "Level_Item_Spacial";
    public const string WOLRD_ITEM = "World";

    public const float MIN_MOVE_TO_POS = 0.2f;
    public static Vector3 SCALE_CIRCLE = new Vector3(1.15f, 1.15f, 1);
    public static Vector3 SCALE_ELLIPSE = new Vector3(1.75f, 1.15f, 1);
    public static Vector3 SCALE_RHOMBUS = new Vector3(0.85f, 0.6f, 1);
    public static Vector3 SCALE_SQUARE = new Vector3(1.15f, 1.15f, 1);
    public static Vector3 SCALE_TRIANGLE = new Vector3(2.0f, 2.0f, 1);
    // PlayerPrefs
    public const string BEST_SCORE = "BEST_SCORE";
    public const string ID_SKIN_SELECTED = "ID_SKIN_SELECTED";
    // UI
    public const float POS_Y_BEGIN = 1500;
    public const float POS_Y_SHOW = 800; //800
    public const float TIME_MOVE = 0.5f;
    // Camera
    public const float DURATION_BIG_SHAKE = 0.45f;
    public const float DURATION_SHAKE = 0.08f;
    public const float OFFSET_SHAKE = 0.15f;
    // String 
    public const string POPUP_NAME_NOTIFICATION = "Notification";
    public const string POPUP_NOTIFICATION_CONTENT_COMING_SOON = "The feature is coming soon";
    public const string POPUP_NOTIFICATION_CONTENT_LEVEL_LOCK = "The level is not unlock";
    public const string POPUP_NOTIFICATION_CONTENT_TRUE = "The feature is TRUE";
    public const string POPUP_NOTIFICATION_CONTENT_FALSE = "The feature is FALSE";
    public const string POPUP_NOTIFICATION_CONTENT_LOGIN_COMPLETE = "Login complete";
    public const string POPUP_NOTIFICATION_CONTENT_WATCH_FAIL = "The watch ads is FAIL";
    public const string POPUP_NOTIFICATION_CONTENT_SKIN_UNLOCK = "The star is not enough";
    public const string POPUP_NOTIFICATION_CONTENT_UNLOCK_BY_ADS = "The feature unlocked by watch ADS";
    public const string POPUP_NOTIFICATION_CONTENT_OPENT_URL_LINK = "The feature will open url link!";
    public const string POPUP_NOTIFICATION_CONTENT_OPENT_ADS = "The feature will open ADS!";
    public const string POPUP_NOTIFICATION_CONTENT_CONFIRM_UPDATE_DATA_USER = "You want to UPDATE data from cloud into your phone or PUSH data from your phone to cloud";
    public const string POPUP_NOTIFICATION_NAME_OPTION_1_CONFIRM_UPDATE_DATA_USER = "UPDATE";
    public const string POPUP_NOTIFICATION_NAME_OPTION_2_CONFIRM_UPDATE_DATA_USER = "PUSH";
    public const string POPUP_NOTIFICATION_CONTENT_1 = "The HINT of this Level is coming soon!";
    public const string TXT_EQUIPED = "EQUIPED";
    public const string TXT_EQUIP = "EQUIP";
    public const string strLinkCompany = "https://play.google.com/store/apps/dev?id=8868470488288288514";
    public const string LINE_HIDE = "line_hide";
    public const string LINE_SHOW = "line_show";

    public const string worm_main_show_mat = "worm_main_show_mat";
    public const string shape_circle_mask = "shape_circle_mask";
    public const string shape_ellipse_mask = "shape_ellipse_mask";
    public const string shape_mask = "shape_mask";
    public const string shape_square_mask = "shape_square_mask";
    public const string shape_triangle_mask = "triangle_mask";//"shape_triangle_mask";
    public const string shape_rhombus_mask = "shape_rhombus_mask";
}
public enum EIDSoundEffect
{
    GAME_OVER,
    GAME_WIN,
    Select_Level,
    Click_Button,    
    Back_Button,    
    Swipe_Up_Menu,
    Swipe_Down_Menu,
    Active_Hint,
    Active_MagicBall,
    Active_SkipMove,
    Release_Beat,
    Win,
    Lose,
    Reward    
}
public enum EGameMode
{
    NORMAL,
    WORM_V1,
    WORM_V2
}
public enum EGameStage
{
    PRE,
    PLAYING,
    END_GAME
}
public enum EScreenName
{
    MAIN_MENU,
    ACTION_PHASE,
    SHOP,
    SETIING,
    ACHIEVEMENTS,
    LAUGUAGE,
    CREDIT,
    LOADING,
    LOGO
}
public enum EPopupName
{
    END_GAME,
    PAUSE,
    VIP_GOLD,
    REMOVE_ADS,
    REVIVE,
    NOTIFICATION,
    SUPPORT
}
public enum EEndGameStage
{
    WIN,
    LOSE
}
public enum EEnemyType
{
    NEEDLE, // KIM 
    GROWING, // MOC 
    WATER, // THUY 
    FIRE, // HOA 
    EARTH // THO
}
public enum EEnemyShootType
{
    LINEAR,
    RANDOM
}
public enum ETagName
{
    Player,
    Obstacle,
    PlayerDraft
}
public enum ELayer
{
    Obstacle = 8
}
public enum ELineType
{
    CIRCLE,
    ELLIPSE,
    SQUARE,
    TRIANGLE,
    RHOMBUS,
    LINE,
    CUSTOM
}
