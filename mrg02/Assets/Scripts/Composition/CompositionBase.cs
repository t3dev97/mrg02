﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

public enum EGridType
{
    SafeArea,
    BasicGrid,
    Custom1,
    Custom2,
    Custom3
}

public abstract class CompositionBase : MonoBehaviour
{
    [SerializeField] protected int numOfVerticalLine = 2;
    [SerializeField] protected int numOfHorizontalLine = 2;
    private int cacheNumOfVerticalLine = 2;
    private int cacheNumOfHorizontalLine = 2;

    protected EGridType compositionType = EGridType.SafeArea;
    [SerializeField] protected Color lineColor = Color.white;
    [SerializeField] protected float lineThickness = 1;
    [SerializeField] protected Image lineVerPrefab;
    [SerializeField] protected Image lineHorPrefab;
    protected List<Image> listLineVertical = new List<Image>();
    protected List<Image> listLineHorizontal = new List<Image>();

    protected float widthSize = 0;
    protected float heightSize = 0;

    [HideInInspector]public bool isCanEdit = false;

    private void OnEnable()
    {
        LeanTouch.OnGesture += HandleGesture;
        Init();       
    }
    private void OnDisable()
    {
        LeanTouch.OnGesture -= HandleGesture;
    }

    public virtual void Init()
    {
        Debug.Log(this + ": Init");
        Clear();

        cacheNumOfVerticalLine = numOfVerticalLine;
        cacheNumOfHorizontalLine = numOfHorizontalLine;

        widthSize = Screen.safeArea.width/ UIManager.Instance.GetScaleUI() / (numOfVerticalLine + 1);
        heightSize = Screen.safeArea.height / UIManager.Instance.GetScaleUI() / (numOfHorizontalLine + 1);

        for (int i = 0; i < numOfVerticalLine + 2; i++)
        {
            var line = Instantiate(lineVerPrefab, transform);
            line.gameObject.SetActive(true);
            line.color = lineColor;
            line.rectTransform.sizeDelta = new Vector2(1, 1);
            line.rectTransform.localScale = new Vector2(lineThickness, Screen.safeArea.height / UIManager.Instance.GetScaleUI());

            listLineVertical.Add(line);
        }

        for (int i = 0; i < numOfHorizontalLine + 2; i++)
        {
            var line = Instantiate(lineHorPrefab, transform);
            line.gameObject.SetActive(true);
            line.color = lineColor;
            line.rectTransform.sizeDelta = new Vector2(1, 1);
            line.rectTransform.localScale = new Vector2(lineThickness, Screen.safeArea.width / UIManager.Instance.GetScaleUI());

            listLineHorizontal.Add(line);
        }

        DrawLine();
    }

    protected virtual void DrawLine()
    {
        for (int i = 0; i < listLineVertical.Count; i++)
        {
            listLineVertical[i].rectTransform.anchoredPosition = new Vector2(i * widthSize, 0);
        }

        for (int i = 0; i < listLineHorizontal.Count; i++)
        {
            listLineHorizontal[i].rectTransform.anchoredPosition = new Vector2(0, heightSize * i);
        }
    }

    public virtual void ReDrawLine()
    {
        Init();
    }

    private void Clear()
    {
        foreach (var line in listLineVertical)
            Destroy(line.gameObject);

        foreach (var line in listLineHorizontal)
            Destroy(line.gameObject);

        listLineVertical.Clear();
        listLineHorizontal.Clear();
    }

    private void Update()
    {
        if (numOfHorizontalLine != cacheNumOfHorizontalLine || numOfVerticalLine != cacheNumOfVerticalLine)
        {
            ReDrawLine();
        }
    }
    protected virtual void HandleGesture(List<LeanFinger> fingers)
    {
    }
}
