﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class Composition_Custom : CompositionBase
{
    public override void Init()
    {
        base.Init();
        compositionType = EGridType.BasicGrid;
    }

    protected override void DrawLine()
    {
        base.DrawLine();
    }

    public override void ReDrawLine()
    {
        base.ReDrawLine();
    }

    protected override void HandleGesture(List<LeanFinger> fingers)
    {
        base.HandleGesture(fingers);
        if (!isCanEdit) return;
       
        if (fingers.Count == 1)
        {
            //move
            transform.localPosition = new Vector3(transform.localPosition.x + LeanGesture.GetScreenDelta(fingers).x, transform.localPosition.y + LeanGesture.GetScreenDelta(fingers).y);

        }
        else if (fingers.Count == 2)
        {
            //Scale
            var dis = fingers[0].ScreenPosition - fingers[1].ScreenPosition;
            if (Mathf.Abs(dis.x) <= Mathf.Abs(dis.y))
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * LeanGesture.GetPinchScale(fingers));
            }
            else
            {
                transform.localScale = new Vector3(transform.localScale.x * LeanGesture.GetPinchScale(fingers), transform.localScale.y);
            }
        }
    }
}
