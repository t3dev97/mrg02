﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EEditComposition
{
    None,
    Custom01,
    Custom02,
    Custom03,
}
public class CompositionManager : MonoBehaviour
{
    [SerializeField] private List<CompositionBase> listComposition;
    [Header("SaveArea")]
    [SerializeField] private bool isShowSafeAre = false;
    private bool cacheIsShowSafeAre = false;

    [Header("BasicGrid")]
    [SerializeField] private bool isShowBasicGrid = false;
    private bool cacheIsShowBasicGrid = false;

    [Header("Custom01")]
    [SerializeField] private bool isShowCustom01 = false;
    private bool cacheIsShowCustom01 = false;

    [Header("Custom02")]
    [SerializeField] private bool isShowCustom02 = false;
    private bool cacheIsShowCustom02 = false;

    [Header("Custom03")]
    [SerializeField] private bool isShowCustom03 = false;
    private bool cacheIsShowCustom03 = false;
    [SerializeField] private EEditComposition editComposition = EEditComposition.None;
    private EEditComposition cacheEditComposition = EEditComposition.None;

    // Start is called before the first frame update
    void Start()
    {
        cacheIsShowSafeAre = isShowSafeAre;
        cacheIsShowBasicGrid = isShowBasicGrid;
        cacheIsShowCustom01 = isShowCustom01;
        cacheIsShowCustom02 = isShowCustom02;
        cacheIsShowCustom03 = isShowCustom03;
        cacheEditComposition = editComposition;

        listComposition = new List<CompositionBase>(GetComponentsInChildren<CompositionBase>(true));
        listComposition[0].gameObject.SetActive(isShowSafeAre);
        listComposition[1].gameObject.SetActive(isShowBasicGrid);
        listComposition[2].gameObject.SetActive(isShowCustom01);
        listComposition[3].gameObject.SetActive(isShowCustom02);
        listComposition[4].gameObject.SetActive(isShowCustom03);
    }

    private void Update()
    {
        if (cacheIsShowSafeAre != isShowSafeAre)
        {
            cacheIsShowSafeAre = isShowSafeAre;
            listComposition[0].gameObject.SetActive(isShowSafeAre);
        }

        if (cacheIsShowBasicGrid != isShowBasicGrid)
        {
            cacheIsShowBasicGrid = isShowBasicGrid;
            listComposition[1].gameObject.SetActive(isShowBasicGrid);
        }

        if (cacheIsShowCustom01 != isShowCustom01)
        {
            cacheIsShowCustom01 = isShowCustom01;
            listComposition[2].gameObject.SetActive(isShowCustom01);
        }

        if (cacheIsShowCustom02 != isShowCustom02)
        {
            cacheIsShowCustom02 = isShowCustom02;
            listComposition[3].gameObject.SetActive(isShowCustom02);
        }

        if (cacheIsShowCustom03 != isShowCustom03)
        {
            cacheIsShowCustom03 = isShowCustom03;
            listComposition[4].gameObject.SetActive(isShowCustom03);
        }

        if (cacheEditComposition != editComposition)
        {
            Debug.Log("Change Edit: " + editComposition);
            cacheEditComposition = editComposition;

            listComposition[2].isCanEdit = false;
            listComposition[3].isCanEdit = false;
            listComposition[3].isCanEdit = false;
            switch (editComposition)
            {
                case EEditComposition.Custom01:
                    listComposition[2].isCanEdit = true;
                    break;
                case EEditComposition.Custom02:
                    listComposition[3].isCanEdit = true;
                    break;
                case EEditComposition.Custom03:
                    listComposition[4].isCanEdit = true;
                    break;
            }
        }

    }


}
