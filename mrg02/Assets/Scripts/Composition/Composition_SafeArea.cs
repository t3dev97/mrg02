﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Composition_SafeArea : CompositionBase
{
    public override void Init()
    {
        base.Init();
        compositionType = EGridType.SafeArea;
    }

    protected override void DrawLine()
    {
        base.DrawLine();
       
    }

    public override void ReDrawLine()
    {
        base.ReDrawLine();
    }
}
