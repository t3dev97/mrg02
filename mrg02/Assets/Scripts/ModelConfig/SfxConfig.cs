﻿using FileHelpers;
using System.Collections.Generic;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class SfxRow
{
    public int ID;
    public string NAME;    
}
public class SfxConfig : GConfigDataTable<SfxRow>
{
    public SfxConfig() : base("ItemShopConfig")
    {

    }
    public SfxConfig(string name) : base(name)
    {

    }
    protected override void OnDataLoaded()
    {
        //rebuild data index
        RebuildIndexField<int>("ID");
    }

    public SfxRow GetItem(int ID)
    {
        return FindRecordByIndex<int>("ID", ID);
    }
    public List<SfxRow> GetAllItem()
    {
        return records;
    }
}