﻿using UnityEngine;
using System;
using System.Collections;
#if UNITY_ANDROID
using GooglePlayGames;
#elif UNITY_IOS
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class GooglePlayGameManager : SingletonMonoAwake<GooglePlayGameManager>
{
    public string userID;
    private bool mAuthenticating = false;

    public bool DoneInit = false;
    public bool isAuthenticated = false;

    public void Activate(Action<bool> callback)
    {
        //Debug.Log("Authenticate - Activate");
        //Debug.Log("isAuthenticated " + isAuthenticated);
        //Debug.Log("mAuthenticating " + mAuthenticating);
        if (isAuthenticated || mAuthenticating)
        {
            Debug.Log(" Authenticate - Ignoring repeated call to Authenticate().");

            string mess = "Logging in...";
            if (isAuthenticated)
            {
                mess = "Login Success!";
            }

            //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData(mess, false, "ok", () =>
            //{
            //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
            //GameController.Instance.ShowPopupConfirm(() =>
            //{

            callback?.Invoke(true);
            //});

            //}));

            DoneInit = true;
            return;
        }
        mAuthenticating = true;

        try
        {
#if UNITY_ANDROID
            PlayGamesPlatform.DebugLogEnabled = true;
            // Activate the Play Games platform. This will make it the default
            // implementation of Social.Active
            PlayGamesPlatform.Activate();
            Debug.Log(" Social.localUser.id =>> " + Social.localUser.userName);
            Social.localUser.Authenticate((bool success, string error) =>
            {
                mAuthenticating = false;
                Debug.LogWarning("userID " + userID);
                Debug.LogWarning("user Name " + Social.localUser.userName);
                if (success)
                {
                    Debug.Log("Authenticate - successful : " + Social.localUser.id + "USER NAME: " + Social.localUser.userName);
                    userID = Social.localUser.id;
                    isAuthenticated = true;
                    callback?.Invoke(true);
                }
                else
                {
                    Debug.Log("Authenticate - false : " + error);
                    //GameController.Instance.ShowPopupNotification("No internet connection!", false, null);
#if UNITY_EDITOR
                    userID = "UNITY_EDITOR_GOOGLE" + SystemInfo.deviceUniqueIdentifier;
                    isAuthenticated = true;
                    callback?.Invoke(true);
#else
                    userID = "";
                    callback?.Invoke(false);
#endif
                }
                DoneInit = true;
            });
#elif UNITY_IOS
            GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
            Social.localUser.Authenticate((bool success, string error) =>
            {
                mAuthenticating = false;
                if (success)
                {
                    Debug.Log("Authenticate - successful : " + Social.localUser.id);
                    userID = Social.localUser.id;
                    isAuthenticated = true;
                    callback?.Invoke(true);
                }
                else
                {
                    Debug.Log("Authenticate - false : " + error);
                    userID = "";
                    callback?.Invoke(false);
                }
                DoneInit = true;
            });
#endif
        }
        catch
        {
            Debug.Log("Authenticate - false catch ");
            userID = "";
            DoneInit = true;
        }
    }


    public void ShowLeaderboard()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("No internet connection!", false, "ok", () =>
            //{
            //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
            //}));
        }
        else
        {
            if (isAuthenticated)
            {
                Debug.Log("Leaderboard show");
#if UNITY_ANDROID
                Social.ShowLeaderboardUI();
#elif UNITY_IOS
                GameCenterPlatform.ShowLeaderboardUI("01", TimeScope.AllTime);
#endif
            }
            else
            {
#if UNITY_ANDROID
                string mes = "Login Google Play Game to view Leaderboard";
#elif UNITY_IOS
                string mes = "Login Game Center to view Leaderboard";
#endif

                //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData(mes, true, "login", () =>
                //{
                //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
                //    Activate(callback);
                //}));

                void callback(bool islogin)
                {
                    if (islogin)
                    {
                        //UserProfileManager.Instance.SyncUserProfile(false);
                        StartCoroutine(Delay(0.2f, () =>
                        {
#if UNITY_ANDROID
                            Social.ShowLeaderboardUI();
#elif UNITY_IOS
                            GameCenterPlatform.ShowLeaderboardUI("01", TimeScope.AllTime);
#endif
                        }));
                    }
                    else
                    {
                        //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("Login Failed!", false, "ok", () =>
                        //{
                        //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
                        //}));
                    }
                }
            }
        }
    }

    IEnumerator Delay(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        yield return null;

        action?.Invoke();
    }

    public void PostLeaderboard(int score)
    {
        Debug.Log("Leaderboard Call : " + isAuthenticated);
#if !USE_CHEAT_UNLOCK_LEVELS
        if (isAuthenticated)
        {
#if UNITY_ANDROID
            Social.ReportScore(score, "CgkIrYW_2uYfEAIQBQ", (bool success) =>
            {
                Debug.Log("Leaderboard post : " + success);
            });
#elif UNITY_IOS
            Social.ReportScore(score,"01", (bool success) =>
            {
                Debug.Log("Leaderboard post : " + success);
            });
#endif
        }
#endif
    }
}
