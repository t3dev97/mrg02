﻿using Antada.Utils;
using Firebase.Analytics;
using Firebase.Extensions;
using MEC;
using System;
using System.Collections;
using UnityEngine;

public class TrackingLog : SingletonMonoAwake<TrackingLog>
{
   
        #region Var
        public bool firebaseReady = false;
        public bool DoneInit = false;
        public TrackingHelper trackingHelper;

        public GameScreen CurrentGameScreen = GameScreen.loading;
        public string CurrentGameScreenName
        {
            get
            {
                return getScreenNameByEnum(CurrentGameScreen);
            }
        }

        public string getScreenNameByEnum(GameScreen _gameScreen)
        {
            return _gameScreen.ToString().ToLower();
        }

        #endregion

        #region Init

        private void Start()
        {
            UpdateFirebaseDependencies();
        }

        private void UpdateFirebaseDependencies()
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    firebaseReady = true;
                    FirebaseAnalytics.SetUserId(SystemInfo.deviceUniqueIdentifier);
                }
                else
                {
                     //Firebase Unity SDK is not safe to use here.
                    firebaseReady = false;
                }
                StartCoroutine(Delay(0.5f, () => { DoneInit = true; }));

            });
        }

        IEnumerator Delay(float time, Action action)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }

        #endregion

        #region SendEvent

        public void TrackFTUE(string evt)
        {
            StartCoroutine(TrackFTUEIEnumerator(evt));
        }

        IEnumerator TrackFTUEIEnumerator(string evt)
        {
            if (!trackingHelper.DoneInit)
            {
                yield return new WaitUntil(() => trackingHelper.DoneInit);
            }

            if (trackingHelper.userJourneySum.session > 1) //only track in first session
            {
                yield break;
            }

            if(PlayerPrefs.GetInt(evt,0) == 0)
            {
                PlayerPrefs.SetInt(evt, 1);
                SendEvent(evt);
            }


        }

        public void SendEventWithSessionInfo(string evt, params string[] param)
        {
            StartCoroutine(SendEventDelay(evt, param));
        }

        IEnumerator SendEventDelay(string evt, params string[] param)
        {
            if (!trackingHelper.DoneInit)
            {
                yield return new WaitUntil(() => trackingHelper.DoneInit);
            }
            int upperBound = param.Length;

            Array.Resize(ref param, param.Length + 8);

            param[upperBound] = "session";
            param[upperBound + 1] = trackingHelper.sessionCount.ToString();
            upperBound += 2;

            param[upperBound] = "session_time";
            param[upperBound + 1] = trackingHelper.session_time.ToString();
            upperBound += 2;

            param[upperBound] = "game_screen";
            param[upperBound + 1] = CurrentGameScreenName;
            upperBound += 2;

            param[upperBound] = "game_time";
            param[upperBound + 1] = trackingHelper.game_time;
            upperBound += 2;

            SendEvent(evt, param);
        }

        public void SendEvent(string evt, params string[] param)
        {
            StartCoroutine(sendFirebaseEvent(evt, param));
        }

        private IEnumerator sendFirebaseEvent(string evt, params string[] param)
        {
            if (!DoneInit)
            {
                yield return new WaitUntil(() => DoneInit);
                yield return null;
            }

            if (firebaseReady)
            {
                var pars = new Parameter[param.Length / 2];
                string pr = string.Empty;
                for (int i = 1; i < param.Length; i += 2)
                {
                    pars[i / 2] = new Parameter(param[i - 1], param[i]);
    #if FIREBASE_DEBUG
                    pr += string.Format("   \'{0}\' -> \'{1}\'   ", param[i - 1], param[i]);
    #endif

                }
    #if USE_FIREBASE
                FirebaseAnalytics.LogEvent(evt, pars);
    #endif

    #if FIREBASE_DEBUG
                Debug.Log("MRG01 Firebase Event: " + evt + " | " + pr);
    #endif

            }

        }
        #endregion

        #region Track game screen

        public void OnChangeGameScreen(GameScreen gamescreen)
        {
            CurrentGameScreen = gamescreen;
        }
    #endregion
}

public enum GameScreen
{
    loading,
    main_menu,
    action_phase,
    achievement,
    credits,
    gift,
    language,
    leaderboard,
    revive,
    pause,
    result,
    settings,
    skin,
    none,
}