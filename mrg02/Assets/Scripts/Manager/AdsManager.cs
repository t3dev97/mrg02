﻿using MEC;
using System;
using System.Collections;
using UnityEngine;

public class AdsManager : SingletonMonoAwake<AdsManager>
{
    #region Init
    //private string APP_KEY_ANDROID = "b771e7cd"; -> old
    private string APP_KEY_ANDROID = "dacb0de1";
    private string APP_KEY_IOS = "dacb0de1";

    public static float intervalAd = 30;
    public static int numOfDead = 0;
    public override void OnAwake()
    {
        base.OnAwake();
#if UNITY_ANDROID
        IronSource.Agent.init(APP_KEY_ANDROID, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.BANNER);
#else
            IronSource.Agent.init(APP_KEY_IOS, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL,IronSourceAdUnits.BANNER);
#endif
        IronSource.Agent.validateIntegration();

        IronSource.Agent.shouldTrackNetworkState(true);
    }

    private void Start()
    {
        StartCoroutine(Delay(1f, Init));
    }

    void Init()
    {
        //initBanner();

        initVideo();

        initInterstitial();
    }

    IEnumerator Delay(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }

    #endregion

    #region Video
    private void initVideo()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

        TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_request++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo00_request", "ad_reward", "none");
    }

    private Action RewardCallback;
    private Action NoRewardCallback;
    private Action Errorcallback;

    private bool rewardedVideoAvailability;
    private bool isReward = false;
    private string reward = "";
    private DateTime videoAd_ShowTime;
    private string Reward_game_level;
    private string Reward_game_progression;
    public void ShowRewardedVideo(Action _Rewardcallback, Action _errorcallback, Action _noRewardcallback, string _reward, string _game_level, string _game_progression)
    {
        RewardCallback = _Rewardcallback;
        NoRewardCallback = _noRewardcallback;
        Errorcallback = _errorcallback;
        isReward = false;
        reward = _reward;
        Reward_game_level = _game_progression;
        Reward_game_progression = _game_level;
        videoAd_ShowTime = DateTime.Now;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo04_request_show", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression);
        

#if UNITY_EDITOR
        RewardCallback?.Invoke();
        return;
#endif
        //if(IAPManager.Instance.isOwnedNo_ads)
        //{
        //    TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo06_show_failed", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression, "description", "owned_no_ads");
        //    RewardCallback?.Invoke();
        //    return;
        //}

        if (rewardedVideoAvailability) // show Video if ready
        {
            IronSource.Agent.showRewardedVideo();
        }
        else
        {
            TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_show_failed++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo06_show_failed", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression, "description", "not_available");
            //try to show interstitial instead of rewarded video
            if (IronSource.Agent.isInterstitialReady())
            {
                ShowInterstitial(RewardCallback, Errorcallback, Reward_game_level, Reward_game_progression, reward);
            }
            else
            {
                TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_show_failed++;
                TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial06_show_failed", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression, "description", "not_available");

                Errorcallback?.Invoke();
            }
        }
    }

    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy 
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_show++;
        //AppsFlyer.trackRichEvent("ad_watched", new System.Collections.Generic.Dictionary<string, string>());
        TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo05_show", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression);
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
        TimeSpan ts = DateTime.Now - videoAd_ShowTime;
        if (isReward)
        {
            TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_complete++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo09_complete", "ad_reward", reward, "ad_duration", ((int)ts.TotalSeconds).ToString(), "game_level", Reward_game_level, "game_progression", Reward_game_progression);
            RewardCallback?.Invoke();
        }
        else
        {
            TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_close++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo07_close", "ad_reward", reward, "ad_duration", ((int)ts.TotalSeconds).ToString(), "game_level", Reward_game_level, "game_progression", Reward_game_progression);
            NoRewardCallback?.Invoke();
        }
    }
    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available. 
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        //Change the in-app 'Traffic Driver' state according to availability.
        rewardedVideoAvailability = available;
        if (available)
        {
            TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_fill++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo02_fill", "ad_reward", "none");
        }
        else
        {
            //TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_load_failed++;
            //TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo03_load_failed", "ad_reward", "none");

            TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_request++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo00_request", "ad_reward", "none");
        }
    }
    //Invoked when the video ad finishes playing.
    void RewardedVideoAdEndedEvent()
    {
    }
    //Invoked when the user completed the video and should be rewarded. 
    //If using server-to-server callbacks you may ignore this events and wait for the callback from the  ironSource server.
    //
    //@param - placement - placement object which contains the reward data
    //
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        isReward = true;
    }
    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_rewardedvideo_show_failed++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo06_show_failed", "ad_reward", reward, "game_level", Reward_game_level, "game_progression", Reward_game_progression, "description", "show_error");
        Debug.Log("RewardedVideoAdShowFailedEvent : error" + error);
        Errorcallback?.Invoke();
    }

    void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
    {
        TimeSpan ts = DateTime.Now - videoAd_ShowTime;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_rewardedvideo08_click", "ad_reward", reward, "ad_duration", ((int)ts.TotalSeconds).ToString(), "game_level", Reward_game_level, "game_progression", Reward_game_progression);
    }
    #endregion

    #region Intertitial
    private DateTime interstitial_ShowTime;
    private DateTime interstitial_DownloadTime;
    private Action InterstitialDoneCallback;
    private Action InterstitialErrorCallback;
    private string Interstitial_game_level;
    private string Interstitial_game_progression;
    private string Interstitial_reward;

    public void CountDowntInterstitialAds()
    {
        StartCoroutine(Delay(30, () => { intervalAd = 30; }));
    }
    public void GameplayShowInterstitial(Action _InterstitialDoneCallback, Action _InterstitialErrorCallback, string _Interstitial_game_level, string _Interstitial_game_progression, string _Interstitial_reward)
    {
        if (intervalAd <= 0)
        {
            if (IronSource.Agent.isInterstitialReady())
            {
                numOfDead = 0;
                intervalAd = 30;
                ShowInterstitial(_InterstitialDoneCallback, _InterstitialErrorCallback, _Interstitial_game_level, _Interstitial_game_progression, _Interstitial_reward);
            }
            else
            {
                _InterstitialErrorCallback?.Invoke();
                TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_show_failed++;
                TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial06_show_failed", "ad_reward", _Interstitial_reward, "game_level", _Interstitial_game_level, "game_progression", _Interstitial_game_progression, "description", "not_available");
            }
        }
        else
        {
            _InterstitialErrorCallback?.Invoke();
        }
    }


    public void ShowInterstitial(Action _InterstitialDoneCallback, Action _InterstitialErrorCallback, string _Interstitial_game_level, string _Interstitial_game_progression, string _Interstitial_reward)
    {
        InterstitialDoneCallback = _InterstitialDoneCallback;
        InterstitialErrorCallback = _InterstitialErrorCallback;
        Interstitial_game_level = _Interstitial_game_level;
        Interstitial_game_progression = _Interstitial_game_progression;
        Interstitial_reward = _Interstitial_reward;

        //if (IAPManager.Instance.isOwnedNo_ads)
        //{
        //    TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial06_show_failed", "ad_reward", Interstitial_reward, "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression, "description", "owned_no_ads");
        //    InterstitialDoneCallback?.Invoke();
        //    return;
        //}

        if (IronSource.Agent.isInterstitialReady())
        {
            interstitial_ShowTime = DateTime.Now;
            IronSource.Agent.showInterstitial();
        }
        else
        {
            TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_show_failed++;
            TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial06_show_failed", "ad_reward", Interstitial_reward, "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression, "description", "not_available");

            InterstitialErrorCallback?.Invoke();
        }
    }

    void initInterstitial()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

        loadInterstitial();
    }
    void loadInterstitial()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_request++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial00_request", "ad_reward", "none");
        IronSource.Agent.loadInterstitial();
        interstitial_DownloadTime = DateTime.Now;
    }
    //Invoked when the initialization process has failed.
    //@param description - string - contains information about the failure.
    void InterstitialAdLoadFailedEvent(IronSourceError error)
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_load_failed++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial03_load_failed", "ad_reward", "none");
        Timing.CallDelayed(5, loadInterstitial);
    }
    //Invoked right before the Interstitial screen is about to open.
    void InterstitialAdShowSucceededEvent()
    {
        //AppsFlyer.trackRichEvent("ad_watched", new System.Collections.Generic.Dictionary<string, string>());
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_show++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial05_show", "ad_reward", Interstitial_reward, "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression);
    }
    //Invoked when the ad fails to show.
    //@param description - string - contains information about the failure.
    void InterstitialAdShowFailedEvent(IronSourceError error)
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_show_failed++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial06_show_failed", "ad_reward", Interstitial_reward, "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression, "description", "show_error");

        InterstitialErrorCallback?.Invoke();

        Timing.CallDelayed(1, loadInterstitial);
    }
    // Invoked when end user clicked on the interstitial ad
    void InterstitialAdClickedEvent()
    {
        TimeSpan ts = DateTime.Now - interstitial_ShowTime;
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_click++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial08_click", "ad_reward", Interstitial_reward, "ad_duration", ((int)ts.TotalSeconds).ToString(), "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression);
    }
    //Invoked when the interstitial ad closed and the user goes back to the application screen.
    void InterstitialAdClosedEvent()
    {
        Debug.Log("InterstitialAdClosedEvent");
        TimeSpan ts = DateTime.Now - interstitial_ShowTime;
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_close++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial07_close", "ad_reward", Interstitial_reward, "ad_duration", ((int)ts.TotalSeconds).ToString(), "game_level", Interstitial_game_level, "game_progression", Interstitial_game_progression);

        InterstitialDoneCallback?.Invoke();

        Timing.CallDelayed(1, loadInterstitial);
    }
    //Invoked when the Interstitial is Ready to shown after load function is called
    void InterstitialAdReadyEvent()
    {
        TimeSpan ts = DateTime.Now - interstitial_DownloadTime;
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_interstitial_fill++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_interstitial02_fill", "ad_reward", "none", "ad_download", ((int)ts.TotalSeconds).ToString());
    }
    //Invoked when the Interstitial Ad Unit has opened
    void InterstitialAdOpenedEvent()
    {
    }

    #endregion

    #region Banner
    private string Banner_game_level;
    private string Banner_game_progression;
    private string Banner_reward;

    public void initBanner()
    {
        IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
        IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
        IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent;
        IronSourceEvents.onBannerAdScreenPresentedEvent += BannerAdScreenPresentedEvent;
        IronSourceEvents.onBannerAdScreenDismissedEvent += BannerAdScreenDismissedEvent;
        IronSourceEvents.onBannerAdLeftApplicationEvent += BannerAdLeftApplicationEvent;
        RequestBanner();
    }

    private void RequestBanner()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_request++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner00_request", "ad_reward", "none");
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }

    public bool ShowBanner(string _Banner_game_level, string _Banner_game_progression, string _Banner_reward)
    {
        Banner_game_level = _Banner_game_level;
        Banner_game_progression = _Banner_game_progression;
        Banner_reward = _Banner_reward;

        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_show++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner05_show", "ad_reward", Banner_reward, "game_level", Banner_game_level, "game_progression", Banner_game_progression);
        IronSource.Agent.displayBanner();
        return true;
    }

    public bool HideBanner()
    {
        IronSource.Agent.hideBanner();
        return true;
    }
    public bool DestroyBanner()
    {
        IronSource.Agent.destroyBanner();
        return true;
    }
    //Invoked once the banner has loaded
    void BannerAdLoadedEvent()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_fill++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner02_fill", "ad_reward", "none");
    }
    //Invoked when the banner loading process has failed.
    //@param description - string - contains information about the failure.
    void BannerAdLoadFailedEvent(IronSourceError error)
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_load_failed++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner03_load_failed", "ad_reward", "none");
        Debug.Log("BannerAdLoadFailedEvent: " + error);
        Timing.CallDelayed(5, RequestBanner);
    }
    // Invoked when end user clicks on the banner ad
    void BannerAdClickedEvent()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_click++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner08_click", "ad_reward", Banner_reward, "game_level", Banner_game_level, "game_progression", Banner_game_progression);
    }
    //Notifies the presentation of a full screen content following user click
    void BannerAdScreenPresentedEvent()
    {
    }
    //Notifies the presented screen has been dismissed
    void BannerAdScreenDismissedEvent()
    {
        TrackingLog.Instance.trackingHelper.userJourneySum.ads_banner_close++;
        TrackingLog.Instance.SendEventWithSessionInfo("ads_banner07_close", "ad_reward", Banner_reward, "game_level", Banner_game_level, "game_progression", Banner_game_progression);
    }
    //Invoked when the user leaves the app
    void BannerAdLeftApplicationEvent()
    {
    }

    #endregion

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }

    private void Update()
    {
        if (IAPManager.Instance.isOwnedVip_ads == false && IAPManager.Instance.isOwnedNo_ads == false)
            if (intervalAd >= 0)
                intervalAd -= Time.deltaTime;
    }
}
