﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetBundles;
using System.Linq;
using System;

public class MRGGameManager : SingletonMonoStart<MRGGameManager>
{
    public static Action<bool> onAssetbundleLoaded;

    private AssetBundleManager abm;
    private int mDownloadedCount = 0;
    private bool isLoadedAll = false;

    private string assetbundleUrl = "";
    private string assetbundleVersion = "0.0.5/";
    

    private void Awake()
    {
        assetbundleUrl = "https://antada-musicrhythmgames.s3.amazonaws.com/MRG02/" + assetbundleVersion + "AssetBundles/";
    }

    public override void OnStart()
    {
        base.OnStart();
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#elif DEBUG_LOG
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = false;
#endif

#if SG_DEBUG_LOG
        SRDebug.Init();
#endif

        InitAssetBundleManager();
    }


    #region Assetbundle
    public AssetBundleManager GetABM()
    {
        return abm;
    }

    public void InitAssetBundleManager()
    {
#if USE_ASSETBUNDLE
        Debug.Log("Init Assetbundle");
        abm = new AssetBundleManager();
        abm.SetBaseUri(assetbundleUrl);
        //abm.SetPrioritizationStrategy(AssetBundleManager.PrioritizationStrategy.PrioritizeStreamingAssets);
        abm.Initialize(OnAssetBundleManagerInitialized);
#endif
    }

    private void OnAssetBundleManagerInitialized(bool success)
    {
        Debug.LogError("OnAssetBundleManagerInitialized: " + success);
        onAssetbundleLoaded?.Invoke(success);
    }
    
    #endregion
}
