﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Purchasing;
public class IAPManager : SingletonMonoAwake<IAPManager>, IStoreListener
{
    public static IStoreController m_StoreController;
    public static IExtensionProvider m_StoreIExtensionProviderr;
    [SerializeField]
    private string NO_ADS = "com.antada.justlinesandbeats.noads";//com.antada.gamingbeats.justtilesandbeats.vip1
    [SerializeField]
    private string VIP_GOLD = "com.antada.justlinesandbeats.vipgoldpack";
    public bool isOwnedNo_ads;
    public bool isOwnedVip_ads;

    public bool isDoneInit;
    public bool isInit;

    private Action<bool> BuyNo_adsSuccessCallback;
    private Action<bool> BuyVip_adsSuccessCallback;
    // Start is called before the first frame update
    void Start()
    {

        if (m_StoreController == null)
        {
            InitIAP();
        }
    }

    private void InitIAP()
    {
        if (isInited())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(NO_ADS, ProductType.NonConsumable);
        builder.AddProduct(VIP_GOLD, ProductType.Subscription);

        UnityPurchasing.Initialize(this, builder);
    }

    public bool isInited()
    {
        return m_StoreController != null && m_StoreIExtensionProviderr != null;
    }

    public void BuyNo_ads(Action<bool> _BuyNo_adsSuccessCallback)
    {
        BuyNo_adsSuccessCallback = _BuyNo_adsSuccessCallback;
        BuyProductID(NO_ADS);
    }
    public void BuyVipGold(Action<bool> _BuyVip_adsSuccessCallback)
    {
        BuyVip_adsSuccessCallback = _BuyVip_adsSuccessCallback;
        BuyProductID(VIP_GOLD);
    }
    void BuyProductID(string productID)
    {
        if (isInited())
        {
            Product product = m_StoreController.products.WithID(productID);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log("IAPManager BuyProductID Purchasing");

                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("IAPManager BuyProductID Fail. not found or not available");
                Debug.Log("==>> productID: " + productID + "//" + (product != null));
            }
        }
        else
        {
            Debug.Log("IAPManager BuyProductID Fail. not Init");
            Debug.Log(" 2 ==>> productID: " + productID + "//");
        }
    }

    public void RestorePurchases(Action<bool> callback = null)
    {
#if UNITY_EDITOR
        callback?.Invoke(true);
#endif
        if (!isInited())
        {
            return;
        }
#if UNITY_IOS
        Debug.Log("IAPManager RestorePurchases started...");
        var apple = m_StoreIExtensionProviderr.GetExtension<IAppleExtensions>();
        apple.RestoreTransactions((result) => {
            Debug.Log("IAPManager RestorePurchases continuing: " + result);
            isOwnedNo_ads = checkOwnedProduct(NO_ADS);
            if (isOwnedNo_ads)
            {
                UserProfileManager.Instance.UnlockAllRewardedSkin();
            }
            callback?.Invoke(isOwnedNo_ads);
        });
#else
        Debug.Log("IAPManager RestorePurchases FAIL Not support on this platform");
#endif
    }

    public string GetProducePriceFromStore(string id)
    {
        if (isInited())
        {
            return m_StoreController.products.WithID(id).metadata.localizedPriceString;
        }
        else
        {
            return "";
        }
    }

    public bool checkOwnedProduct(string id)
    {
        Product cproduct = m_StoreController.products.WithID(id);

        if (cproduct != null && cproduct.hasReceipt)
        {
            Debug.Log("IAPManager checkOwnedProduct true: " + id);
            return true;
        }
        else
        {
            Debug.Log("IAPManager checkOwnedProduct false: " + id);
            return false;
        }
    }



    void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("IAPManager OnInitialized");

        m_StoreController = controller;
        m_StoreIExtensionProviderr = extensions;

        isOwnedNo_ads = checkOwnedProduct(NO_ADS);
        if (isOwnedNo_ads)
        {
            UserProfileManager.Instance.UnlockNoAds();
        }

        isOwnedVip_ads = checkOwnedProduct(VIP_GOLD);
        if (isOwnedVip_ads)
        {
            UserProfileManager.Instance.UnlockVipGold();
        }
        isInit = true;
        isDoneInit = true;
    }

    void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("IAPManager OnInitializeFailed " + error);
        //if (isOwnedVip_ads == false && isOwnedNo_ads == false)
        //{
        //    AdsManager.Instance.ShowBanner("Screen Banner", "none", "none");
        //}
        isDoneInit = true;
    }

    void IStoreListener.OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        Debug.Log("IAPManager OnPurchaseFailed " + i.definition.id + " ---- " + p);
        BuyNo_adsSuccessCallback?.Invoke(false);
        BuyVip_adsSuccessCallback?.Invoke(false);
    }

    PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
    {

        if (String.Equals(e.purchasedProduct.definition.id, NO_ADS, StringComparison.Ordinal))
        {
            Debug.Log("IAPManager ProcessPurchase " + e.purchasedProduct.definition.id);

            isOwnedNo_ads = true;
            UserProfileManager.Instance.UnlockNoAds();

            BuyNo_adsSuccessCallback?.Invoke(true);
        }
        Debug.Log("e.purchasedProduct.definition.id ===>" + e.purchasedProduct.definition.id);
        if (String.Equals(e.purchasedProduct.definition.id, VIP_GOLD, StringComparison.Ordinal))
        {
            Debug.Log("IAPManager ProcessPurchase " + e.purchasedProduct.definition.id);

            isOwnedVip_ads = true;
            UserProfileManager.Instance.UnlockVipGold();

            BuyVip_adsSuccessCallback?.Invoke(true);
        }
        return PurchaseProcessingResult.Complete;

    }
    private void OnDestroy()
    {
        DOTween.Kill(this);
    }
}
