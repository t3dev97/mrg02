﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class TrackingHelper : MonoBehaviour
{
    #region Var

    bool doneCaculateSessionTime = false;
    bool doneGetGameTime = false;
    public bool DoneInit
    {
        get { return doneCaculateSessionTime && doneGetGameTime; }
    }

    private float session_time_float;
    public int session_time
    {
        get { return (int)session_time_float; }
    }

    public int sessionCount;

    public string game_time;

    public UserJourneySum userJourneySum;
    public bool doneInit;

    private string TempFilePath;

    #endregion

    #region Init
    private void Awake()
    {
        loadData();
        getGameTime();
    }
    private void Start()
    {
        StartCoroutine(Delay(1f, () =>
        {
            CaculateSessionTime(false);
            UserProfileManager.Instance.SyncUserProfile(true);
        }));
    }

    IEnumerator Delay(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }

    private void getGameTime()
    {
        DateTime now = DateTime.Now;

        if (now.Minute >= 30)
        {
            now = now.AddHours(1);
        }

        game_time = now.ToString("HH");

        doneGetGameTime = true;
    }

    private void CaculateSessionTime(bool isCallOnResume)
    {
        DateTime now = DateTime.Now;

        //caculate session time in day
        //string lastSessionDay = PlayerPrefs.GetString("lastSessionDay", (new DateTime()).ToString());
        //DateTime lastSessionDayDate = DateTime.Parse(lastSessionDay);

        //if (lastSessionDayDate.IsSameDate(now))
        //{
        //    sessionCount = PlayerPrefs.GetInt("sessionCount", 1);
        //    string lastSessionPauseTime = PlayerPrefs.GetString("lastSessionPauseTime", "null");
        //    if (lastSessionPauseTime != "null")
        //    {
        //        DateTime lastSessionPauseTimeDate = DateTime.Parse(lastSessionPauseTime);
        //        TimeSpan ts = now - lastSessionPauseTimeDate;
        //        //Debug.Log("CaculateSessionTime : " + ts.TotalMinutes);
        //        if (ts.TotalMinutes >= 30)
        //        {
        //            sessionCount++;
        //            userJourneySum.session++;
        //            trackUserJourney(isCallOnResume);
        //        }
        //    }
        //}
        //else
        //{
        //    // start New day
        //    sessionCount = 1;
        //    userJourneySum.session++;
        //    trackUserJourney(isCallOnResume);
        //}

        PlayerPrefs.SetString("lastSessionDay", now.ToString());
        PlayerPrefs.SetInt("sessionCount", sessionCount);

        doneCaculateSessionTime = true;
    }

    private void saveData()
    {
        if (TempFilePath != null)
        {
            string json = JsonUtility.ToJson(userJourneySum);

            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                newTask.WriteLine(json);
                newTask.Close();
            }
        }
    }

    private void loadData()
    {
        TempFilePath = Application.persistentDataPath + "/TrackingUserJourney.data";
        Debug.Log("loadData  " + TempFilePath);
        if (!File.Exists(TempFilePath))//make a file if not exists
        {
            string json = JsonUtility.ToJson(userJourneySum);

            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                newTask.WriteLine(json);
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(TempFilePath);
            string str = reader.ReadLine();
            reader.Close();
            UserJourneySum _userJourneySum = new UserJourneySum();

            JsonUtility.FromJsonOverwrite(str, _userJourneySum);

            if (_userJourneySum.version != userJourneySum.version) // make new file if this is new ver
            {
                string json = JsonUtility.ToJson(userJourneySum);

                using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
                {
                    newTask.WriteLine(json);
                    newTask.Close();
                }
            }
            else
            {
                JsonUtility.FromJsonOverwrite(str, userJourneySum);
            }
        }

        doneInit = true;
    }

    #endregion

    #region logic
    void Update()
    {
        if (doneInit)
        {
            session_time_float += Time.deltaTime;
            userJourneySum.session_time_sum += Time.deltaTime;
        }
    }

    private void trackUserJourney(bool isCallOnResume)
    {
        StartCoroutine(trackUserJourneyIEnumerator(isCallOnResume));
    }

    IEnumerator trackUserJourneyIEnumerator(bool isCallOnResume)
    {
        yield return null;

        yield return new WaitUntil(() => UserProfileManager.Instance.DoneInit && FacebookManager.Instance.DoneInit && GooglePlayGameManager.Instance.DoneInit);
        if (isCallOnResume)
        {
            UserProfileManager.Instance.SyncUserProfile(true);
        }
        Debug.Log("trackUserJourney");

        int session_time_average = (int)(userJourneySum.session_time_sum / userJourneySum.session);

        int gameplay_own_skin = UserProfileManager.Instance.userProfileLocal.SkinUnlockedId.Count;
        int gameplay_own_star = UserProfileManager.Instance.userProfileLocal.TotalStar;

        //var configSongItems = AvConfigManager.configSongs.GetAllItem().OrderBy(x => x.Sort).ToList();
        //var dictSongStatus = SaveDataManager.Instance.LoadDictSongStatus();
        //int gameplay_current_level = -1;

        //for (int i = dictSongStatus.Count - 1; i >= 0; i--)
        //{
        //    if (dictSongStatus[i].isUnLock)
        //    {
        //        gameplay_current_level = dictSongStatus[i].Order;
        //        break;
        //    }
        //}

        TrackingLog.Instance.SendEvent("user_journey"
            , "did", SystemInfo.deviceUniqueIdentifier
            , "u_facebook_id", FacebookManager.Instance.UserID
            , "u_gamecenter_id", GooglePlayGameManager.Instance.userID
            , "session", userJourneySum.session.ToString()
            , "session_time_sum", ((int)userJourneySum.session_time_sum).ToString()
            , "session_time_average", session_time_average.ToString()
            , "gameplay_duration", ((int)userJourneySum.gameplay_duration).ToString()

            , "gameplay_stage_win", userJourneySum.gameplay_stage_win.ToString()
            , "gameplay_stage_lose", userJourneySum.gameplay_stage_lose.ToString()
            , "gameplay_stage_giveup", userJourneySum.gameplay_stage_giveup.ToString()
            , "gameplay_stage_start", userJourneySum.gameplay_stage_start.ToString()
            , "gameplay_stage_replay", userJourneySum.gameplay_stage_replay.ToString()
            , "gameplay_stage_revive", userJourneySum.gameplay_stage_revive.ToString()
            , "gameplay_own_skin", gameplay_own_skin.ToString()
            , "gameplay_own_star", gameplay_own_star.ToString()
            //, "gameplay_current_level", gameplay_current_level.ToString()
            ); ;

        yield return null;
        user_journey_ad(session_time_average);
        yield return null;
        user_journey_ad_rewardedvideo(session_time_average);
        yield return null;
        user_journey_ad_interstitial(session_time_average);
        yield return null;
        user_journey_ad_banner(session_time_average);
    }

    private void user_journey_ad(int session_time_average)
    {
        TrackingLog.Instance.SendEvent("user_journey_ad"
            , "did", SystemInfo.deviceUniqueIdentifier
            , "u_facebook_id", FacebookManager.Instance.UserID
            , "u_gamecenter_id", GooglePlayGameManager.Instance.userID
            , "session", userJourneySum.session.ToString()
            , "session_time_sum", ((int)userJourneySum.session_time_sum).ToString()
            , "session_time_average", session_time_average.ToString()

            , "ads_total_request", userJourneySum.ads_total_request().ToString()
            , "ads_total_fill", userJourneySum.ads_total_fill().ToString()
            , "ads_total_show", userJourneySum.ads_total_show().ToString()
            , "ads_total_show_failed", userJourneySum.ads_total_show_failed().ToString()
            , "ads_total_load_failed", userJourneySum.ads_total_load_failed().ToString()
            , "ads_total_close", userJourneySum.ads_total_close().ToString()
            , "ads_total_complete", userJourneySum.ads_total_complete().ToString()
            , "ads_total_click", userJourneySum.ads_total_click().ToString()

            , "ads_rewardedvideo_show", userJourneySum.ads_rewardedvideo_show.ToString()
            , "ads_interstitial_show", userJourneySum.ads_interstitial_show.ToString()
            , "ads_banner_show", userJourneySum.ads_banner_show.ToString()

            , "ads_rewardedvideo_click", userJourneySum.ads_rewardedvideo_click.ToString()
            , "ads_interstitial_click", userJourneySum.ads_interstitial_click.ToString()
            , "ads_banner_click", userJourneySum.ads_banner_click.ToString()
            );
    }

    private void user_journey_ad_rewardedvideo(int session_time_average)
    {
        TrackingLog.Instance.SendEvent("user_journey_ad_rewardedvideo"
            , "did", SystemInfo.deviceUniqueIdentifier
            , "u_facebook_id", FacebookManager.Instance.UserID
            , "u_gamecenter_id", GooglePlayGameManager.Instance.userID
            , "session", userJourneySum.session.ToString()
            , "session_time_sum", ((int)userJourneySum.session_time_sum).ToString()
            , "session_time_average", session_time_average.ToString()

            , "ads_rewardedvideo_request", userJourneySum.ads_rewardedvideo_request.ToString()
            , "ads_rewardedvideo_fill", userJourneySum.ads_rewardedvideo_fill.ToString()
            , "ads_rewardedvideo_show", userJourneySum.ads_rewardedvideo_show.ToString()
            , "ads_rewardedvideo_show_failed", userJourneySum.ads_rewardedvideo_show_failed.ToString()
            , "ads_rewardedvideo_load_failed", userJourneySum.ads_rewardedvideo_load_failed.ToString()
            , "ads_rewardedvideo_close", userJourneySum.ads_rewardedvideo_close.ToString()
            , "ads_rewardedvideo_complete", userJourneySum.ads_rewardedvideo_complete.ToString()
            , "ads_rewardedvideo_click", userJourneySum.ads_rewardedvideo_click.ToString()
            );
    }

    private void user_journey_ad_interstitial(int session_time_average)
    {
        TrackingLog.Instance.SendEvent("user_journey_ad_interstitial"
            , "did", SystemInfo.deviceUniqueIdentifier
            , "u_facebook_id", FacebookManager.Instance.UserID
            , "u_gamecenter_id", GooglePlayGameManager.Instance.userID
            , "session", userJourneySum.session.ToString()
            , "session_time_sum", ((int)userJourneySum.session_time_sum).ToString()
            , "session_time_average", session_time_average.ToString()

            , "ads_interstitial_request", userJourneySum.ads_interstitial_request.ToString()
            , "ads_interstitial_fill", userJourneySum.ads_interstitial_fill.ToString()
            , "ads_interstitial_show", userJourneySum.ads_interstitial_show.ToString()
            , "ads_interstitial_show_failed", userJourneySum.ads_interstitial_show_failed.ToString()
            , "ads_interstitial_load_failed", userJourneySum.ads_interstitial_load_failed.ToString()
            , "ads_interstitial_close", userJourneySum.ads_interstitial_close.ToString()
            , "ads_interstitial_complete", userJourneySum.ads_interstitial_complete.ToString()
            , "ads_interstitial_click", userJourneySum.ads_interstitial_click.ToString()
            );
    }

    private void user_journey_ad_banner(int session_time_average)
    {
        TrackingLog.Instance.SendEvent("user_journey_ad_banner"
            , "did", SystemInfo.deviceUniqueIdentifier
            , "u_facebook_id", FacebookManager.Instance.UserID
            , "u_gamecenter_id", GooglePlayGameManager.Instance.userID
            , "session", userJourneySum.session.ToString()
            , "session_time_sum", ((int)userJourneySum.session_time_sum).ToString()
            , "session_time_average", session_time_average.ToString()

            , "ads_banner_request", userJourneySum.ads_banner_request.ToString()
            , "ads_banner_fill", userJourneySum.ads_banner_fill.ToString()
            , "ads_banner_show", userJourneySum.ads_banner_show.ToString()
            , "ads_banner_show_failed", userJourneySum.ads_banner_show_failed.ToString()
            , "ads_banner_load_failed", userJourneySum.ads_banner_load_failed.ToString()
            , "ads_banner_close", userJourneySum.ads_banner_close.ToString()
            , "ads_banner_complete", userJourneySum.ads_banner_complete.ToString()
            , "ads_banner_click", userJourneySum.ads_banner_click.ToString()
            );
    }
    #endregion

    #region outnit
    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            StartCoroutine(Delay(1f, () => { CaculateSessionTime(true); }));
        }
        else
        {
            PlayerPrefs.SetString("lastSessionPauseTime", DateTime.Now.ToString());
            saveData();
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetString("lastSessionPauseTime", DateTime.Now.ToString());
        saveData();
    }
    #endregion
}



[System.Serializable]
public class UserJourneySum
{
    public int version;

    public int session;
    public float session_time_sum;
    public int gameplay_stage_win;
    public int gameplay_stage_lose;
    public int gameplay_stage_giveup;
    public int gameplay_stage_start;
    public int gameplay_stage_replay;
    public int gameplay_stage_revive;
    public float gameplay_duration;

    public int ads_rewardedvideo_request;
    public int ads_rewardedvideo_fill;
    public int ads_rewardedvideo_show;
    public int ads_rewardedvideo_show_failed;
    public int ads_rewardedvideo_load_failed;
    public int ads_rewardedvideo_close;
    public int ads_rewardedvideo_complete;
    public int ads_rewardedvideo_click;

    public int ads_interstitial_request;
    public int ads_interstitial_fill;
    public int ads_interstitial_show;
    public int ads_interstitial_show_failed;
    public int ads_interstitial_load_failed;
    public int ads_interstitial_close;
    public int ads_interstitial_complete;
    public int ads_interstitial_click;

    public int ads_banner_request;
    public int ads_banner_fill;
    public int ads_banner_show;
    public int ads_banner_show_failed;
    public int ads_banner_load_failed;
    public int ads_banner_close;
    public int ads_banner_complete;
    public int ads_banner_click;

    public int ads_total_request() { return ads_rewardedvideo_request + ads_interstitial_request + ads_banner_request; }
    public int ads_total_fill() { return ads_rewardedvideo_fill + ads_interstitial_fill + ads_banner_fill; }
    public int ads_total_show() { return ads_rewardedvideo_show + ads_interstitial_show + ads_banner_show; }
    public int ads_total_show_failed() { return ads_rewardedvideo_show_failed + ads_interstitial_show_failed + ads_banner_show_failed; }
    public int ads_total_load_failed() { return ads_rewardedvideo_load_failed + ads_interstitial_load_failed + ads_banner_load_failed; }
    public int ads_total_close() { return ads_rewardedvideo_close + ads_interstitial_close + ads_banner_close; }
    public int ads_total_complete() { return ads_rewardedvideo_complete + ads_interstitial_complete + ads_banner_complete; }
    public int ads_total_click() { return ads_rewardedvideo_click + ads_interstitial_click + ads_banner_click; }
}
