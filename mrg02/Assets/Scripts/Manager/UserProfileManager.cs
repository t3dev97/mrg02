﻿using DG.Tweening;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
public class UserProfileManager : SingletonMonoAwake<UserProfileManager>
{
    public UserProfile userProfileLocal;
    private string TempFilePath;

    public bool DoneInit = false;
    private bool DoneAuth = false;
    private bool isAuth = false;

    //private List<ConfigAchievementItem> configAchievementItems;

    private DatabaseReference reference;

    public Action OnSyncUserProfile;
    private bool isRemoveAds = false;
    private bool isVipGold = false;

    public bool IsRemoveAds { get => isRemoveAds; set => isRemoveAds = value; }
    public bool IsVipGold { get => isVipGold; set => isVipGold = value; }

    public override void OnAwake()
    {
        base.OnAwake();

    }

    private void Start()
    {
        TempFilePath = Application.persistentDataPath + "/UserProfile.data";
        print("TempFilePath ==> " + TempFilePath);

        if (!File.Exists(TempFilePath))//make a file if not exists
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                userProfileLocal = new UserProfile();
                newTask.WriteLine(JsonUtility.ToJson(userProfileLocal));
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(TempFilePath);
            JsonUtility.FromJsonOverwrite(reader.ReadLine(), userProfileLocal);
            reader.Close();
        }
        MCache.Instance.UserData.ToUserData(userProfileLocal);
        GameController.Instance.UpdateShowStar(userProfileLocal.TotalStar);
        //init dictSongStatus
        //var configSongItems = AvConfigManager.configSongs.GetAllItem().OrderBy(x => x.Order).ToList();
        //for (int i = 0; i < configSongItems.Count; i++)
        //{
        //    if (!userProfile.dictSongStatus.Exists(x => x.Order.Equals(configSongItems[i].Order)))
        //    {
        //        userProfile.dictSongStatus.Add(new PlayerSongsInfomation(configSongItems[i].Order, 0, configSongItems[i].Order < 1));
        //    }
        //}
#if USE_CHEAT_UNLOCK_LEVELS
        for (int i = 0; i < userProfile.dictSongStatus.Count; i++)
        {
            userProfile.dictSongStatus[i].isUnLock = true;
            userProfile.dictSongStatus[i].star = 3;
        }
        userProfile.UpdateTotalStar();
#endif

        DoneInit = true;

        //configAchievementItems = AvConfigManager.configAchievement.GetAllItem();

        StartCoroutine(DelayStart());
    }

    public void ResetData()
    {
        userProfileLocal.SkinUnlockedId = new List<string>();
        userProfileLocal.AchievementDoneId = new List<string>();
        userProfileLocal.GiftReceivedId = new List<string>();
        userProfileLocal.dictSongStatus = new List<PlayerSongsInfomation>();
        userProfileLocal.TotalStar = 0;

        //var configSongItems = AvConfigManager.configSongs.GetAllItem().OrderBy(x => x.Order).ToList();
        //for (int i = 0; i < configSongItems.Count; i++)
        //{
        //    if (!userProfile.dictSongStatus.Exists(x => x.Order.Equals(configSongItems[i].Order)))
        //    {
        //        userProfile.dictSongStatus.Add(new PlayerSongsInfomation(configSongItems[i].Order, 0, configSongItems[i].Order < 1));
        //    }
        //}

        SaveData();
        if (GooglePlayGameManager.Instance.isAuthenticated)
        {
            PushUserProfile(GooglePlayGameManager.Instance.userID, userProfileLocal);
        }
    }

    private IEnumerator DelayStart()
    {
        if (!TrackingLog.Instance.DoneInit)
        {
            yield return new WaitUntil(() => TrackingLog.Instance.DoneInit);
            yield return null;
        }

        if (!DoneInit)
        {
            yield return new WaitUntil(() => DoneInit);
            yield return null;
        }

        if (!TrackingLog.Instance.firebaseReady)
        {
            yield break;
        }

        InitializeFirebaseDataBase();
        FirebaseAuth auth = FirebaseAuth.DefaultInstance;
        auth.SignInAnonymouslyAsync().ContinueWithOnMainThread(task =>
        {
            DoneAuth = true;
            if (task.IsCanceled)
            {
                return;
            }
            if (task.IsFaulted)
            {
                return;
            }
            FirebaseUser newUser = task.Result;
            isAuth = true;
        });
    }

    public void InitializeFirebaseDataBase()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://mrg02-cfc0e.firebaseio.com/");
        if (FirebaseApp.DefaultInstance.Options.DatabaseUrl != null)
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(FirebaseApp.DefaultInstance.Options.DatabaseUrl);

        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    public void SyncUserProfile(bool isAutoSync)
    {
        StartCoroutine(SyncUserProfileIEnumerator(isAutoSync));
    }

    private IEnumerator SyncUserProfileIEnumerator(bool isAutoSync)
    {
        if (!DoneAuth)
        {
            yield return new WaitUntil(() => DoneAuth);
            yield return null;
        }

        if (!isAuth)
        {
            yield break;
        }

        if (!GooglePlayGameManager.Instance.DoneInit)
        {
            yield return new WaitUntil(() => GooglePlayGameManager.Instance.DoneInit);
            yield return null;
        }

        if (GooglePlayGameManager.Instance.isAuthenticated/* && SaveDataManager.Instance.isCloudSave*/)
        {
            LoadUserProfile(GooglePlayGameManager.Instance.userID, isAutoSync);
        }
    }

    private void LoadUserProfile(string userID, bool isAutoSync)
    {
        FirebaseDatabase.DefaultInstance.GetReference("up/" + userID)
        .GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("LoadUserProfile IEnumerator error ");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                string str = "";
                if (snapshot.ChildrenCount > 0)
                {
                    str = snapshot.Child("data").GetValue(false).ToString();
                }

                LoadUserProfileCallback(userID, str, isAutoSync);
            }
        });
    }

    private void LoadUserProfileCallback(string userID, string _UserProfileStrRemote, bool isAutoSync)
    {

        //print("isAutoSync = " + isAutoSync);
        //Debug.Log("userID ==>" + userID);
        //Debug.Log("_UserProfileStrRemote ==> " + _UserProfileStrRemote);
        string str = "";// \"1\",
        string s = "/+";
        for (int i = 1; i <= 200; i++)
        {
            str += s + i + s + ",";
        }
        str = str.Replace('/', '\\');
        str = str.Replace('+', '"');
        //print(str + " ==>" + str);
        if (_UserProfileStrRemote != "")
        {
            UserProfile _UserProfileRemoteCache = new UserProfile();
            JsonUtility.FromJsonOverwrite(_UserProfileStrRemote, _UserProfileRemoteCache);
            //if (isAutoSync)
            //{
            //    using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            //    {
            //        newTask.WriteLine(JsonUtility.ToJson(_UserProfileRemoteCache));
            //        MCache.Instance.UserData.StarNumber = _UserProfileRemoteCache.TotalStar;
            //        newTask.Close();
            //    }
            //    GameController.Instance.UpdateShowStar(_UserProfileRemoteCache.TotalStar);
            //}
            //else
            {
                if (!isAutoSync)
                {
                    //GameController.Instance.ShowPopupConfirmOptions(() => // UPDATE FROM CLOUD
                    //{
                    //    using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
                    //    {
                    //        MCache.Instance.UserData.ToUserData(_UserProfileRemoteCache);
                    //        newTask.WriteLine(JsonUtility.ToJson(_UserProfileRemoteCache));
                    //        newTask.Close();
                    //    }
                    //    DOVirtual.DelayedCall(0.5f, () =>
                    //    {
                    //        for (int index = 0; index < _UserProfileRemoteCache.LevelsUnlockId.Count; index++)
                    //        {
                    //            GameController.Instance.ReUpdateUnlockLevel(int.Parse(_UserProfileRemoteCache.LevelsUnlockId[index]));
                    //        }
                    //        for (int index = 0; index < _UserProfileRemoteCache.LevelsCompleteId.Count; index++)
                    //        {
                    //            GameController.Instance.ReUpdateLevelComplete(int.Parse(_UserProfileRemoteCache.LevelsCompleteId[index]));
                    //        }
                    //        for (int index = 0; index < _UserProfileRemoteCache.SkinUnlockedId.Count; index++)
                    //        {
                    //            GameController.Instance.ReUpdateSkin(int.Parse(_UserProfileRemoteCache.SkinUnlockedId[index]));
                    //        }
                    //        GameController.Instance.UpdateShowStar(_UserProfileRemoteCache.TotalStar);
                    //        GameController.Instance.CurrentIdBall = _UserProfileRemoteCache.IdSkinSelected;
                    //    });
                    //}, () => // FUSH TO CLOUD
                    //{
                    //    //using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
                    //    //{
                    //    //    userProfileLocal.ToUserProfile(MCache.Instance.UserData);
                    //    //    newTask.WriteLine(JsonUtility.ToJson(userProfileLocal));
                    //    //    newTask.Close();
                    //    //}
                    //    userProfileLocal.ToUserProfile(MCache.Instance.UserData);
                    //    Debug.Log("_UserProfileStrLocal ==> " + userProfileLocal.TotalStar);
                    //    PushUserProfile(userID, userProfileLocal);
                    //});

                    userProfileLocal.ToUserProfile(MCache.Instance.UserData);
                    //Debug.Log("_UserProfileStrLocal ==> " + userProfileLocal.TotalStar);
                    PushUserProfile(userID, userProfileLocal);
                }
                else
                {
                    using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
                    {
                        MCache.Instance.UserData.ToUserData(_UserProfileRemoteCache);
                        newTask.WriteLine(JsonUtility.ToJson(_UserProfileRemoteCache));
                        newTask.Close();
                    }

                    DOVirtual.DelayedCall(ConfigVariables.DELAY_TIME_WAITING, () =>
                    {
                        GameController.Instance.UpdateShowStar(_UserProfileRemoteCache.TotalStar);
                        for (int index = 0; index < MCache.Instance.LevelsUI.Count; index++)
                        {
                            GameController.Instance.ReShowLockLevel(index + 1);
                        }
                        for (int index = 0; index < _UserProfileRemoteCache.LevelsUnlockId.Count; index++)
                        {
                            GameController.Instance.ReUpdateUnlockLevel(int.Parse(_UserProfileRemoteCache.LevelsUnlockId[index]));
                        }
                        for (int index = 0; index < _UserProfileRemoteCache.LevelsCompleteId.Count; index++)
                        {
                            GameController.Instance.ReUpdateLevelComplete(int.Parse(_UserProfileRemoteCache.LevelsCompleteId[index]));
                        }
                        for (int index = 0; index < _UserProfileRemoteCache.SkinUnlockedId.Count; index++)
                        {
                            GameController.Instance.ReUpdateSkin(int.Parse(_UserProfileRemoteCache.SkinUnlockedId[index]));
                        }
                        //GameController.Instance.CurrentIdBall = _UserProfileRemoteCache.IdSkinSelected;
                    });
                }
            }

            //if (_UserProfileRemoteCache.TotalStar > userProfileLocal.TotalStar)
            //{
            //    if (isAutoSync)
            //    {

            //        JsonUtility.FromJsonOverwrite(_UserProfileStrRemote, userProfileLocal);
            //        OnSyncUserProfile?.Invoke();
            //    }
            //    else
            //    {
            //        //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("We detect a save game with " + _UserProfile.TotalStar + " total star\nDo you want to use it?", true, "Yes", () =>
            //        //{
            //        //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
            //        JsonUtility.FromJsonOverwrite(_UserProfileStrRemote, userProfileLocal);
            //        OnSyncUserProfile?.Invoke();
            //        //}, () =>
            //        //{
            //        //    SaveDataManager.Instance.isCloudSave = false;
            //        //    UIManager.Instance.uiSettingDialog.updateCloudSaveImage();
            //        //}));
            //    }
            //}
            //else if (_UserProfileRemoteCache.TotalStar < userProfileLocal.TotalStar)
            //{
            //    PushUserProfile(userID, userProfileLocal);
            //}
            //else
            //{
            //    if (_UserProfileRemoteCache.SkinUnlockedId.Count > userProfileLocal.SkinUnlockedId.Count)
            //    {
            //        if (isAutoSync)
            //        {
            //            JsonUtility.FromJsonOverwrite(_UserProfileStrRemote, userProfileLocal);
            //            OnSyncUserProfile?.Invoke();
            //        }
            //        else
            //        {
            //            //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("We detect a save game with " + _UserProfile.SkinUnlockedId.Count + " total skin\nDo you want to use it?", true, "Yes", () =>
            //            //{
            //            //    UIManager.Instance.HideDialog(DialogName.UIMessageBox);
            //            JsonUtility.FromJsonOverwrite(_UserProfileStrRemote, userProfileLocal);
            //            OnSyncUserProfile?.Invoke();
            //            //}, () =>
            //            //{
            //            //    SaveDataManager.Instance.isCloudSave = false;
            //            //    UIManager.Instance.uiSettingDialog.updateCloudSaveImage();
            //            //}));
            //        }
            //    }
            //    else if (_UserProfileRemoteCache.SkinUnlockedId.Count < userProfileLocal.SkinUnlockedId.Count)
            //    {
            //        PushUserProfile(userID, userProfileLocal);
            //    }
            //}
        }
        else
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                userProfileLocal.ToUserProfile(MCache.Instance.UserData);
                //Debug.Log("_UserProfileStrLocalUptoRemoe ==> " + userProfileLocal.TotalStar);
                newTask.WriteLine(JsonUtility.ToJson(userProfileLocal));
                newTask.Close();
            }
            PushUserProfile(userID, userProfileLocal);
            //DOVirtual.DelayedCall(ConfigVariables.DELAY_TIME_WAITING, () => { GameController.Instance.HidePopupWaiting(); }).SetId(this);
        }
    }

    private void PushUserProfile(string userID, UserProfile _UserProfile)
    {

        firebaseUserProfile data = new firebaseUserProfile(JsonUtility.ToJson(_UserProfile));
        Dictionary<string, object> entryValues = data.ToDictionary();
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        childUpdates["/up/" + userID] = entryValues;
        reference.UpdateChildrenAsync(childUpdates);
    }

    public void UnLockSkin(string _skinID)
    {
        if (!userProfileLocal.SkinUnlockedId.Contains(_skinID))
        {
            userProfileLocal.SkinUnlockedId.Add(_skinID);
        }
    }

    public void UnlockAllRewardedSkin()
    {
        //var configSkinItems = AvConfigManager.configSkin.GetAllItem();

        //for (int i = configSkinItems.Count - 1; i >= 0; i--)
        //{
        //    if (configSkinItems[i].UnlockType == "Ads")
        //    {
        //        UnLockSkin(configSkinItems[i].ID);
        //    }
        //}
    }
    public void UnlockNoAds()
    {
        isRemoveAds = true;
        GameController.Instance.HideBtnRemoveAds();
        AdsManager.Instance.HideBanner();
    }
    public void UnlockVipGold()
    {
        isVipGold = true;
        MCache.Instance.UserData.IdLevelsUnlocked.Clear();
        for (int i = 1; i <= 200; i++)
        {
            MCache.Instance.UserData.IdLevelsUnlocked.Add(i);
            GameController.Instance.ReUpdateUnlockLevel(i);
        }

        MCache.Instance.UserData.IdSkins.Clear();
        for (int i = 1; i <= 30; i++)
        {
            MCache.Instance.UserData.IdSkins.Add(i);
            GameController.Instance.ReUpdateSkin(i);
        }

        AdsManager.Instance.HideBanner();
    }
    public void UnLockGift(string _giftID, string value)
    {
        if (!userProfileLocal.GiftReceivedId.Contains(_giftID))
        {
            userProfileLocal.GiftReceivedId.Add(_giftID);
        }

        UnLockSkin(value);
    }

    public void UnLockAchievement(string _AchievementID)
    {
        if (!userProfileLocal.AchievementDoneId.Contains(_AchievementID))
        {
            userProfileLocal.AchievementDoneId.Add(_AchievementID);
        }
        //UnLockSkin(configAchievementItems.Find(x => x.ID == _AchievementID).RewardValue);
    }

    private void SaveData()
    {
        if (TempFilePath != null)
        {
            using (StreamWriter newTask = new StreamWriter(TempFilePath, false))
            {
                userProfileLocal = new UserProfile();
                //print("MCache.Instance.UserData " + MCache.Instance.UserData.IdSkinSelected);
                userProfileLocal.ToUserProfile(MCache.Instance.UserData);
                string str = JsonUtility.ToJson(userProfileLocal);
                //print("SaveData ==>" + str);
                newTask.WriteLine(str);
                newTask.Close();
            }
        }
        if (GooglePlayGameManager.Instance.isAuthenticated)
        {
            PushUserProfile(GooglePlayGameManager.Instance.userID, userProfileLocal);
            Debug.Log("userProfileLocal.TotalStar " + userProfileLocal.TotalStar);
        }

        PlayerPrefs.SetInt(ConfigVariables.ID_SKIN_SELECTED, GameController.Instance.CurrentIdBall);
        Debug.Log("PlayerPrefs.GetFloat(ConfigVariables.ID_SKIN_SELECTED) " + PlayerPrefs.GetInt(ConfigVariables.ID_SKIN_SELECTED));
        Debug.Log("PlayerPrefs.GetFloat(ConfigVariables.ID_SKIN_SELECTED===> " + GameController.Instance.CurrentIdBall);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveData();
        }
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }

    [ContextMenu("delete cheat")]
    public void deletecheat()
    {
        FirebaseDatabase.DefaultInstance.GetReference("up")
        .GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("Deletecheat IEnumerator error ");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                string str = "";
                if (snapshot.ChildrenCount > 0)
                {

                    foreach (var child in snapshot.Children)
                    {
                        str = child.Child("data").GetValue(false).ToString();

                        UserProfile _UserProfile = new UserProfile();
                        JsonUtility.FromJsonOverwrite(str, _UserProfile);
                        if (_UserProfile.TotalStar > 33)
                        {
                            Debug.Log("Deletecheat found: " + child.Key + "---" + _UserProfile.TotalStar);
                            _delete(child.Key);
                        }

                    }

                    Debug.Log("Deletecheat Done");
                }

            }
        });


        void _delete(string _userID)
        {
            userProfileLocal.SkinUnlockedId = new List<string>();
            userProfileLocal.AchievementDoneId = new List<string>();
            userProfileLocal.GiftReceivedId = new List<string>();
            userProfileLocal.dictSongStatus = new List<PlayerSongsInfomation>();
            userProfileLocal.TotalStar = 0;

            //var configSongItems = AvConfigManager.configSongs.GetAllItem().OrderBy(x => x.Order).ToList();
            //for (int i = 0; i < configSongItems.Count; i++)
            //{
            //    if (!userProfile.dictSongStatus.Exists(x => x.Order.Equals(configSongItems[i].Order)))
            //    {
            //        userProfile.dictSongStatus.Add(new PlayerSongsInfomation(configSongItems[i].Order, 0, configSongItems[i].Order < 1));
            //    }
            //}

            PushUserProfile(_userID, userProfileLocal);
        }
    }
}

[System.Serializable]
public class UserProfile
{
    public int Version;

    public List<string> SkinUnlockedId;
    public List<string> LevelsUnlockId;
    public List<string> LevelsCompleteId;
    public List<string> AchievementDoneId;
    public List<string> GiftReceivedId;
    public List<PlayerSongsInfomation> dictSongStatus;
    public int TotalStar;
    public bool IsVipGold;
    public bool IsRemoveAds;
    public int IdSkinSelected;

    public void UpdateTotalStar()
    {
        TotalStar = 0;
        foreach (var item in dictSongStatus)
        {
            TotalStar += item.star;
        }
    }

    public UserProfile()
    {
        Version = 0;

        SkinUnlockedId = new List<string>();
        AchievementDoneId = new List<string>();
        GiftReceivedId = new List<string>();
        LevelsUnlockId = new List<string>();
        LevelsCompleteId = new List<string>();
        dictSongStatus = new List<PlayerSongsInfomation>();
        TotalStar = 0;
        IsVipGold = false;
        IsRemoveAds = false;
        IdSkinSelected = 1;
        SkinUnlockedId.Add("1");
        LevelsUnlockId.Add("1");
    }
    public void ToUserProfile(UserData userData)
    {
        SkinUnlockedId.Clear();
        AchievementDoneId.Clear();
        GiftReceivedId.Clear();
        LevelsUnlockId.Clear();
        LevelsCompleteId.Clear();

        for (int index = 0; index < userData.IdSkins.Count; index++)
        {
            SkinUnlockedId.Add(userData.IdSkins[index].ToString());
        }

        for (int index = 0; index < userData.IdAchiementDoned.Count; index++)
        {
            AchievementDoneId.Add(userData.IdAchiementDoned[index].ToString());
        }

        for (int index = 0; index < userData.IdLevelsUnlocked.Count; index++)
        {
            LevelsUnlockId.Add(userData.IdLevelsUnlocked[index].ToString());
        }

        for (int index = 0; index < userData.IdLevelsComplete.Count; index++)
        {
            LevelsCompleteId.Add(userData.IdLevelsComplete[index].ToString());
        }

        TotalStar = userData.StarNumber;
        IsVipGold = userData.IsVip;
        IsRemoveAds = userData.IsRemove;
        IdSkinSelected = userData.IdSkinSelected;
    }
}

[System.Serializable]
public class PlayerSongsInfomation
{
    public int Order = -2;
    public bool isUnLock = false;
    public int star = 0;

    public PlayerSongsInfomation(int _Order, int _star, bool _isUnlock)
    {
        Order = _Order;
        isUnLock = _isUnlock;
        star = _star;
    }
}

[System.Serializable]
public class firebaseUserProfile
{
    public string data = "";

    public firebaseUserProfile(string _data)
    {
        data = _data;
    }

    public Dictionary<string, object> ToDictionary()
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        result["data"] = data;

        return result;
    }
}