﻿using Facebook.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;

public class FacebookManager : SingletonMonoAwake<FacebookManager>
{

    public string UserID;
    public bool DoneInit = false;

    public bool isLogin {
        get
        {
#if UNITY_EDITOR
            return true;
#else
            return FB.IsLoggedIn;
#endif
        }
    }
    public override void OnAwake()
    {
        base.OnAwake();
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(()=> {

                FB.ActivateApp();

                if (FB.IsLoggedIn)
                {
                    FB.Mobile.RefreshCurrentAccessToken();
                    var aToken = AccessToken.CurrentAccessToken;
                    UserID = aToken.UserId;
                    Debug.Log("Hieu Unity Facebook  Init 0 : " + UserID);
                }
                else
                {
#if UNITY_EDITOR
                    UserID = "UNITY_EDITOR_FACEBOOK" + SystemInfo.deviceUniqueIdentifier;
#endif
                }
                DoneInit = true;
            });
        }
        else
        {
            FB.ActivateApp();

            if (FB.IsLoggedIn)
            {
                FB.Mobile.RefreshCurrentAccessToken();
                var aToken = AccessToken.CurrentAccessToken;
                UserID = aToken.UserId;
                Debug.Log("Hieu Unity Facebook  Init 1 : " + UserID);
            }
            DoneInit = true;
        }
    }

    Action<bool> callback;
    public void FacebookLoging(Action<bool> _callback)
    {
        Debug.Log("Hieu Unity Facebook start Loging : ");
        callback = _callback;
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, OnloggedIn);
    }

    private void OnloggedIn(ILoginResult loginResult)
    {
        Debug.Log("Hieu Unity Facebook start callback Loging : ");
        
        if (FB.IsLoggedIn)
        {
            var aToken = AccessToken.CurrentAccessToken;
            UserID = aToken.UserId;
#if UNITY_EDITOR
            UserID = "UNITY_EDITOR_FACEBOOK" + SystemInfo.deviceUniqueIdentifier;
#endif
            Debug.Log("Hieu Unity Facebook callback Loging : " + UserID);

        }
        else
        {
            Debug.Log("Hieu Unity Facebook callback Loging false  ");
        }
        callback?.Invoke(FB.IsLoggedIn);
    }

    Action sharesuccesscallback;
    public void FacebookFeedShare(Action _callback = null)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //UIManager.Instance.ShowDialog(DialogName.UIMessageBox, new MessageBoxData("No internet connection!", false, "ok", () =>
            //{
                //UIManager.Instance.HideDialog(DialogName.UIMessageBox);
            //}));
        }
        else
        {
            sharesuccesscallback = _callback;
            string linkDescription = "New Skin!!!";

            //ScreenCapture.CaptureScreenshot("ScreenshotFacebook.png");
            //Uri imageUri = new Uri("file://" + Application.persistentDataPath + "/ScreenshotFacebook.png");

#if UNITY_ANDROID
            Uri link = new Uri("https://play.google.com/store/apps/details?id=com.antada.gamingbeats.justtilesandbeats");
#elif UNITY_IOS
        Uri link = new Uri("https://apps.apple.com/us/app/classic-solitaire-klondike/id1508982237");
#endif
            //FB.Mobile.ShareDialogMode = ShareDialogMode.NATIVE;
            FB.FeedShare("", link, " Just Tiles And Beats", "Join Me", linkDescription, null, "", shareCallBack);
        }
    }

    private void shareCallBack(IShareResult result)
    {
        if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Hieu Unity Facebook FeedShare Error: " + result.Error);
        }
        else if (!string.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            UserProfileManager.Instance.UnLockAchievement("0");

            sharesuccesscallback?.Invoke();
            Debug.Log("Hieu Unity Facebook  FeedShare success! with post id" + result.PostId);
        }
        else
        {
            // Share succeeded without postID
            UserProfileManager.Instance.UnLockAchievement("0");
            sharesuccesscallback?.Invoke();
            Debug.Log("Hieu Unity Facebook  FeedShare success!");
        }
    }
}
