﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//using Antada.MRG01.Constance;
using System.Linq;

public enum ESetting
{
    Option1 = 0,
}

[Serializable]
public class DebugAttributeOption
{
    public float Curvy = 0.00262f;
    public float CamX = -10.4f;
    public float CamY = 9f;
    public float CamRotX = 40f;
    public float Speed = 0.7f;
}

public class SaveDataManager : Singleton<SaveDataManager>
{

    private const string kSensitivityDefault = "kSensitivityDefault";
    private const string kSensitivityUser = "kSensitivityUser";
    private const string kDelayTimeUserChange = "kDelayTimeUserChange";
    private const string kGraphicQuaityUserChange = "kGraphicQuaityUserChange";
    private const string kPushNotifi = "kPushNotifi";

    private const string kSkinNamePick = "kSkinNamePick";
    private const string kLanguage = "kLanguage";
    private const string kPlayByAd = "kPlayByAd";
    private const string kIsRated = "kisRated";
    private const string kLastDateShowRate = "kLastDateShowRate";
    private const string kFirstDatePlaying = "kFirstDatePlaying";
    private const string kCloudSave = "kCloudSave";
    private const string kIsFirstOpen = "kIsFirstOpen";

    private Dictionary<int, int> dicStarPerSong = new Dictionary<int, int>();

    private Vector3 camPos = default;
    private Vector3 camEuler = default;

    private float horizontalControllSpeed = 0.3f;
    private Vector2 gridCellSize = default;
    private Vector2 gridStartCellSize = default;
    private RectOffset gridPadding = default;
    private float camFollowZSmooth = 0;
    private bool isGod = false;
    private string selectedSong = "";
    private string selectedSongSort = "";
    private bool isShortVersion;
    private bool isTutorial;
    private bool isCheatWin = false;
    private bool isTestRollingSpeed = false;

    private float mControlSensitivityDefault = 0;

    private int checkPoint = -1;

    Dictionary<int, List<int>> dicMenuTree;

    public bool Init()
    {
        dicMenuTree = new Dictionary<int, List<int>>();
        //var menuDatas = AvConfigManager.configMenu.GetAllItem();
        //for (int i = 0; i < menuDatas.Count - 1; i++)
        //{
        //    if (menuDatas[i].M3 >= 0)
        //    {
        //        if (i + 1 < menuDatas.Count)
        //        {
        //            if ((menuDatas[i + 1].M3 >= 0)) dicMenuTree.Add(menuDatas[i].M3, new List<int> { menuDatas[i + 1].M3 });
        //            else if (menuDatas[i + 1].M2 >= 0) dicMenuTree.Add(menuDatas[i].M3, new List<int> { menuDatas[i + 1].M2, menuDatas[i + 1].M4 });
        //        }
        //        else
        //        {
        //            dicMenuTree.Add(menuDatas[i].M2, new List<int> { });
        //        }
        //    }
        //    else if (menuDatas[i].M2 >= 0)
        //    {
        //        if (i + 1 < menuDatas.Count)
        //        {
        //            if ((menuDatas[i + 1].M2 >= 0)) dicMenuTree.Add(menuDatas[i].M2, new List<int> { menuDatas[i + 1].M2 });
        //            else if (menuDatas[i + 1].M3 >= 0) dicMenuTree.Add(menuDatas[i].M2, new List<int> { menuDatas[i + 1].M3 });

        //            if ((menuDatas[i + 1].M4 >= 0)) dicMenuTree.Add(menuDatas[i].M4, new List<int> { menuDatas[i + 1].M4 });
        //            else if (menuDatas[i + 1].M3 >= 0) dicMenuTree.Add(menuDatas[i].M4, new List<int> { menuDatas[i + 1].M3 });
        //        }
        //        else
        //        {
        //            dicMenuTree.Add(menuDatas[i].M2, new List<int> { });
        //            dicMenuTree.Add(menuDatas[i].M4, new List<int> { });
        //        }
        //    }
        //}
        //remove commingsoon songs
        //foreach (var dt in dicMenuTree)
        //{
        //    foreach (var val in dt.Value)
        //    {
        //        if (val > GameConstance.NumOfSongsRelease / 2 - 1)
        //        {
        //            dt.Value.Clear();
        //            break;
        //        }
        //    }
        //}

        //foreach (var dt in dicMenuTree)
        //{
        //    Debug.LogError("=====>>> " + dt.Key);
        //    foreach (var val in dt.Value)
        //    {
        //        Debug.LogError(val);
        //    }
        //}
        return true;
    }
    #region Songdata

    //public List<PlayerSongsInfomation> LoadDictSongStatus()
    //{
    //    return UserProfileManager.Instance.userProfile.dictSongStatus;
    //}

    //public void SaveDataPlayerWin()
    //{
    //    var userProfileManager = UserProfileManager.Instance;
    //    var configSongItems = AvConfigManager.configSongs.GetAllItem().OrderBy(x => x.Order).ToList();

    //    if (string.IsNullOrEmpty(SelectedSong)) return;

    //    if (GetStarsPerSong.Count > 0)
    //    {
    //        Debug.Log("SaveDataPlayerWin");
    //        var songInfo = AvConfigManager.configSongs.GetAllItem().Find(x => x.ID == SelectedSong);
    //        if (songInfo != null)
    //        {

    //            var info = userProfileManager.userProfile.dictSongStatus.Find(x => x.Order == songInfo.Order);
    //            if (GetStarsPerSong.Count > info.star)
    //            {
    //                info.star = GetStarsPerSong.Count;
    //                userProfileManager.userProfile.UpdateTotalStar();
    //            }

    //            if (isPlayByAd)
    //            {
    //                info.isUnLock = true;
    //            }

    //            foreach (var item in userProfileManager.userProfile.dictSongStatus)
    //            {
    //                if (item.star > 0 && dicMenuTree.ContainsKey(item.Order))
    //                {
    //                    var nextsong = dicMenuTree[item.Order];
    //                    if (nextsong.Count > 0)
    //                    {
    //                        for (int i = 0; i < nextsong.Count; i++)
    //                        {
    //                            var itemConfig = configSongItems.Find(x => x.Order == nextsong[i]);
    //                            if (itemConfig.UnlockCondition <= userProfileManager.userProfile.TotalStar)
    //                            {
    //                                userProfileManager.userProfile.dictSongStatus.Find(x => x.Order == nextsong[i]).isUnLock = true;
    //                            }
    //                        }
    //                    }
    //                }
    //            }

    //            //var songsUnlock = userProfileManager.userProfile.dictSongStatus.FindAll(x=> x.isUnLock == true);
    //            //if (songsUnlock != null)
    //            //{
    //            //    NotificationManager.Instance.PurchaseRemoveAdsNotifications(songsUnlock.Count);
    //            //}


    //        }
    //        GooglePlayGameManager.Instance.PostLeaderboard(userProfileManager.userProfile.TotalStar);

    //    }
    //}

    //public ConfigSongItems GetUnlockNextSongs()
    //{
    //    if (int.TryParse(selectedSongSort, out int songSortInt))
    //    {
    //        if (dicMenuTree.ContainsKey(songSortInt))
    //        {
    //            if (dicMenuTree[songSortInt].Count == 0)//last song
    //            {
    //                var dicSongs = UserProfileManager.Instance.userProfile.dictSongStatus;
    //                List<PlayerSongsInfomation> listSuggests = new List<PlayerSongsInfomation>();
    //                foreach (var song in dicSongs)
    //                {
    //                    if (song.isUnLock == false && song.Order <= GameConstance.NumOfSongsRelease / 2 - 1) listSuggests.Add(song);
    //                }

    //                if (listSuggests.Count == 0)
    //                {
    //                    //listSuggests = dicSongs.Where(x => x.star == dicSongs.Min(y => y.star) && x.Order <= GameConstance.NumOfSongsRelease / 2 - 1).ToList();
    //                    var minStar = 4;
    //                    foreach (var song in dicSongs)
    //                    {
    //                        if (song.Order <= GameConstance.NumOfSongsRelease / 2 - 1 && song.star < minStar)
    //                        {
    //                            minStar = song.star;
    //                            listSuggests.Clear();
    //                            listSuggests.Add(song);
    //                        }
    //                    }
    //                }

    //                try
    //                {
    //                    return AvConfigManager.configSongs.GetAllItem().Find(x => x.Order == listSuggests[0].Order);
    //                }
    //                catch
    //                {
    //                    return null;
    //                }

    //            }
    //            else
    //            {
    //                return AvConfigManager.configSongs.GetAllItem().Find(x => x.Order == dicMenuTree[songSortInt][0]);
    //            }
    //        }
    //    }
    //    return null;
    //}

    //public ConfigSongItems GetCurrentSong()
    //{
    //    if (string.IsNullOrEmpty(SelectedSong)) return null;

    //    return AvConfigManager.configSongs.GetAllItem().Find(x => x.ID == SelectedSong);
    //}

    //#endregion

    public bool isCloudSave
    {
        set
        {
            PlayerPrefs.SetInt(kCloudSave, value ? 1 : 0);
        }
        get
        {
            return PlayerPrefs.GetInt(kCloudSave, 1) == 0 ? false : true;
        }
    }

    public bool isFirstOpen
    {
        set
        {
            PlayerPrefs.SetInt(kIsFirstOpen, value ? 1 : 0);
        }
        get
        {
            return PlayerPrefs.GetInt(kIsFirstOpen, 0) == 0 ? true : false;
        }
    }

    public string SkinNamePick
    {
        set
        {
            PlayerPrefs.SetString(kSkinNamePick, value);
        }

        get
        {
            return PlayerPrefs.GetString(kSkinNamePick, "Skin_00");
        }
    }

    public bool isPlayByAd
    {
        set
        {
            PlayerPrefs.SetInt(kPlayByAd, value == true ? 1 : 0);
        }
        get
        {
            return PlayerPrefs.GetInt(kPlayByAd, 0) == 0 ? false : true;
        }
    }

    public bool IsRated
    {
        set
        {
            PlayerPrefs.SetInt(kIsRated, value == true ? 1 : 0);
        }
        get
        {
            return PlayerPrefs.GetInt(kIsRated, 0) == 0 ? false : true;
        }
    }

    public DateTime LastDateShowRate
    {
        set
        {
            PlayerPrefs.SetString(kLastDateShowRate, value.ToString());
        }
        get
        {
            return DateTime.Parse(PlayerPrefs.GetString(kLastDateShowRate, (new DateTime()).ToString()));
        }
    }

    public DateTime FirstDatePlaying
    {
        set
        {
            PlayerPrefs.SetString(kFirstDatePlaying, value.ToString());
        }
        get
        {
            return DateTime.Parse(PlayerPrefs.GetString(kFirstDatePlaying, (new DateTime()).ToString()));
        }
    }

    public string LanguagePick
    {
        set
        {
            PlayerPrefs.SetString(kLanguage, value);
        }

        get
        {
            return PlayerPrefs.GetString(kLanguage, "EN");
        }
    }

    public void HitStar(int node)
    {
        if (!dicStarPerSong.ContainsKey(node))
        {
            dicStarPerSong.Add(node, 1);
        }
    }

    public Dictionary<int, int> GetStarsPerSong
    {
        get { return dicStarPerSong; }
    }

    public int CheckPoint
    {
        set
        {
            checkPoint = value;
        }
        get
        {
            return checkPoint;
        }
    }

    public bool IsPlayTutorialSong
    {
        get { return isTutorial; }
    }

    public void Clear()
    {
        dicStarPerSong.Clear();
        CheckPoint = -1;
    }

    #region Debug  
    public bool IsTestRollingSpeed
    {
        set { isTestRollingSpeed = value; }
        get { return isTestRollingSpeed; }
    }

    public bool IsCheatWin
    {
        set { isCheatWin = value; }
        get { return isCheatWin; }
    }
    public void CheatUnlockLevel(int sortLevel, int star)
    {
        var userProfileManager = UserProfileManager.Instance;
        var info = userProfileManager.userProfileLocal.dictSongStatus.Find(x => x.Order == sortLevel);
        if (star > info.star && star >= 0 && star <= 3)
        {
            info.isUnLock = true;
            for (int i = 0; i < star; i++)
            {
                HitStar(i);
            }

            info.star = GetStarsPerSong.Count;
            userProfileManager.userProfileLocal.UpdateTotalStar();
            //SceneLoadManager.ReLoadLevel();
        }
    }
    public void CheatPlayerWinWithStar(int star)
    {
        for (int i = 0; i < star; i++)
        {
            HitStar(i);
        }
        //SaveDataPlayerWin();
    }
    public void SaveCameraPos(Vector3 pos)
    {
        camPos = pos;
    }

    public Vector3 GetCameraPos()
    {
        return camPos;
    }

    public void SaveCameraRotate(Vector3 eul)
    {
        camEuler = eul;
    }

    public Vector3 GetCameraRotate()
    {
        return camEuler;
    }

    public float HorizontalControllSpeed
    {
        set { horizontalControllSpeed = value; }
        get { return horizontalControllSpeed; }
    }

    public Vector2 GridCellSize
    {
        set { gridCellSize = value; }
        get { return gridCellSize; }
    }

    public Vector2 GridStartCellSize
    {
        set { gridStartCellSize = value; }
        get { return gridStartCellSize; }
    }

    public RectOffset GridPadding
    {
        set { gridPadding = value; }
        get { return gridPadding; }
    }

    public bool IsGod
    {
        set { isGod = value; }
        get { return isGod; }
    }

    public float CamFollowZSmooth
    {
        set { camFollowZSmooth = value; }
        get { return camFollowZSmooth; }
    }
    #endregion

    public string SelectedSong
    {
        private set { }
        get { return selectedSong; }
    }
    public string SelectedSongSort
    {
        set
        {
            selectedSongSort = value;

            if (selectedSongSort == "tutorial")
            {
                selectedSongSort = "0";
                isTutorial = true;
            }
            else
            {
                isTutorial = false;
            }

            if (int.TryParse(selectedSongSort, out int songSortInt))
            {
                isShortVersion = true;
                //var info = LoadDictSongStatus().Find(x => x.Order == songSortInt);
                //if (info.star > 0)
                //{
                //    isShortVersion = false;
                //}

                //selectedSong = isShortVersion ? AvConfigManager.configSongs.GetAllItem().Find(x => x.Order == songSortInt && AvConfigManager.configSongs.GetSongType(x.ID) == ESongType.S.ToString()).ID :
                //    AvConfigManager.configSongs.GetAllItem().Find(x => x.Order == songSortInt && AvConfigManager.configSongs.GetSongType(x.ID) == ESongType.F.ToString()).ID;

                //Debug.Log("====selectedSong====: " + selectedSong);
            }
        }
        get { return selectedSongSort; }
    }

    //Only use for tracking
    public string SelectedSongVersion
    {
        get
        {
            if (isTutorial)
            {
                return "tutorial";
            }
            else
            {
                return SelectedSongSort + (isShortVersion ? "s" : "f");
            }
        }
    }


    public float ControlSensitivityDefault
    {
        set { PlayerPrefs.SetFloat(kSensitivityDefault, value); }
        get { return PlayerPrefs.GetFloat(kSensitivityDefault, 1.5f); }
    }

    public float ControlSensitivityUserChange
    {
        set { PlayerPrefs.SetFloat(kSensitivityUser, value); }
        get { return PlayerPrefs.GetFloat(kSensitivityUser, 0); }
    }

    public float ControlSensitivityCurrent
    {
        get { return ControlSensitivityDefault + ControlSensitivityUserChange; }
    }

    public float DelayTimeUserChange
    {
        set { PlayerPrefs.SetFloat(kDelayTimeUserChange, value); }
        get { return PlayerPrefs.GetFloat(kDelayTimeUserChange, 0); }
    }

    //public float DelayTimeCurrent(string songID)
    //{
    //    //var delayTime = AvConfigManager.configSongs.GetAllItem().Find(x => x.ID == songID).Delay;
    //    return delayTime + DelayTimeUserChange;
    //}

    public int GraphicQualityUserChange
    {
        set
        {
            PlayerPrefs.SetInt(kGraphicQuaityUserChange, value);
            QualitySettings.SetQualityLevel(value);
        }
        get { return PlayerPrefs.GetInt(kGraphicQuaityUserChange, QualitySettings.GetQualityLevel()); }
    }

    public bool PushNotification
    {
        set
        {
            PlayerPrefs.SetInt(kPushNotifi, value ? 1 : 0);
            //if (value) LocalPushManager.Instance.TurnOnNoti();
            //else LocalPushManager.Instance.CancelAllNotifications();
        }
        get
        {
            return PlayerPrefs.GetInt(kPushNotifi, 1) == 1 ? true : false;
        }
    }


    /// <summary>
    /// Base64s the encode.
    /// </summary>
    /// <returns>The encode.</returns>
    /// <param name="plainText">Plain text.</param>
    public string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }
    /// <summary>
    /// Base64s the decode.
    /// </summary>
    /// <returns>The decode.</returns>
    /// <param name="base64EncodedData">Base64 encoded data.</param>
    public string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
    #endregion
}