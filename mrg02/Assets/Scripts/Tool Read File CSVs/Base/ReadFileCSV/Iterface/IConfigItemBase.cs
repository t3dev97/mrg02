﻿using UnityEngine;
using System.Collections;

public interface IConfigItemBase {

	string GetIconPath ();
	int GetNameId ();
	int GetDescId();

}
