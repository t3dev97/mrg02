﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using FileHelpers;
using System.IO;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreCommentedLines("//")]
[IgnoreEmptyLines(true)]
public class ConfigMapItems {

	public int NoteID;
	public string M1;
    public string M2;
    public string M3;
    public string M4;
    public string M5;
}
public class ConfigMap : GConfigDataTable<ConfigMapItems>
{
	public ConfigMap():base("ConfigMap")
	{
		
	}
	public ConfigMap(string name) : base(name)
	{

	}
	protected override void OnDataLoaded ()
	{
		//rebuild data index
		RebuildIndexField<int>("NoteID");
	}
	
	public ConfigMapItems GetItem(int ID)
	{
		return FindRecordByIndex<int>("NoteID", ID);
	}
	public List<ConfigMapItems> GetAllItem()
	{
		return records;
	}

}

