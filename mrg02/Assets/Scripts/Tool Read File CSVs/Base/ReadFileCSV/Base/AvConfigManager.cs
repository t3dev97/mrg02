using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvConfigManager : Singleton<AvConfigManager>
{
    public static bool IsInit = false;

    public static ConfigMap configMapTest;
    public static ItemShopConfig S_ItemShopConfig;
    public static UserConfig S_UserConfig;
    public static ListMusicConfig S_ListMusicConfig;
    public static LocalizationConfig S_LocalizationConfig;
    public static MusicLevelsConfig S_MusicLevelsConfig;
    public static HintSkillConfig S_HintSkillConfig;
    public static SfxConfig S_SfxConfig;

    public static Dictionary<string, IConfigDataTable> configList = new Dictionary<string, IConfigDataTable>();


    private void LoadDataConfig<TConfigTable>(ref TConfigTable configTable, params string[] dataPaths) where TConfigTable : IConfigDataTable, new()
    {
        try
        {
            configTable = new TConfigTable();
#if !USING_ASSETLOADER
            configTable.BeginLoadAppend();
#endif
            foreach (var path in dataPaths)
            {
#if !USING_ASSETLOADER
                configTable.LoadFromAssetPath(path);
#endif
                configList[path] = configTable;

            }
#if !USING_ASSETLOADER
            configTable.EndLoadAppend();
#endif

        }
        catch (System.Exception ex)
        {
            Debug.LogError("Load Config Error:" + configTable.GetName() + ", " + ex.ToString());
        }
    }

    private TConfigTable LoadDataConfig<TConfigTable>(params string[] dataPaths) where TConfigTable : IConfigDataTable, new()
    {
        try
        {
            var configTable = new TConfigTable();
#if !USING_ASSETLOADER
            configTable.BeginLoadAppend();
#endif
            foreach (var path in dataPaths)
            {
#if !USING_ASSETLOADER
                configTable.LoadFromAssetPath(path);
#endif
                configList[path] = configTable;

            }
#if !USING_ASSETLOADER
            configTable.EndLoadAppend();
#endif
            return configTable;
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Load Config Error: "+dataPaths[0] + " # "+ ex.ToString());
        }

        return default;
    }

    public void LoadAll()
    {
        Debug.LogWarning("log data config");
        LoadDataConfig<ConfigMap>(ref configMapTest, "Datas/BreakingHeart_F_Config");        
        LoadDataConfig<ItemShopConfig>(ref S_ItemShopConfig, "Datas/items_shop");        
        LoadDataConfig<UserConfig>(ref S_UserConfig, "Datas/user_info");        
        LoadDataConfig<ListMusicConfig>(ref S_ListMusicConfig, "Datas/list_music");        
        LoadDataConfig<LocalizationConfig>(ref S_LocalizationConfig, "Datas/localization");        
        LoadDataConfig<MusicLevelsConfig>(ref S_MusicLevelsConfig, "Datas/music_levels_config");        
        LoadDataConfig<HintSkillConfig>(ref S_HintSkillConfig, "Datas/hint_skill");        
        LoadDataConfig<SfxConfig>(ref S_SfxConfig, "Datas/list_sfx");        
        IsInit = true;
    }

    public void UnLoad()
    {
        foreach (KeyValuePair<string, IConfigDataTable> item in configList)
            item.Value.Clear();
        configList.Clear();
    }


#if UNITY_EDITOR

    private void LoadDataConfigEditor<TConfigTable>(ref TConfigTable configTable, params string[] dataPaths) where TConfigTable : IConfigDataTable, new()
    {

        try
        {
            configTable = new TConfigTable();

            configTable.BeginLoadAppend();
            foreach (var path in dataPaths)
            {
                configTable.LoadFromAssetPath(path);
            }
            configTable.EndLoadAppend();
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Load Config Error:" + configTable.GetName() + ", " + ex.ToString());
        }
    }

#endif


}
