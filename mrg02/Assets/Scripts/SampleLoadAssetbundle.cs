﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetBundles;

public class SampleLoadAssetbundle : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        MRGGameManager.onAssetbundleLoaded += OnAssetbundleLoaded;
    }

    public void OnAssetbundleLoaded(bool success)
    {
        if (success)
        {
            //Do something here
            if (MRGGameManager.Instance.GetABM() != null)
            {
                MRGGameManager.Instance.GetABM().GetBundle("bass", (b) =>
                {
                    Debug.LogError(b.name);
                    //lay bundle o day
                    //vd neu la Audio clip thi
                    var clip = b.LoadAsset("bass") as AudioClip;
                }
                , AssetBundleManager.DownloadSettings.UseCacheIfAvailable, (process)=> {
                    //tien do download -> show len UI
                    Debug.LogError(process);
                });
            }
           
        }
    }
    private void OnDestroy()
    {
        MRGGameManager.onAssetbundleLoaded -= OnAssetbundleLoaded;
    }
}
