﻿using UnityEngine;
using UnityEngine.UI;

public class GuideLineController : MonoBehaviour
{
    [SerializeField] private Transform trsGuideLine;
    [SerializeField] private Button btnShow;
    private bool isShow = false;
#if !DEBUG_LOG
    private void Awake()
    {
        btnShow.transform.parent.transform.gameObject.SetActive(false);
    }
#endif
#if DEBUG_LOG
    private void Start()
    {
        btnShow.onClick.AddListener(() =>
        {
            isShow = !isShow;
            if (isShow)
            {
                ShowGUIDE_LINE();
            }
            else
            {
                HideGUIDE_LINE();
            }
        });
    }
    private void OnGUI()
    {
        //GUIStyle gUI = new GUIStyle();
        //gUI.fontSize = 25;
        //gUI.border.Add(new Rect(150, 10.0f, 350, 100));
        //if (GUI.Button(new Rect(150, 10.0f, 350, 100), new GUIContent("ShowGUIDE_LINE"), gUI))
        //{
        //    isShow = !isShow;
        //}

        //if (isShow)
        //{
        //    ShowGUIDE_LINE();
        //}
        //else
        //{
        //    HideGUIDE_LINE();
        //}
    }
    private void ShowGUIDE_LINE()
    {
        trsGuideLine.gameObject.SetActive(true);
    }
    private void HideGUIDE_LINE()
    {
        trsGuideLine.gameObject.SetActive(false);
    }
#endif
}
